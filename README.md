# riscv

A very basic assembler and simulator for the RISC-V ISA.

## Assembler
I have sort of put this on hold for now. Most of the work was going into parsing, which is not very interesting. It was a great starting point to familiarize myself with the instruction set, though, and I may come back to it at some point. I am calling this project "glowbo".

## Simulator
The simulator is providing me with a means to improve my C++ while continuing to learn the ISA. I am trying to keep the hardware in mind all the time so at some point I can actually write some Verilog to have my own little CPU. The current focus is on simulating the base ISA (RV32I). I am calling this project "mumbo".

## Core
Hopefully someday! The project, when it comes, will be called "jinjo".
