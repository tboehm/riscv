.globl _start
_start:
        addi x1, x0, 0     # 'a'
        addi x2, x0, 1     # 'b'
        addi x3, x0, 0     # 'c'
        addi x4, x0, 0x100 # location of results
loop:
        add  x3, x1, x2    # c = a + b
        addi x1, x2, 0     # a = b
        addi x2, x3, 0     # b = c
        sw   x3, 0(x4)     # store result
        addi x4, x4, 4     # increment result pointer
        slti x5, x3, 1000  # stopping point
        bne x5, x0, loop

        ecall
