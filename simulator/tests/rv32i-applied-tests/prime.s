.globl _start

_start:
    j main

# divrem
#  [in] a0 -- dividend
#  [in] a1 -- divisor
# [out] a0 -- quotient
# [out] a1 -- remainder
divrem:
    # Check for divide-by-zero
    bne a1, zero, 1f
    ecall

1:
    # a2 = 0; // quotient
    mv a2, zero
2:
    # for ( ; dividend >= disivor; dividend -= divisor) {
    blt a0, a1, 3f
    sub a0, a0, a1
    #     quotient += 1;
    addi a2, a2, 1
    j 2b
    # }

3:
    # a1 = remainder
    mv a1, a0
    # a2 = quotient
    mv a0, a2
    jr ra

# mult
#  [in] a0 -- multiplicand
#  [in] a1 -- multiplier
# [out] a0 -- product
mult:
    # If a0 is 0, return 0
    bne a0, zero, 1f
    mv a0, zero
    jr ra

1:
    # a2 = 0; // product
    addi a2, zero, 0
2:
    # for ( ; a1 != 0; a1--) {
    beq a1, zero, 3f
    add a2, a2, a0
    addi a1, a1, -1
    j 2b

3:
    # Move product into a0
    mv a0, a2
    jr ra

# isprime
#  [in] a0 -- prime candidate
# [out] a0 -- 1 if prime, 0 if composite
isprime:
    addi sp, sp, -8
    sw ra, 4(sp)

    # treat 0-2 as special cases
    addi a1, zero, 2
    blt a0, a1, 3f
    beq a0, a1, 4f

    # use temporary registers because `divrem` will clobber a0-a2
    mv t0, a0  # prime candidate (dividend)
    mv t1, a1  # divisor, starts at 2
    # for (t2 = (t0 // t1); t1 < t2; t1++) {
    srli t2, t0, 1  # only go up until half of the divisor
1:
    bgt t1, t2, 4f
    # Check dividend % divisor:
    mv a0, t0
    mv a1, t1
    jal divrem
    beq a1, zero, 3f  # t0 % t1 == 0
    addi t1, t1, 1
    j 1b

3: # not prime
    mv a0, zero
    lw ra, 4(sp)
    addi sp, sp, 8
    jr ra

4: # prime
    addi a0, zero, 1
    lw ra, 4(sp)
    addi sp, sp, 8
    jr ra

main:
    # Initialize stack pointer
    li sp, 0x80000000

    # Test 102 * 83
    addi a0, zero, 102
    addi a1, zero, 83
    jal mult
    # s0 == 0x0000.2112 (4266)
    mv s0, a0

    # Test 1023 / 16
    addi a0, zero, 1023
    addi a1, zero, 16
    jal divrem
    # s1 == 0x0000.003f (63)
    mv s1, a0
    # s2 == 0x0000.000f (15)
    mv s2, a1

    # Test 1024 / 16
    addi a0, zero, 1024
    addi a1, zero, 16
    jal divrem
    # s3 == 0x0000.0040 (64)
    mv s3, a0
    # s4 == 0x0000.0000
    mv s4, a1

    # Test 6 / 2
    addi a0, zero, 6
    addi a1, zero, 2
    jal divrem
    # s5 == 0x0000.0003
    mv s5, a0
    # s6 == 0x0000.0000
    mv s6, a1

    # Test isprime(6)
    addi a0, zero, 97
    jal isprime
    # s7 == 0x0000.0000
    mv s7, a0

    # Test isprime(7)
    addi a0, zero, 11
    jal isprime
    # s8 == 0x0000.0001
    mv s8, a0

    # Find the sum of all primes under 1000
    addi s9, zero, 1     # prime candidate
    addi t4, zero, 1000  # maximum
    mv s10, zero         # prime count
    mv s11, zero         # prime sum
1:
    # for (n = 1; n < 100; n++) {
    beq s9, t4, 3f
    mv a0, s9
    jal isprime
    #     counter += isprime(n) ? 1 : 0;
    add s10, s10, a0
    #     n prime => add n to prime sum
    beqz a0, 2f
    add s11, s11, s9
2:
    addi s9, s9, 1
    # }
    j 1b

3:
    addi s11, s11, 0
    ecall
