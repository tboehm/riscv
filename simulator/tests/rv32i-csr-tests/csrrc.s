.globl _start
_start:
    addi x1, zero, 0x0f     # x1 = 0x0000000f
    addi x2, zero, 0x15     # x2 = 0x00000015
    csrrs x0, mscratch, x1  # mscratch = 0x0000000f
    csrrc x3, mscratch, x2  # x3 = 0x0000000f, mscratch = 0x000000a
    csrrc x4, mscratch, x3  # x4 = 0x0000000a, mscratch = 0x0000000
    csrrc x5, mscratch, x0  # x5 = 0x00000000
    ecall
