.globl _start
_start:
    csrrsi x1, mscratch, 1  # x1 = 0x00000000, mscratch = 0x00000001
    csrrsi x2, mscratch, 2  # x2 = 0x00000001, mscratch = 0x00000003
    csrrsi x3, mscratch, 8  # x3 = 0x00000003, mscratch = 0x0000000b
    csrrsi x0, mscratch, 16 # mscratch = 0x0000001b
    ecall
