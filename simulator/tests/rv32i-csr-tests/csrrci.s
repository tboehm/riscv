.globl _start
_start:
    csrrsi x0, mscratch, 0x0f # mscratch = 0x0000000f
    csrrci x3, mscratch, 0x15 # x3 = 0x0000000f, mscratch = 0x000000a
    csrrci x4, mscratch, 0x0a # x4 = 0x0000000a, mscratch = 0x0000000
    csrrci x5, mscratch, 0x0  # x5 = 0x00000000
    ecall
