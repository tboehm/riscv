.globl _start
_start:
    csrrwi x1, mscratch, 0xf  # x1 = 0x00000000, mscratch = 0x0000000f
    csrrwi x2, mscratch, 0x10 # x2 = 0x0000000f, mscratch = 0x00000010
    csrrwi x3, mscratch, 0    # x3 = 0x00000010, mscratch = 0x00000000
    csrrwi x4, mscratch, 0x12 # x4 = 0x00000000, mscratch = 0x00000012
    csrrwi x0, mscratch, 0    #                  mscratch = 0x00000000
    csrrwi x5, mscratch, 0x0a # x5 = 0x00000000, mscratch = 0x0000000a
    ecall
