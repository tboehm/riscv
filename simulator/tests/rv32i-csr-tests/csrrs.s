.globl _start
_start:
    addi x1, zero, 15       # x1 = 0x0000000f
    slli x2, x1, 4          # x2 = 0x000000f0
    slli x3, x1, 28         # x3 = 0xf0000000
    csrrs x4, mscratch, x2  # x4 = 0x00000000, mscratch = 0x000000f0
    csrrs x0, mscratch, x1  #                  mscratch = 0x000000ff
    csrrs x5, mscratch, x3  # x5 = 0x000000ff, mscratch = 0xf00000ff
    csrrs x6, mscratch, x0  # x6 = 0xf00000ff
    ecall
