.globl _start
_start:
    addi x1, zero, 15       # x1 = 0x0000000f
    slli x2, x1, 28         # x3 = 0xf0000000
    csrrw x3, mscratch, x1  # x3 = 0x00000000, mscratch = 0x0000000f
    csrrw x4, mscratch, x3  # x4 = 0x0000000f, mscratch = 0x00000000
    csrrw x5, mscratch, x2  # x5 = 0x00000000, mscratch = 0xf0000000
    csrrw x0, mscratch, x0  # mscratch = 0x00000000
    csrrw x6, mscratch, x4  # x6 = 0x00000000, mscratch = 0x0000000f
    ecall
