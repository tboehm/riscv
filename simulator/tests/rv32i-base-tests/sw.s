.globl _start
_start:
addi x1, x0, 5   # x1 = 0x00000005
slli x2, x1, 4   # x2 = 0x00000050
sw x1, 4(x2)     # m[x54] = 0x05
lw x3, 4(x2)     # x3 = 0x00000005
ecall
