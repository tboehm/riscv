.globl _start
_start:
addi x1, x0, -1      # x01 = 0xffffffff
andi x2, x1, -0x556  # x02 = 0xfffffaaa
xori x3, x2, -1      # x03 = 0x00000555
slli x4, x3, 12      # x04 = 0x00555000
ori  x5, x4, 0x123   # x05 = 0x00555123
addi x6, x5, -0x122  # x06 = 0x00555001
slli x7, x6, 9       # x07 = 0xaaa00200
srai x8, x7, 4       # x08 = 0xfaaa0020
srli x9, x8, 4       # x09 = 0x0faaa002
slti x10, x3, -1     # x10 = 0x00000000
sltiu x11, x3, -1    # x11 = 0x00000001
slti x12, x8, 1      # x12 = 0x00000001
sltiu x13, x8, 1     # x13 = 0x00000000
ecall
