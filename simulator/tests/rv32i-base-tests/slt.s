.globl _start
_start:
addi x1, x0, 15  # x1 = 0x0000000f
addi x2, x0, -15 # x2 = 0xfffffff1
slti x3, x1, 14  # x3 = 0x00000000
slti x4, x1, 15  # x4 = 0x00000000
slti x5, x1, 16  # x5 = 0x00000001
slti x6, x2, -14 # x6 = 0x00000001
slti x7, x2, -15 # x7 = 0x00000000
slti x8, x2, -16 # x8 = 0x00000000
ecall
