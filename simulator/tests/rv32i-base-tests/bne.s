# NOTE: This test does not cover the extreme ranges of BNE's offset
.globl _start
_start:
    addi x1, x1, 0xf    # pc = 0x00, x01 = 0x0000000f
    bne x1, x0, quart   # pc = 0x04, branch to "quart" taken
    addi x2, x0, 0x1    # pc = 0x08, x02 = 0x00000001 (never executed)

quart:
    add x3, x1, x1      # pc = 0x0c, x03 = 0x0000001e
    add x4, x3, x3      # pc = 0x10, x04 = 0x0000003c
    sll x5, x1, x2      # pc = 0x14, x05 = 0x0000000f, then 1e, then 3c
    beq x4, x5, end1    # pc = 0x18, branch to "end1" taken later
    addi x2, x2, 0x1    # pc = 0x1c, x02 = 0x00000001, then 02
    j quart             # pc = 0x20, go to "quart"

end1:
    addi x6, x0, 0x6    # pc = 0x24, x06 = 0x00000006
    ecall
