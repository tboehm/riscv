.globl _start
_start:
addi x1, x0, -0x001 # x01 = 0xffffffff
addi x2, x0, 0x001  # x02 = 0x00000001
and x3, x1, x2      # x03 = 0x00000001
and x4, x1, x0      # x04 = 0x00000000
addi x5, x0, 0x0ab  # x05 = 0x000000ab
addi x6, x0, 0x0f3  # x06 = 0x000000f3
and x7, x5, x6      # x07 = 0x000000a3
ecall
