.globl _start
_start:
addi x1, x0, 13 # x1 = 0x0000000d
addi x2, x0, 8  # x2 = 0x00000008
sub x3, x1, x2  # x3 = 0x00000005
sub x4, x3, x2  # x4 = 0xfffffffd
addi x5, x4, -1 # x5 = 0xfffffffc
sub x6, x4, x5  # x6 = 0x00000001
ecall
