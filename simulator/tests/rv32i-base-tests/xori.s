.globl _start
_start:
addi x1, x0, 0xa  # x01 = 0x0000000a
xori x2, x1, 0x5  # x02 = 0x0000000f
xori x3, x2, -0x1 # x03 = 0xfffffff0
xori x4, x3, 0x0  # x04 = 0xfffffff0
xori x5, x2, 0x0  # x05 = 0x0000000f
xori x6, x5, 0xe  # x06 = 0x00000001
ecall
