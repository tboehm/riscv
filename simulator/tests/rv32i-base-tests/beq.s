# NOTE: This test does not cover the extreme ranges of BEQ's offset
.globl _start
_start:
    addi x1, x0, 0xf   # pc = 0x00, x01 = 0x0000000f
    beq x1, x2, double # pc = 0x04, go to "double" the second time
    add x2, x1, x0     # pc = 0x08, x02 = 0x0000000f
    j _start           # pc = 0x0c, go to "_start"

quart:
    srli x5, x4, 0x2   # pc = 0x10, x05 = 0x00000007
    j end2             # pc = 0x14, go to "end2"

half:
    srli x5, x4, 0x1   # pc = 0x18, x04 = 0x0000000f (never executed)
    j end1             # pc = 0x1c, go to "end1" (never executed)

double:
    slli x3, x1, 0x1   # pc = 0x20, x03 = 0x0000001e
    beq x3, x4, half   # pc = 0x24, branch to "half" not taken
    add x4, x2, x2     # pc = 0x28, x04 = 0x0000001e
    beq x3, x4, quart  # pc = 0x2c, go to "quart"

end1:
    addi x7, x0, 0x1   # pc = 0x30, x07 = 0x00000001 (never executed)

end2:
    addi x6, x0, 0x1   # pc = 0x34, x06 = 0x00000001
    ecall
