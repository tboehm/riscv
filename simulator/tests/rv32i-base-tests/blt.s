.globl _start
_start:
    addi x1, x0, 0xf   # pc = 0x00, x01 = 0x0000000f
    addi x2, x0, 0x2   # pc = 0x04, x02 = 0x00000002

loop:
    addi x3, x3, 0x8   # pc = 0x08, x03 = 0x00000008, then 10
    addi x31, x31, 0x1 # pc = 0x0c, x31 = 0x00000001, then 2
    blt x3, x1, loop   # pc = 0x10, branch taken once
    blt x31, x2, check # pc = 0x14, branch not taken (x31 == x2)

noskip:
    addi x4, x0, 0x1   # pc = 0x18, x04 = 0x00000001
    addi x5, x0, 0x1f  # pc = 0x1c, x05 = 0x0000001f

check:
    addi x6, x6, 0x1   # pc = 0x20, x06 = 0x00000001, then 2
    sll x7, x4, x5     # pc = 0x24, x07 = 0x00000000, then 0x40000000
    addi x5, x5, -0x1  # pc = 0x28, x05 = 0x0000001e, then 0x0000001d
    blt x7, x4, check  # pc = 0x2c, branch taken once
    slli x8, x4, 0x1f  # pc = 0x30, x08 = 0x80000000
    bltu x4, x8, end   # pc = 0x34, branch taken

skip:
    addi x9, x0, 0x1   # pc = 0x38, x9 = 0x00000001 (not executed)

end:
    addi x10, x0, 0x1  # pc = 0x3c, x10 = 0x00000001
                       # pc ends at 0x44 (zeros at 0x40)
    ecall
