.globl _start
_start:
addi x1, x0, 7  # x01 = 0x00000007
slli x2, x1, 5  # x02 = 0x000000e0
or   x3, x1, x2 # x03 = 0x000000e7
slli x4, x1, 4  # x04 = 0x00000070
slli x5, x1, 11 # x05 = 0x00003800
sb   x1, 0(x4)  # m[0x70] (byte) = 0x07
sb   x2, 1(x4)  # m[0x71] (byte) = 0xe0
sb   x5, 3(x4)  # m[0x73] (byte) = 0x00
sb   x3, 2(x4)  # m[0x72] (byte) = 0xe7
# m[0x70] (word) = 0x00e7e007
lb   x6, 0(x4)  # x06 = 0x00000007
lbu  x7, 0(x4)  # x07 = 0x00000007
lb   x8, 1(x4)  # x08 = 0xffffffe0
lbu  x9, 1(x4)  # x09 = 0x000000e0
lb   x10, 2(x4) # x10 = 0xffffffe7
lbu  x11, 2(x4) # x11 = 00000000e7
lb   x12, 3(x4) # x12 = 0x00000000
lbu  x13, 3(x4) # x12 = 0x00000000
lw   x14, 0(x4) # x14 = 0x00e7e007
lw   x15, 4(x4) # x15 = 0x00000000
ecall
