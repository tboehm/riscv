.globl _start
_start:
lui x1, 0x1      # x01 = 0x00001000
lui x2, 0x12     # x02 = 0x00012000
lui x3, 0x123    # x03 = 0x00123000
lui x4, 0x1234   # x04 = 0x01234000
lui x5, 0x12345  # x05 = 0x12345000
lui x6, 0xfffff  # x06 = 0xfffff000
ecall
