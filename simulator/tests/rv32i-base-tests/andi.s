.globl _start
_start:
addi x1, x0, -0x001 # x01 = 0xffffffff
andi x2, x1, -0x0f1 # x02 = 0xffffff0f
andi x3, x2, -0x0ff # x03 = 0xffffff01
andi x4, x3,  0x001 # x04 = 0x00000001
andi x5, x4, -0x020 # x05 = 0x00000000
ecall
