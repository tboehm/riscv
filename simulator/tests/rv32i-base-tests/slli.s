.globl _start
_start:
addi x1, x0, 1  # x01 = 0x00000001
slli x2, x1, 1  # x02 = 0x00000002
slli x3, x2, 2  # x03 = 0x00000008
slli x4, x1, 31 # x04 = 0x80000000
addi x5, x0, 15 # x05 = 0x0000000f
slli x6, x5, 1  # x06 = 0x0000001e
slli x7, x6, 17 # x07 = 0x003c0000
ecall
