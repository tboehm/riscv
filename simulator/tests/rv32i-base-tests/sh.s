.globl _start
_start:
addi x1, x0, 7  # x01 = 0x00000007
slli x2, x1, 13 # x02 = 0x0000e000
add  x3, x1, x2 # x03 = 0x0000e007
slli x4, x1, 4  # x03 = 0x00000070
slli x5, x1, 17 # x04 = 0x000e0000
sh   x1, 0(x4)  # m[0x70] (half) = 0x0007
sh   x2, 2(x4)  # m[0x72] (half) = 0xe000
# m[0x70] (word) = 0xe0000007
sh   x3, 4(x4)  # m[0x74] (half) = 0xe007
sh   x5, 6(x4)  # m[0x76] (half) = 0x0000
# m[0x74] (word) = 0x0000e007
lh   x6, 0(x4)  # x06 = 0x00000007
lhu  x7, 0(x4)  # x07 = 0x00000007
lh   x8, 2(x4)  # x08 = 0xffffe000
lhu  x9, 2(x4)  # x09 = 0x0000e000
lh   x10, 4(x4) # x10 = 0xffffe007
lhu  x11, 4(x4) # x11 = 0x0000e007
ecall
