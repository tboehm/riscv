# NOTE: This test does not cover the extreme ranges of JALR's offset
.globl _start
_start:
    addi x1, x0, 0x0f   # pc = 0x00, x01 = 0x0000000f
    addi x2, x0, 0x18   # pc = 0x04, x02 = 0x00000018 (address of "double")
    jalr x3, x2, 0x0    # pc = 0x08, x03 = 0x0000000c (pc+4)

return:
    add x10, x7, x1     # pc = 0x0c, x10 = 0x0000003c
    addi x11, x0, -0x04 # pc = 0x10, x11 = 0xfffffffc
    jalr x0, x11, 0x34  # pc = 0x14, x00 = 0x00000000, go to "end"

double:
    add x4, x1, x1      # pc = 0x18, x04 = 0x0000001e
    addi x5, x0, 0x20   # pc = 0x1c, x05 = 0x00000020 (next instruction, one before "triple")
    jalr x6, x5, 0x4    # pc = 0x20, x06 = 0x00000024 (pc+4), go to "triple"

triple:
    add x7, x4, x1      # pc = 0x24, x07 = 0x0000002d
    addi x8, x0, 0x14   # pc = 0x28, x08 = 0x00000014
    jalr x9, x8, -0x8   # pc = 0x2c, x09 = 0x00000030 (pc+4), go to "return"

end:
    add x12, x10, x1    # pc = 0x30, x12 = 0x0000004b
    ecall
