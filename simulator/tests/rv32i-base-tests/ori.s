.globl _start
_start:
ori x1, x0, 0x005 # x01 = 0x00000005
ori x2, x1, 0x008 # x02 = 0x0000000d
ori x3, x2, 0x002 # x03 = 0x0000000f
ori x4, x3, -0x10 # x04 = 0xffffffff
ori x5, x3, -0x20 # x05 = 0xffffffef
ori x6, x5, 0x010 # x06 = 0xffffffff
ecall
