# NOTE: This test does not cover the extreme ranges of JAL's offset
.globl _start
_start:
    addi x1, x0, 0xf # pc = 0x00, x01 = 0x0000000f
    jal x2, double   # pc = 0x04, x02 = 0x00000008 (PC+4)

return:
    add x7, x5, x1   # pc = 0x08, x07 = 0x0000003c
    jal x8, end      # pc = 0x0c, x08 = 0x00000010

double:
    add x3, x1, x1   # pc = 0x10, x03 = 0x0000001e
    jal x4, triple   # pc = 0x14, x04 = 0x00000018

triple:
    add x5, x3, x1   # pc = 0x18, x05 = 0x0000002d
    jal x0, return   # pc = 0x1c, x00 = 0x00000000

end:
    add x9, x4, x8   # pc = 0x20, x09 = 0x00000028
    ecall
