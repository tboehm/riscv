.globl _start
_start:
addi x1, x0, 0x005 # x01 = 0x00000005
addi x2, x0, 0x008 # x02 = 0x00000008
or x3, x2, x1      # x03 = 0x0000000d
addi x4, x0, -0x01 # x04 = 0xffffffff
or x5, x4, x3      # x05 = 0xffffffff
or x6, x3, x0      # x06 = 0x0000000d
ecall
