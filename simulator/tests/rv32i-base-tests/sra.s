.globl _start
_start:
addi x1, x0, -1    # x01 = 0xffffffff
srai x2, x1, 1     # x02 = 0xffffffff
srai x3, x2, 2     # x03 = 0xffffffff
srai x4, x1, 31    # x04 = 0xffffffff
addi x5, x0, 0x7ff # x05 = 0x000007ff
srai x6, x5, 4     # x06 = 0x0000007f
srai x7, x6, 8     # x07 = 0x00000000
ecall
