.globl _start
_start:
addi x1, x0, 15   # x01 = 0x0000000f
addi x2, x0, -15  # x02 = 0xfffffff1
sltiu x3, x1, 14  # x03 = 0x00000000
sltiu x4, x1, 15  # x04 = 0x00000000
sltiu x5, x1, 16  # x05 = 0x00000001
sltiu x5, x2, -14 # x06 = 0x00000000
sltiu x6, x2, -15 # x07 = 0x00000000
sltiu x8, x2, -16 # x08 = 0x00000000
sltiu x9, x0, -1  # x09 = 0x00000001
sltiu x10, x0, 0  # x10 = 0x00000000
sltiu x11, x0, 1  # x11 = 0x00000001
ecall
