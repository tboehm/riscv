.globl _start
_start:
addi x1, x0, 1  # x01 = 0x00000001
sll x2, x1, x1  # x02 = 0x00000002
sll x3, x2, x2  # x03 = 0x00000008
add  x4, x3, x2 # x04 = 0x0000000a
sll x5, x4, x2  # x05 = 0x00000028
sll x31, x5, x5 # x31 = 0x00002800 -- only look at lower 5 bits!
ecall
