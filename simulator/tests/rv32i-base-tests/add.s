.globl _start
_start:
addi x1, x0, 1 # x1 = 0x00000001
add x2, x1, x0 # x2 = 0x00000001
add x3, x2, x2 # x3 = 0x00000002
add x4, x3, x2 # x4 = 0x00000003
add x5, x4, x3 # x5 = 0x00000005
add x6, x5, x4 # x6 = 0x00000008
ecall
