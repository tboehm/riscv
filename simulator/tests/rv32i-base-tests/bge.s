.globl _start
_start:
    addi x1, x0, 0xf   # pc = 0x00,  x1 = 0x0000000f
    addi x2, x0, 0x2   # pc = 0x04,  x2 = 0x00000002

loop:
    addi x3, x3, 0x8   # pc = 0x08,  x3 = 0x00000008, then 10
    addi x31, x31, 0x1 # pc = 0x0c, x31 = 0x00000001, then 2
    bge x1, x3, loop   # pc = 0x10, branch taken once
    bge x2, x31, setup # pc = 0x14, branch not taken (x31 == x2)

skip1:
    addi x11, x0, 0x1  # pc = 0x18, x11 = 0x00000001 (not executed)

setup:
    addi x4, x0, 0x1   # pc = 0x1c,  x4 = 0x00000001
    addi x5, x0, 0x1f  # pc = 0x20,  x5 = 0x0000001f

check:
    addi x6, x6, 0x1   # pc = 0x24,  x6 = 0x00000001, then 2
    sll x7, x4, x5     # pc = 0x28,  x7 = 0x00000000, then 0x40000000
    addi x5, x5, -0x1  # pc = 0x2c,  x5 = 0x0000001e, then 0x0000001d
    bge x4, x7, check  # pc = 0x30, branch taken once
    slli x8, x4, 0x1f  # pc = 0x34,  x8 = 0x80000000
    bgeu x8, x4, end   # pc = 0x38, branch taken

skip2:
    addi x9, x0, 0x1   # pc = 0x3c, x9 = 0x00000001 (not executed)

end:
    addi x10, x0, 0x1  # pc = 0x40, x10 = 0x00000001
                       # pc ends at 0x48 (zeros at 0x44)
    ecall
