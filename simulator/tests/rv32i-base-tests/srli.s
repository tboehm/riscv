.globl _start
_start:
addi x1, x0, -1 # x01 = 0xffffffff
srli x2, x1, 1  # x02 = 0x7fffffff
srli x3, x2, 2  # x03 = 0x1fffffff
srli x4, x1, 31 # x04 = 0x00000001
addi x5, x0, -1 # x05 = 0xffffffff
srli x6, x5, 17 # x06 = 0x00007fff
ecall
