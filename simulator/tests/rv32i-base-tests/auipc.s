.globl _start
_start:
auipc x1, 0x1     # x01 = 0x00001000
auipc x2, 0x12    # x02 = 0x00012004
auipc x3, 0x123   # x03 = 0x00123008
auipc x4, 0x1234  # x04 = 0x0123400c
auipc x5, 0x12345 # x05 = 0x12345010
auipc x6, 0xfffff # x06 = 0xfffff014
ecall
