#!/usr/bin/env python3

# Copyright (c) 2019 Trey Boehm
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import argparse
import os
import re
import shutil
import subprocess as sp
import sys

parser = argparse.ArgumentParser(description='Run tests through the simulator')
parser.add_argument('--dir', '-d', default='tests',
                    help='Directory where the tests are located')
parser.add_argument('--simulator', '-s', default='./simulate',
                    help='Simulator binary')
parser.add_argument('--traps', '-t', default='./src/asm/trap.elf',
                    help='Path to trap file')
parser.add_argument('--gen-key', '-g', action='store_true',
                    help='Generate a key file for a test')
parser.add_argument('tests', nargs='*', default=['all'])

red = '\033[1;31m'
green = '\033[32m'
off = '\033[0m'
good = green + 'good' + off
failed = red + 'failed' + off
differ = red + 'differ' + off


def assemble(base_name):
    make_cmd = f'make {base_name}.elf'
    child = sp.run(make_cmd.split(), stdout=sp.DEVNULL)
    return child.returncode


def run_test(simulator, traps, base_name):
    rc = assemble(base_name)
    if rc != 0:
        print(f'Failed to assemble {base_name}')
        return rc

    program = f'{base_name}.elf'
    outfile = f'{base_name}.out'
    testfile = f'{base_name}.test'

    with open(testfile, 'rb') as fp:
        sim_cmds = fp.read()

    with open(outfile, 'w') as fp:
        child = sp.run([simulator, '--traps', traps, program],
                       input=sim_cmds, stdout=fp)
    return child.returncode


def remove_whitespace(string: str):
    '''
    Remove all whitespace from a string.
    '''
    return re.sub('\\s', '', string)


def lines_match(key_line: str, out_line: str):
    '''
    Do two lines match?
    '''
    return remove_whitespace(key_line) == remove_whitespace(out_line)


def diff_strict(key_lines: list, out_lines: list):
    '''
    The key file and output file must match linewise, but may differ in
    whitespace.

    out_lines: Lines from the output file
    key_lines: Lines from the key file

    The indices for differing lines will be the same, but for compatability with
    other diff functions we return it twice.

    return: (key index of first difference: int,
             out index of first difference: int,
             matched: bool)
    '''
    for index, (key_line, out_line) in enumerate(zip(key_lines, out_lines)):
        if not lines_match(key_line, out_line):
            return index, index, False
    # It's possible we have junk at the end of the diff, too.
    if len(key_lines) > len(out_lines):
        return index + 1, index, False
    if len(key_lines) < len(out_lines):
        return index, index + 1, False
    return index, index, True


def diff_at_index(out_lines: list, out_idx: int, key_lines: list):
    '''
    Line-by-line diff starting at the out_idx in the out_lines.

    out_lines: Lines from the output file
    out_idx: Index to start looking for a match in the output
    key_lines: Lines from the key file

    return: (key match index: int,
             out match index: int,
             matched: bool)
    '''
    for key_idx, key_line in enumerate(key_lines):
        out_line = out_lines[out_idx]
        if not lines_match(key_line, out_line):
            return key_idx, out_idx, False
        out_idx += 1
    return key_idx, out_idx, True


def diff_contiguous(key_lines: list, out_lines: list):
    '''
    The key file must be a subset of the output file, but all matching lines
    must be contiguous.

    out_lines: Lines from the output file
    key_lines: Lines from the key file

    return: (key index of best match: int,
             out index of best match: int,
             matched: bool)
    '''
    best_key_idx = 0  # Key index for the longest hunk
    best_out_idx = 0  # Out index for the longest hunk

    for out_idx in range(len(out_lines)):
        match_key_idx, match_out_idx, matched = diff_at_index(out_lines, out_idx, key_lines)
        if matched:
            return match_key_idx, match_out_idx, True
        elif match_key_idx > best_key_idx:
            best_key_idx = match_key_idx
            best_out_idx = match_out_idx
    return best_key_idx, best_out_idx, False


def print_context(key_lines: list, out_lines: list, key_idx: int, out_idx: int):
    '''
    Print the offending lines.
    '''
    if key_idx < len(key_lines):
        print(f'       Key line {key_idx + 1}: {key_lines[key_idx]}')
    if out_idx < len(out_lines):
        print(f'    Output line {out_idx + 1}: {out_lines[out_idx]}')


def diff_subset(key_lines: list, out_lines: list):
    '''
    The key file must be a subset of the output file, and all matching lines
    must be in the same order.

    out_lines: Lines from the output file
    key_lines: Lines from the key file

    return: (key index of unmatchable line: int,
             out index of unmatchable line: int,
             matched: bool)
    '''
    key_idx = 0  # Index for key lines
    out_idx = 0  # Index for output lines

    while key_idx < len(key_lines) and out_idx < len(out_lines):
        key_line = key_lines[key_idx]
        out_line = out_lines[out_idx]
        if lines_match(key_line, out_line):
            # Found a match for this key line, so move on to the next.
            key_idx += 1
        # Always move on to the next output line
        out_idx += 1

    if key_idx == len(key_lines):
        # We made it
        return key_idx, out_idx, True
    return key_idx, out_idx, False


def open_lines(fname):
    with open(fname, 'r') as fp:
        # Strip whitespace from both ends of the line and remove blank lines
        lines = list(filter(lambda l: l, map(str.strip, fp.readlines())))
    return lines

def check_test(base_name):
    outfile = f'{base_name}.out'
    keyfile = f'{base_name}.key'
    out_lines = open_lines(outfile)
    key_lines = open_lines(keyfile)

    # Decide which diff function to use
    if key_lines[0].startswith('diffstyle:'):
        # Specified in the key file
        diff_style = key_lines[0].split(':')[1]
        if diff_style == 'strict':
            diff_func = diff_strict
        elif diff_style == 'contiguous':
            diff_func = diff_contiguous
        elif diff_style == 'subset':
            diff_func = diff_subset
        else:
            print(f'Invalid diff function: {diff_style}. Using subset.')
            diff_func = diff_subset
        del key_lines[0]
    else:
        # Assume diff_subset
        diff_func = diff_subset

    key_idx, out_idx, matched = diff_func(key_lines, out_lines)
    if matched:
        print('{0:{width}} {1}'.format(base_name, good, width=40))
    else:
        print('{0:{width}} {1}'.format(base_name, differ, width=40))
        print_context(key_lines, out_lines, key_idx, out_idx)

    return matched


def all_tests():
    # Paths to all tests
    tests = []

    # Organize the tests into directories ending with '-tests'.
    test_dirs = [os.path.join(test_dir, d)
                 for d in sorted(os.listdir(test_dir)) if d.endswith('-tests')]

    # Run any "applied" and "stress" tests last
    run_last = list()
    for d in test_dirs:
        if "applied" in d or "stress" in d:
            run_last.append(d)
    for d in run_last:
        test_dirs.remove(d)
        test_dirs.append(d)

    # Look inside each directory for files with a '.s' extension. Join
    # those file names with the directory name and add them to the list.
    for d in test_dirs:
        for f in sorted(os.listdir(d)):
            if f.endswith('.s'):
                base_name = os.path.join(d, os.path.splitext(f)[0])
                tests.append(base_name)

    return tests


def generate_key(simulator, traps, base_name):
    '''
    Generate a keyfile for a test.

    The .test and .s files must already exist.
    '''
    keyfile = f'{base_name}.key'
    outfile = f'{base_name}.out'
    rc = run_test(simulator, traps, base_name)
    if rc != 0:
        print(f'Failed to run test {base_name}')
        return

    shutil.move(outfile, keyfile)


if __name__ == '__main__':
    args = parser.parse_args()
    test_dir = args.dir
    simulator = args.simulator
    traps = args.traps
    test_cases = args.tests
    gen_key = args.gen_key

    # Make sure the executable exists, first
    for file in [simulator, traps]:
        if not os.path.exists(file):
            sys.stderr.write(f'error: File {file} not found\n')
            exit(1)

    if gen_key:
        for test in test_cases:
            generate_key(simulator, traps, test)
        exit()

    # Paths to tests to run
    tests = []

    if test_cases != ['all']:
        for test_file in test_cases:
            base_name = os.path.splitext(test_file)[0]
            tests.append(base_name)
    else:
        tests = all_tests()

    # Keep track of which tests we've run and how which tests we've
    # passed. Display this information at the end for the user.
    tests_passed = []
    tests_run = []

    for base_name in tests:
        rc = run_test(simulator, traps, base_name)
        if rc == 0:
            matched = check_test(base_name)
        tests_run.append(base_name)
        if matched:
            tests_passed.append(base_name)

    passed = len(tests_passed)
    total = len(tests_run)

    print(f'\nPassed {passed}/{total} tests.')
    if passed != total:
        exit(1)
