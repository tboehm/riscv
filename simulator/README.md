# mumbo

RISC-V simulator

## About
This is my attempt to simulate the RISC-V instruction set. Currently, there is not very much in place. I have been focusing on code correctness, documentation, and extensibility. Hopefully I can continue with those principles moving forward.

## What works
 * Register-register instructions (ALU operations, shifts, sets)
 * Register-immediate instructinons (ALU operations, shifts, sets)
 * Load and store instructions
 * AUIPC and LUI
 * Jumps
 * Branches

## What's next
 * Hazard detection and stalls
 * Exceptions
 * [RISC-V compliance tests](https://github.com/riscv/riscv-compliance)

I intend to get most of the RV32I base instruction set working before I do much optimization. Some instructions, like the CSRs, FENCEs, and ECALL/EBREAK will come after that is done. Other extensions are even further down the road. The long-term goal for this project is to pass RISC-V compliance tests, but there's still a long way to go before that can happen.

## Dependencies

### General
 * [Clang](http://clang.llvm.org/) or [GCC](https://www.gnu.org/software/gcc) with support for C++11
 * [Make](https://www.gnu.org/software/make)

### Test script
 * [Python](https://www.python.org) 3.6 or higher
 * [RISC-V GNU toolchain](https://github.com/riscv/riscv-gnu-toolchain): Configure this with ``--with-arch=rv32i --with-abi=ilp32`` in order for it to work with the current implementation.

### Documentation
 * [Doxygen](http://www.doxygen.nl)
