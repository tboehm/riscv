/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __ENCODINGS_H__
#define __ENCODINGS_H__

/**
 * @file encodings.h
 *
 * Definitions for instruction encodings.
 *
 * This file contains enumerations for major instruction encoding types,
 * the shift amounts for each field, and the meanings of each opcode and
 * function. There are also structs for each step of the pipeline.
 *
 * @author Trey Boehm
 */

#include "util.h"

#ifdef __cplusplus
/* typedefs for u8, u16, u32, i8, i16, i32 */
using namespace inttypes;
#endif

/// The type of instruction encoding.
#ifdef __cplusplus
enum Instr_Type : u8 {
#else
enum Instr_Type {
#endif
    Unknown_Type = 0, ///< Malformed instructions
    R_Type,           ///< R-type instructions
    I_Type,           ///< I-type instructions
    S_Type,           ///< S-type instructions
    B_Type,           ///< B-type instructions
    U_Type,           ///< U-type instructions
    J_Type,           ///< J-type instructions
};

/// Shift amounts for different instruction fields.
#ifdef __cplusplus
enum Shifts : u8 {
#else
enum Shifts {
#endif
    RD_SHF        = 7,   ///< Used in R, I, U-type
    IMM_4_0_SHF   = 7,   ///< Used in S-type
    F3_SHF        = 12,  ///< Used in R, I, S-type
    IMM_31_12_SHF = 12,  ///< Used in U-type
    RS1_SHF       = 15,  ///< Used in R, I, S-type
    RS2_SHF       = 20,  ///< Used in R, S-type
    IMM_11_0_SHF  = 20,  ///< Used in I-type
    F7_SHF        = 25,  ///< Used in R-type
    IMM_11_5_SHF  = 25,  ///< Used in S-type
};

/// Opcode numbers
#ifdef __cplusplus
enum Opcode_Num : u8 {
#else
enum Opcode_Num {
#endif
    OP_LOAD     = 0x03, ///< Load instructions
    OP_LOAD_FP  = 0x07, ///< FLW/FLD
    /* custom-0:  0x0b */
    OP_MISC_MEM = 0x0f, ///< Miscellaneous memory instructions
    OP_IMM      = 0x13, ///< I-type integer instructions
    OP_AUIPC    = 0x17, ///< AUIPC
    OP_IMM_32   = 0x1b, ///< >32-bit immediate instructions
    /* 48 bits:   0x1f */
    OP_STORE    = 0x23, ///< Store instructions
    OP_STORE_FP = 0x27, ///< FSW/FSD
    /* custom-1:  0x2b */
    OP_AMO      = 0x2f, ///< Atomic (RV32A) instructions
    OP          = 0x33, ///< R-type integer instructions
    OP_LUI      = 0x37, ///< LUI
    OP_32       = 0x3b, ///< >32-bit integer instructions
    /* 64 bits:   0x3f */
    OP_MADD     = 0x43, ///< FMADD.S/D
    OP_MSUB     = 0x47, ///< FMSUB.S/D
    OP_NMSUB    = 0x4b, ///< FNMSUB.S/D
    OP_NMADD    = 0x4f, ///< FNMADD.S/D
    OP_FP       = 0x53, ///< General floating-point instructions
    /* reserved:  0x57 */
    /* custom-2:  0x5b */
    /* 48 bits:   0x5f */
    OP_BRANCH   = 0x63, ///< Branch instructions
    OP_JALR     = 0x67, ///< JALR
    /* reserved:  0x6b */
    OP_JAL      = 0x6f, ///< JAL
    OP_SYSTEM   = 0x73, ///< Floating-point load instructions
    /* reserved:  0x77 */
    /* custom-3:  0x7b */
    /* >=80 bits: 0x7f */
};

/// funct3 meanings (opcode-dependent)
#ifdef __cplusplus
enum Funct3_Num : u8 {
#else
enum Funct3_Num {
#endif
    F3_JALR    = 0x0,
    F3_BEQ     = 0x0,
    F3_BNE     = 0x1,
    F3_BLT     = 0x4,
    F3_BGE     = 0x5,
    F3_BLTU    = 0x6,
    F3_BGEU    = 0x7,
    F3_LB      = 0x0,
    F3_LH      = 0x1,
    F3_LW      = 0x2,
    F3_LBU     = 0x4,
    F3_LHU     = 0x5,
    F3_SB      = 0x0,
    F3_SH      = 0x1,
    F3_SW      = 0x2,
    F3_ADD     = 0x0,
    F3_SHFL    = 0x1,
    F3_SLT     = 0x2,
    F3_SLTU    = 0x3,
    F3_XOR     = 0x4,
    F3_SHFR    = 0x5,
    F3_OR      = 0x6,
    F3_AND     = 0x7,
    F3_FENCE   = 0x0,
    F3_FENCE_I = 0x1,
    F3_ECALL   = 0x0,
    F3_EBREAK  = 0x0,
    F3_CSRRW   = 0x1,
    F3_CSRRS   = 0x2,
    F3_CSRRC   = 0x3,
    F3_CSRRWI  = 0x5,
    F3_CSRRSI  = 0x6,
    F3_CSRRCI  = 0x7,
    F3_CSRI_M  = 0x4, ///< CSR immediate instructions will have this bit set
    F3_CSR_M   = 0x3, ///< CSR instructions will have either of these bits set
    F3_MUL     = 0x0,
    F3_MULH    = 0x1,
    F3_MULHSU  = 0x2,
    F3_MULHU   = 0x3,
    F3_DIV     = 0x4,
    F3_DIVU    = 0x5,
    F3_REM     = 0x6,
    F3_REMU    = 0x7,
    F3_AMO     = 0x2,
    F3_FP      = 0x2,
};

/// funct7 meanings (opcode-dependent)
#ifdef __cplusplus
enum Funct7_Num : u8 {
#else
enum Funct7_Num {
#endif
    F7_NEG       = 0x20, ///< SUB, SRA, and SRAI
    F7_POS       = 0x00, ///< Most arithmetic/logical/shift
    F7_MUL       = 0x01, ///< Multiplication/division
    F7_LR_W      = 0x08, ///< Atomic load-reserved
    F7_SC_W      = 0x0c, ///< Atomic store-conditional
    F7_AMOSWAP_W = 0x04, ///< Atomic swap
    F7_AMOADD_W  = 0x00, ///< Atomic add
    F7_AMOXOR_W  = 0x10, ///< Atomic xor
    F7_AMOAND_W  = 0x30, ///< Atomic and
    F7_AMOOR_W   = 0x20, ///< Atomic or
    F7_AMOMIN_W  = 0x40, ///< Atomic signed minimum
    F7_AMOMAX_W  = 0x50, ///< Atomic signed maximum
    F7_AMOMINU_W = 0x60, ///< Atomic unsigned minimum
    F7_AMOMAXU_W = 0x70, ///< Atomic unsigned maximum
    F7_FADD_S    = 0x00, ///< Single-precision FP add
    F7_FSUB_S    = 0x04, ///< Single-precision FP sub
    F7_FMUL_S    = 0x08, ///< Single-precision FP mul
    F7_FDIV_S    = 0x0c, ///< Single-precision FP div
    F7_FSQRT_S   = 0x2c, ///< Single-precision FP sqrt
    F7_FSGNJ_S   = 0x10,
    F7_FMIN_S    = 0x14,
    F7_FMAX_S    = 0x14,
    F7_FCVT_W_S  = 0x60,
    F7_FMV_X_W   = 0x70,
    F7_FCMP_S    = 0x50,
    F7_FCLASS_S  = 0x70,
    F7_FCVT_S_W  = 0x68,
    F7_FMV_W_X   = 0x78,
};

/// Functional unit (used in the decode and execute stages)
#ifdef __cplusplus
enum F_Unit_Num : u8 {
#else
enum F_Unit_Num {
#endif
    F_UNIT_NOP, ///< No functional unit needed
    F_UNIT_ALU, ///< Addition/subtraction and bitwise logic
    F_UNIT_SHF, ///< Shifts
    F_UNIT_CMP, ///< Comparisons
};

/// CSR names and addresses (section 2.2 of the privileged ISA)
#ifdef __cplusplus
enum CSR : u16 {
#else
enum CSR {
#endif
    // User trap setup
    CSR_USTATUS = 0x000, ///< User status register
    CSR_UIE     = 0x004, ///< User interrupt-enable register
    CSR_UTVEC   = 0x005, ///< User trap handler base address

    // User trap handling
    CSR_USCRATCH = 0x040, ///< Scratch register for user trap handlers
    CSR_UEPC     = 0x041, ///< User exception program counter
    CSR_UCAUSE   = 0x042, ///< User trap cause
    CSR_UTVAL    = 0x043, ///< User bad address or instruction
    CSR_UIP      = 0x044, ///< User interrupt pending

    // User floating-point CSRs
    CSR_FFLAGS = 0x001, ///< Floating-point accrued exceptions
    CSR_FRM    = 0x002, ///< Floating-point dynamic rounding mode
    CSR_FCSR   = 0x003, ///< Floating-point CSR (frm + fflags)

    // User counters/timers
    CSR_CYCLE         = 0xc00, ///< Cycle counter for RDCYCLE instruction
    CSR_TIME          = 0xc01, ///< Timer for RDTIME instruction
    CSR_INSTRET       = 0xc02, ///< Instructions-retired counter for RDINSTRET instruction
    CSR_HPMCOUNTER3   = 0xc03, ///< Performance-monitoring counter
    CSR_HPMCOUNTER4   = 0xc04, ///< Performance-monitoring counter
    // ...
    CSR_HPMCOUNTER31  = 0xc1f, ///< Performance-monitoring counter
    CSR_CYCLEH        = 0xc80, ///< Upper 32 bits of cycle, RV32I-only
    CSR_TIMEH         = 0xc81, ///< Upper 32 bits of time, RV32I-only
    CSR_INSTRETH      = 0xc82, ///< Upper 32 bits of instret, RV32I-only
    CSR_HPMCOUNTER3H  = 0xc83, ///< Upper 32 bits of hpcmcounter3, RV32I-only
    CSR_HPMCOUNTER4H  = 0xc84, ///< Upper 32 bits of hpcmcounter4, RV32I-only
    // ...
    CSR_HPMCOUNTER31H = 0xc9f, ///< Upper 32 bits of hpmcounter31, RV32I-only

    // Supervisor trap setup
    CSR_SSTATUS    = 0x100, ///< Supervisor status register
    CSR_SEDELEG    = 0x102, ///< Supervisor exception delegation register
    CSR_SIDELEG    = 0x103, ///< Supervisor interrupt delegation register
    CSR_SIE        = 0x104, ///< Supervisor interrupt-enable register
    CSR_STVEC      = 0x105, ///< Supervisor trap handler base address
    CSR_SCOUNTEREN = 0x106, ///< Supervisor counter enable

    // Supervisor trap handling
    CSR_SSCRATCH = 0x140, ///< Scratch register for supervisor trap handlers
    CSR_SEPC     = 0x141, ///< Supervisor exception program counter
    CSR_SCAUSE   = 0x142, ///< Supervisor trap cause
    CSR_STVAL    = 0x143, ///< Supervisor bad address or instruction
    CSR_SIP      = 0x144, ///< Supervisor interrupt pending

    // Supervisor protection and translation
    CSR_SATP = 0x180, ///< Supervisor address translation and protection

    // Hypervisor trap setup
    CSR_HSTATUS = 0xa00, ///< Hypervisor status register
    CSR_HEDELEG = 0xa02, ///< Hypervisor exception delegation register
    CSR_HIDELEG = 0xa03, ///< Hypervisor interrupt delegation register

    // Hypervisor protection and translation
    CSR_HGATP = 0xa80, ///< Hypervisor guest address translation and protection

    // Hypervisor background supervisor registers
    CSR_BSSTATUS  = 0x200, ///< Background supervisor status register
    CSR_BSIE      = 0x204, ///< Background supervisor interrupt-enable register
    CSR_BSTVEC    = 0x205, ///< Background supervisor trap handler base address
    CSR_BSSCRATCH = 0x240, ///< Background supervisor scratch register
    CSR_BSEPC     = 0x241, ///< Background supervisor exception program counter
    CSR_BSCAUSE   = 0x242, ///< Background supervisor trap cause
    CSR_BSTVAL    = 0x243, ///< Background supervisor bad address or translation
    CSR_BSIP      = 0x244, ///< Background supervisor interrupt pending
    CSR_BSATP     = 0x280, ///< Background supervisor address translation and protection

    // Machine information registers
    CSR_MVENDORID = 0xf11, ///< Vendor ID
    CSR_MARCHID   = 0xf12, ///< Architecture ID
    CSR_MIMPID    = 0xf13, ///< Implementation ID
    CSR_MHARTID   = 0xf14, ///< Hardware thread ID

    // Machine trap setup
    CSR_MSTATUS    = 0x300, ///< Machine status register
    CSR_MISA       = 0x301, ///< Machine ISA and extensions
    CSR_MEDELEG    = 0x302, ///< Machine exception delegation register
    CSR_MIDELEG    = 0x303, ///< Machine interrupt delegation register
    CSR_MIE        = 0x304, ///< Machine interrupt-enable register
    CSR_MTVEC      = 0x305, ///< Machine trap-handler base address
    CSR_MCOUNTEREN = 0x306, ///< Machine counter enable

    // Machine trap handling
    CSR_MSCRATCH = 0x340, ///< Scratch register for machine trap handlers
    CSR_MEPC     = 0x341, ///< Machine exception program counter
    CSR_MCAUSE   = 0x342, ///< Machine trap cause
    CSR_MTVAL    = 0x343, ///< Machine bad address or instruction
    CSR_MIP      = 0x344, ///< Machine interrupt pending

    // Machine memory protection
    CSR_PMPCFG0   = 0x3a0, ///< Physical memory protection configuration
    CSR_PMPCFG1   = 0x3a1, ///< Physical memory protection configuration, RV32 only
    CSR_PMPCFG2   = 0x3a2, ///< Physical memory protection configuration
    CSR_PMPCFG3   = 0x3a3, ///< Physical memory protection configuration, RV32 only
    CSR_PMPADDR0  = 0x3b0, ///< Physical memory protection address register
    CSR_PMPADDR1  = 0x3b1, ///< Physical memory protection address register
    // ...
    CSR_PMPADDR15 = 0x3bf, ///< Physical memory protection address register

    // Machine counters/timers
    CSR_MCYCLE         = 0xb00, ///< Machine cycle counter
    CSR_MINSTRET       = 0xb02, ///< Machine instructions-retired counter
    CSR_MHPMCOUNTER3   = 0xb03, ///< Machine performance-monitoring counter
    CSR_MHPMCOUNTER4   = 0xb04, ///< Machine performance-monitoring counter
    // ...
    CSR_MHPMCOUNTER31  = 0xb1f, ///< Machine performance-monitoring counter
    CSR_MCYCLEH        = 0xb80, ///< Upper 32 bits of mcycle, RV32I-only
    CSR_MINSTRETH      = 0xb82, ///< Upper 32 bits of minstret, RV32I-only
    CSR_MHPMCOUNTER3H  = 0xb83, ///< Upper 32 bits of mhpcmcounter3, RV32I-only
    CSR_MHPMCOUNTER4H  = 0xb84, ///< Upper 32 bits of mhpcmcounter4, RV32I-only
    // ...
    CSR_MHPMCOUNTER31H = 0xb9f, ///< Upper 32 bits of mhpmcounter31, RV32I-only

    // Machine counter setup
    CSR_MCOUNTINHIBIT = 0x320, ///< Machine counter-inhibit register
    CSR_MHPMEVENT3    = 0x323, ///< Machine performance-monitoring event selector
    CSR_MHPMEVENT4    = 0x324, ///< Machine performance-monitoring event selector
    // ...
    CSR_MHPMEVENT31   = 0x33f, ///< Machine performance-monitoring event selector

    // Debug/trace registers (shared with debug mode)
    CSR_TSELECT = 0x7a0, ///< Debug/trace trigger register select
    CSR_TDATA1  = 0x7a1, ///< First debug/trace trigger data register
    CSR_TDATA2  = 0x7a2, ///< Second debug/trace trigger data register
    CSR_TDATA3  = 0x7a3, ///< Third debug/trace trigger data register

    // Debug mode registers
    CSR_DCSR     = 0x7b0, ///< Debug control and status register
    CSR_DPC      = 0x7b1, ///< Debug PC
    CSR_DSCRATCH = 0x7b2, ///< Debug scratch register
};

/// Encoding of "Extensions" field in the misa register.
#ifdef __cplusplus
enum Extension_Field : u32 {
#else
enum Extension_Field {
#endif
    EXTENSION_A = 0x00000000, ///< Atomic extension
    EXTENSION_B = 0x00000001, ///< Tentatively reserved for bit-manipulation extension
    EXTENSION_C = 0x00000002, ///< Compressed extension
    EXTENSION_D = 0x00000004, ///< Double-precision floating-point extension
    EXTENSION_E = 0x00000008, ///< RV32E base ISA
    EXTENSION_F = 0x00000010, ///< Single-precision floating-point extension
    EXTENSION_G = 0x00000020, ///< Additional standard extensions present
    EXTENSION_H = 0x00000040, ///< Hypervisor extension
    EXTENSION_I = 0x00000080, ///< RV32I/64I/128I base ISA
    EXTENSION_J = 0x00000100, ///< Tentatively reserved for Dynamically Translated Languages extension
    EXTENSION_K = 0x00000200, ///< Reserved
    EXTENSION_L = 0x00000400, ///< Tentatively reserved for Decimal Floating-Point extension
    EXTENSION_M = 0x00000800, ///< Integer multiply/divide extension
    EXTENSION_N = 0x00001000, ///< User-level interrupts supported
    EXTENSION_O = 0x00002000, ///< Reserved
    EXTENSION_P = 0x00004000, ///< Tentatively reserved for Packed-SIMD extension
    EXTENSION_Q = 0x00008000, ///< Quad-precision floating-point extension
    EXTENSION_R = 0x00010000, ///< Reserved
    EXTENSION_S = 0x00020000, ///< Supervisor mode implemented
    EXTENSION_T = 0x00040000, ///< Tentatively reserved for Transactional Memory extension
    EXTENSION_U = 0x00080000, ///< User mode implemented
    EXTENSION_V = 0x00100000, ///< Tentatively reserved for Vector extension
    EXTENSION_W = 0x00200000, ///< Reserved
    EXTENSION_X = 0x00400000, ///< Non-standard extensions present
    EXTENSION_Y = 0x00800000, ///< Reserved
    EXTENSION_Z = 0x01000000, ///< Reserved
};

/// Encoding of "MXL" field in the misa register.
#ifdef __cplusplus
enum MXL_Field : u32 {
#else
enum MXL_Field {
#endif
    MXL_32  = 0x40000000, ///< 32-bit XLEN
    MXL_64  = 0x80000000, ///< 64-bit XLEN
    MXL_128 = 0xc0000000, ///< 128-bit XLEN
};

/// Exception numbers
#ifdef __cplusplus
enum Exception_Number : u8 {
#else
enum Exception_Number {
#endif
    // Instructions
    EXC_INSTR_ADDR_MISALGN     = 0, ///< Instruction address misaligned
    EXC_INSTR_ADDR_FAULT       = 1, ///< Instruction access fault
    EXC_ILLEGAL_INSTR          = 2, ///< Illegal instruction
    EXC_BREAKPOINT             = 3, ///< Breakpoint

    // Load/store/AMO accesses
    EXC_LOAD_ADDR_MISALGN      = 4, ///< Load address misaligned
    EXC_LOAD_ACCESS_FAULT      = 5, ///< Load access fault
    EXC_STORE_AMO_ADDR_MISALGN = 6, ///< Store/AMO address misaligned
    EXC_STORE_AMO_ACCESS_FAULT = 7, ///< Store/AMO access fault

    // ECALL
    EXC_ECALL_U_MODE           = 8,  ///< Environment call from U-mode
    EXC_ECALL_H_MODE           = 9,  ///< Environment call from H-mode
    EXC_RESERVED_10            = 10, ///< Reserved
    EXC_ECALL_M_MODE           = 11, ///< Environment call from M-mode

    // Page faults
    EXC_INSTR_PAGE_FAULT       = 12, ///< Instruction page fault
    EXC_LOAD_PAGE_FAULT        = 13, ///< Load page fault
    EXC_RESERVED_14            = 14, ///< Reserved for future standard use
    EXC_STORE_AMO_PAGE_FAULT   = 15, ///< Store/AMO page fault

    // Reserved
    //                        16-23, // Reserved for future standard use
    //                        24-31, // Reserved for custom use
    //                        32-47, // Reserved for future standard use
    //                        48-63, // Reserved for custom use
    //                        >= 64, // Reserved for future standard use
};

/// Interrupt numbers
#ifdef __cplusplus
enum Interrupt_Number : u8 {
#else
enum Interrupt_Number {
#endif
    // Software interrupts
    INT_USER_SOFTWARE       = 0, ///< User software interrupt
    INT_SUPERVISOR_SOFTWARE = 1, ///< Supervisor software interrupt
    INT_RESERVED_2          = 2, ///< Reserved for future standard use
    INT_MACHINE_SOFTWARE    = 3, ///< Machine software interrupt

    // Timer interrupts
    INT_USER_TIMER          = 4, ///< User timer interrupt
    INT_SUPERVISOR_TIMER    = 5, ///< Supervisor timer interrupt
    INT_RESERVED_6          = 6, ///< Reserved
    INT_MACHINE_TIMER       = 7, ///< Machine timer interrupt

    // External interrupts
    INT_USER_EXTERNAL       = 8,  ///< User external interrupt
    INT_SUPERVISOR_EXTERNAL = 9,  ///< Supervisor external interrupt
    INT_RESERVED_10         = 10, ///< Reserved
    INT_MACHINE_EXTERNAL    = 11, ///< Machine external interrupt

    // Reserved
    //                      12-15, // Reserved for future standard use
    //                      >= 16, // Reserved for platform use
};

/// The high bit of mcause indicates the trap is an interrupt
#define INTERRUPT_MASK 0x80000000

#endif // __ENCODINGS_H__
