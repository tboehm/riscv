/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "assertions.h"

#include <vector>

#include "encodings.h"
#include "util.h"

using namespace inttypes;

/// Opcodes for which at least one instruction is implemented.
static const u8 _opcodes[] = {
    OP_LOAD,
    OP_IMM,
    OP_AUIPC,
    OP_STORE,
    OP,
    OP_LUI,
    OP_JALR,
    OP_JAL
};

static const std::vector<u8> ImplementedOpcodes(begin(_opcodes), end(_opcodes));

/// funct7 bits for which at least one instruction is implemented.
static const u8 _funct7[] = {
    F7_NEG,
    F7_POS
};

static const std::vector<u8> ImplementedFunct7(begin(_funct7), end(_funct7));

/// funct7 bits for which at least one instruction is implemented.
static const u8 _funct3[] = {
    F3_JALR,
    F3_BEQ,
    F3_BNE,
    F3_BLT,
    F3_BGE,
    F3_BLTU,
    F3_BGEU,
    F3_LB,
    F3_LH,
    F3_LW,
    F3_LBU,
    F3_LHU,
    F3_SB,
    F3_SH,
    F3_SW,
    F3_ADD,
    F3_SHFL,
    F3_SLT,
    F3_SLTU,
    F3_XOR,
    F3_SHFR,
    F3_OR,
    F3_AND
};

static const std::vector<u8> ImplementedFunct3(begin(_funct3), end(_funct3));

/**
 * Allowable functional units in execute stage.
 *
 * I never really expect this assertion to fail, but it's nice to have
 * this here nonetheless.
 */
static const u8 _funits[] = {
    F_UNIT_NOP,
    F_UNIT_ALU,
    F_UNIT_SHF,
    F_UNIT_CMP
};

static const std::vector<u8> ImplementedFunctionalUnits(begin(_funits), end(_funits));

bool Aligned_MemoryF3(u32 addr, u8 funct3)
{
    u8 width = 1 << (funct3 & 0x3);

    return Aligned_MemoryWidth(addr, width);
}

bool Aligned_MemoryWidth(u32 addr, u8 width)
{
    switch (width) {
        case 1:
            return true;
        case 2:
            return HALFWORD_ALIGNED(addr);
        case 4:
            return WORD_ALIGNED(addr);
        default:
            return false;
    }
}

static bool CheckValid(u8 arg, const std::vector<u8> arr)
{
    for (u32 i = 0; i < arr.size(); i++) {
        if (arg == arr[i]) {
            return true;
        }
    }
    return false;
}

bool Valid_Opcode(u8 opcode)
{
    return CheckValid(opcode, ImplementedOpcodes);
}

bool Valid_Funct7(u8 funct7)
{
    return CheckValid(funct7, ImplementedFunct7);
}

bool Valid_Funct3(u8 funct3)
{
    return CheckValid(funct3, ImplementedFunct3);
}

bool Valid_FUnit(u8 f_unit)
{
    return CheckValid(f_unit, ImplementedFunctionalUnits);
}
