/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __UTIL_H__
#define __UTIL_H__

/**
 * @file util.h
 *
 * Various typedefs and useful macros.
 *
 * Use the Rust convention for referring to integer sizes/signedness
 * explicitly. The macros help with bit twiddling (masking, selecting,
 * sign extending) and other basic tasks (array length, round-up
 * division).
 *
 * @author Trey Boehm
 */

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
// Some of these conflict with C++ builtin types, so I put them in a namespace.
namespace inttypes {
    using u8  = uint8_t;  ///< Unsigned byte
    using u16 = uint16_t; ///< Unsigned short
    using u32 = uint32_t; ///< Unsigned word
    using u64 = uint64_t; ///< Unsigned double word
    using i8  = int8_t;   ///< Signed byte
    using i16 = int16_t;  ///< Signed short
    using i32 = int32_t;  ///< Signed word
    using i64 = int64_t;  ///< Signed double word
};
#else
typedef uint8_t u8;   ///< Unsigned byte
typedef uint16_t u16; ///< Unsigned short
typedef uint32_t u32; ///< Unsigned word
typedef uint64_t u64; ///< Unsigned double word
typedef int8_t i8;    ///< Signed byte
typedef int16_t i16;  ///< Signed short
typedef int32_t i32;  ///< Signed word
typedef int64_t i64;  ///< Signed double word
#endif

/**
 * Rounded up division.
 *
 * Divide x by y and round up the result. This only works when x and y
 * have the same sign.
 */
#define ROUND_UP_DIV(x, y) ((x + (y-1)) / y)

/**
 * Select a range of bits.
 *
 * Mask out a range of bits and shift it to the lower part of the
 * number. This *only* works for unsigned words!
 *
 * Example:
 * BIT_SEL(0x1265, 11, 9) = 0x1265[11:9] = 001
 */
#define BIT_SEL(x, hi, lo) ((((u32)x << (31-hi)) >> (31-hi)) >> lo)

/**
 * Apply a mask to a range of bits.
 *
 * Select a range of bits and leave them in place.
 *
 * Example:
 * MASK(0x1265, 11, 9) = 0000.0010.0000.0000
 */
#define MASK(x, hi, lo) (BIT_SEL(x, hi, lo) << lo)


/**
 * Length of an array.
 *
 * I took this one straight from the Kernel style guide.
 */
#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

/**
 * Sign-extend an integer.
 *
 * Specify the integer's width and sign-extend. This macro only works
 * for 32-bit integers.
 */
#define SEXT(num, width) ((((i32)num) << (32 - width)) >> (32-width))

#ifdef __cplusplus
/// Vector initialization (start of array) for LLVM compatability
template <typename T, size_t N>T* begin(T(&arr)[N]) { return &arr[0]; }

/// Vector initialization (end of array) for LLVM compatability
template <typename T, size_t N>T* end(T(&arr)[N]) { return &arr[0]+N; }
#endif // __cplusplus

#endif // __UTIL_H__
