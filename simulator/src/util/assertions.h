/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __ASSERTIONS_H__
#define __ASSERTIONS_H__

/**
 * @file assertions.h
 *
 * Explicitly declare what pieces have been implemented.
 *
 * Based on what has been implemented, the assertions (particularly in
 * the decode stage) will be different. This file makes it easy
 * (hopefully!) to configure that.
 *
 * Once I actually have different extensions implemented, these will be
 * based on whichever I choose to enable. For example, a single #define
 * somewhere would switch all RV32F instructions on or off.
 *
 * @author Trey Boehm
 */

#include "util.h"

using namespace inttypes;

/**
 * Determine whether an address is halfword-aligned
 *
 * Use this in an assertion when performing halfword memory accesses.
 */
#define HALFWORD_ALIGNED(addr) ((addr & 0x1) == 0)

/**
 * Determine whether an address is word-aligned
 *
 * Use this in an assertion when performing word memory accesses.
 */
#define WORD_ALIGNED(addr) ((addr & 0x3) == 0)

/**
 * Determine whether a PC is aligned.
 *
 * For RV32I, the PC must be word-aligned. Use this macro inside an
 * assert to check it.
 *
 * TODO: When RV32C is implemented, have an #ifdef block to set this to
 * use either WORD_ALIGNED or HALFWORD_ALIGNED.
 */
#define PC_ALIGNED(pc) (WORD_ALIGNED(pc))

/**
 * Check a memory access for alignment using funct3 bits.
 *
 * @return true if the memory access is aligned, otherwise false.
 */
bool Aligned_MemoryF3(
    u32 addr, ///< Address to check
    u8 funct3 ///< funct3 bits from the instruction
);

/**
 * Check a memory access for alignment using its width.
 *
 * TODO: If RV64 is implemented, have a LONG_ALIGNED or equivalent that
 * this function can use.
 *
 * @return true if the memory access is aligned, otherwise false.
 */
bool Aligned_MemoryWidth(
    u32 addr, ///< Address to check
    u8 width  ///< Width, in bytes
);

/**
 * Check if an opcode is valid.
 *
 * @return True if the opcode is implemented, otherwise false.
 */
bool Valid_Opcode(
    u8 opcode ///< The opcode to check
);

/**
 * Check if some funct7 bits are (potentially) valid.
 *
 * Not all funct7 bits make sense with all instructions, but it is
 * useful to check nonetheless.
 *
 * @return True if the funct7 bits are valid, otherwise false.
 */
bool Valid_Funct7(
    u8 funct7 ///< The funct7 bits to check
);

/**
 * Check if some funct3 bits are (potentially) valid.
 *
 * Not all funct3 bits make sense with all instructions, but it is
 * useful to check nonetheless.
 *
 * @return True if the funct3 bits are valid, otherwise false.
 */
bool Valid_Funct3(
    u8 funct3 ///< The funct3 bits to check
);

/**
 * Check if the functional unit (exec stage) is valid.
 *
 * @return True if the functional unit is valid, otherwise false.
 */
bool Valid_FUnit(
    u8 f_unit ///< Functional unit number
);

#endif // __ASSERTIONS_H__
