/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "fetch_stage.h"

#include "assertions.h"
#include "encodings.h"
#include "memory.h"
#include "processor.h"
#include "util.h"

using namespace inttypes;

void
CPU::Fetch_Stage(void)
{
    ////////////////////////////////////
    //          STAGE LOGIC           //
    ////////////////////////////////////

    fetch_latches &fetch = next_fetch.stage_latches;
    exec_latches &exec = prev_execute.stage_latches;

    bool ready;
    u32 pc;
    u32 link;
    u32 ir;

    pc = regread(PC_NUM);
    assert(PC_ALIGNED(pc)); // TODO: Misaligned fetch exception
    link = pc + 4;
    ready = mem_load(ir, pc, F3_LW);
    regwrite(IR_NUM, ir); // This is only meaningful for simulation/debugging.

    fetch.pc   = pc;
    fetch.link = link;
    fetch.ir   = ir;

    ////////////////////////////////////
    //       PIPELINE CONTROL         //
    ////////////////////////////////////

    // STALLS: Memory
    stalls.fetch_mem_stall = !ready;

    // VALID BIT: No stalls anywhere in the pipeline
    //  1) Memory delays here in fetch or in memory
    //  2) Data dependencies in decode
    //  3) Branch/jump instructions in decode, execute, or memory
    //  4) ECALL instruction anywhere in the pipeline
    bool mem_stall = stalls.fetch_mem_stall || stalls.memory_mem_stall;
    bool dep_stall = stalls.decode_dep_stall;
    bool br_stall = stalls.decode_br_stall || stalls.execute_br_stall
                 || stalls.memory_br_stall;
    bool ei_stall = stalls.decode_ei_stall || stalls.execute_ei_stall
                 || stalls.memory_ei_stall;
    bool ca_stall = stalls.decode_ca_stall || stalls.execute_ca_stall
                 || stalls.memory_ca_stall;
    fetch.valid = !(mem_stall || dep_stall || br_stall || ei_stall || ca_stall);

    // PC FORWARDING: PC+4, branch PC from exec
    if (exec.valid && exec.ctrl) {
        regwrite(PC_NUM, prev_execute.stage_latches.addr);
    } else if (fetch.valid) {
        regwrite(PC_NUM, link);
    }

    // PROPAGATE: No [data] memory stall or data dependency
    bool propagate = !(dep_stall || stalls.memory_mem_stall);
    if (propagate) {
        prev_fetch.stage_latches = next_fetch.stage_latches;
    }
}
