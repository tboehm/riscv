/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "csr.h"

#include <map>

#include "encodings.h"
#include "util.h"

using namespace inttypes;

struct csr *
CSRFile::byaddr(u16 addr)
{
    csr_mapping::iterator it;

    it = this->csr_file.find(addr);
    if (it != this->csr_file.end()) {
        return &it->second;
    } else {
        return nullptr;
    }
}

struct csr *
CSRFile::byname(const char *name)
{
    csr_mapping::iterator it;

    for (it = this->csr_file.begin(); it != this->csr_file.end(); it++) {
        if (it->second.name == name) {
            return &it->second;
        }
    }

    return nullptr;
}

CSRFile::CSRFile(void)
{
    struct csr c;

    for (size_t i = 0; i < NUM_CSRS; i++) {
        c = CSR_INIT[i];
        this->csr_file[c.addr] = c;
    }
}

void
CSRFile::show(void)
{
    for (auto it = this->csr_file.cbegin(); it != this->csr_file.end(); it++) {
        printf("%20s (0x%.3x) = 0x%.8x\n", it->second.name, it->first,
               it->second.data);
    }
}

void
CSRFile::print(u16 addr)
{
    struct csr *c = this->byaddr(addr);
    if (c != nullptr) {
        printf("%20s (0x%.3x) = 0x%.8x\n", c->name, addr, c->data);
    }
}

bool
CSRFile::write(u16 addr, u32 data)
{
    struct csr *c = this->byaddr(addr);
    if (c != nullptr) {
        c->data = data;
        return true;
    } else {
        return false;
    }
}

u32
CSRFile::read(u16 addr)
{
    struct csr *c = this->byaddr(addr);
    if (c != nullptr) {
        return c->data;
    } else {
        return 0;
    }
}

const char *
CSRFile::getname(u16 addr)
{
    struct csr *c = this->byaddr(addr);
    if (c != nullptr) {
        return c->name;
    } else {
        return nullptr;
    }
}

bool
CSRFile::implemented(u16 addr)
{
    return (this->byaddr(addr) != nullptr);
}
