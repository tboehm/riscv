/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "processor.h"

#include <assert.h>
#include <iostream>
#include <stdio.h>
#include <string.h>

#include "assertions.h"
#include "color.h"
#include "csr.h"
#include "memory.h"
#include "util.h"

using namespace inttypes;

CPU::CPU(u32 entry, bool interactive)
{
    this->running = false;
    this->power_on = true;
    this->verbose = false;
    this->delta = interactive;
    this->color = interactive;
    this->nice = interactive;

    this->prev_fetch.clear();
    this->next_fetch.clear();
    this->prev_decode.clear();
    this->next_decode.clear();
    this->prev_execute.clear();
    this->next_execute.clear();
    this->prev_memory.clear();
    this->next_memory.clear();

    memset(&(this->stalls), 0, sizeof(stall_signals));

    this->csr_file = CSRFile();

    for (size_t i = 0; i < REG_COUNT; i++) {
        this->Registers[i] = 0;
    }

    Registers[PC_NUM] = entry;
}

void
CPU::rshow(const char *name, u8 rnum)
{
    u32 rvalue = this->Registers[rnum];
    if (this->color) {
        Color_Set(COLOR_BLUE, COLOR_DEFAULT, STYLE_NONE);
    }
    printf("%7s", name);
    if (this->color) {
        Color_Reset();
    }
    printf(" = ");
    // Color all nonzero registers
    if (this->color && rvalue != 0) {
        Color_Set(COLOR_MAGENTA, COLOR_DEFAULT, STYLE_NONE);
    }
    // Color the last register that was written
    if (this->color && rnum == this->next_memory.stage_latches.rd) {
        Color_Set(COLOR_YELLOW, COLOR_DEFAULT, STYLE_BOLD);
    }
    // Print a single 0 instead of 0x00000000
    if (this->nice && rvalue == 0) {
        printf("0%9s", " ");
    } else {
        printf("0x%.8x", rvalue);
    }
    if (this->color) {
        Color_Reset();
    }
}

void
CPU::rdump(u8 cols, bool abi)
{
    u8 rows;
    u8 rnum;
    const char **names;
    const char *name;

    /* columns must evenly divide 32 (the number of GP registers */
    if (GP_REGS % cols != 0) {
        cols = 1;
    }
    rows = GP_REGS / cols;

    /* Set which register names to use (ABI or x0, x1, ...) */
    if (abi) {
        names = ABIRegNames;
    } else {
        names = GPRegNames;
    }

    printf("Processor's internal state:\n");
    for (size_t i = 0; i < rows; i++) {
        for (size_t j = 0; j < cols; j++) {
            rnum = i + j * rows;
            name = names[rnum];
            rshow(name, rnum);
        }
        putchar('\n');
    }
    if (this->nice) {
        putchar('\n');
    }
    rshow("PC", PC_NUM);
    putchar('\n');
}

void
CPU::regwrite(u8 regnum, u32 data)
{
    assert(regnum < REG_COUNT);
    if (regnum == 0) {
        return;
    } else {
        this->Registers[regnum] = data;
    }
}

u32
CPU::regread(u8 regnum)
{
    assert(regnum < REG_COUNT);
    return this->Registers[regnum];
}

void
CPU::set_verbose(bool verbosity)
{
    this->verbose = verbosity;
}

void
CPU::set_delta(bool delta)
{
    this->delta = delta;
}

void
CPU::set_color(bool color)
{
    this->color = color;
}

void
CPU::set_nice(bool nice)
{
    this->nice = nice;
}

void
CPU::halt(void)
{
    printf("Simulator halted.\n");
    this->running = false;
    this->power_on = false;
}

void
CPU::cycle(void)
{
    CPU &core = *this;

    core.Writeback_Stage();
    core.Memory_Stage();
    core.Execute_Stage();
    core.Decode_Stage();
    core.Fetch_Stage();

    // Halt when ECALL is about to reach writeback
    if (core.next_memory.stage_latches.valid
     && core.next_memory.stage_latches.ecall) {
        core.halt();
        return;
    }

    if (core.verbose) {
        core.pdump();
    }
}

void
CPU::run(u32 num_cycles)
{
    CPU &core = *this;
    if (!core.power_on) {
        core.halt();
        return;
    }

    core.running = true;
    if (num_cycles == 0) {
        while (core.running) {
            core.cycle();
        }
    } else {
        for (u32 i = 0; core.running && (i < num_cycles); i++) {
            core.cycle();
        }
    }
    core.running = false;
}

// TODO: Additional structs/arrays for the latches to make printing easier
void
CPU::print_fetch_latches(void)
{
    fetch_latches &fetch = this->next_fetch.stage_latches;
    printf("Fetch latches:\n");
    printf("    valid = %d\n", fetch.valid);
    printf("    pc = 0x%.8x\n", fetch.pc);
    printf("    link = 0x%.8x\n", fetch.link);
    printf("    ir = 0x%.8x\n", fetch.ir);
}

void
CPU::print_dec_latches(void)
{
    dec_latches &decode = this->next_decode.stage_latches;
    printf("Decode latches:\n");
    printf("    valid = %d\n", decode.valid);
    printf("    ctrl = %.1d\n", decode.ctrl);
    printf("    ecall = %.1d\n", decode.ecall);
    printf("    opcode = 0x%.2x\n", decode.opcode);
    printf("    rd = %u\n", decode.rd);
    printf("    alu_a = 0x%.8x\n", decode.alu_a);
    printf("    alu_b = 0x%.8x\n", decode.alu_b);
    printf("    c_in = %d\n", decode.c_in);
    printf("    alu_op = 0x%x\n", decode.alu_op);
    printf("    alu_addr = %d\n", decode.alu_addr);
    printf("    invert_a = %d\n", decode.invert_a);
    printf("    cmp_a = 0x%.8x\n", decode.cmp_a);
    printf("    cmp_a = 0x%.8x\n", decode.cmp_b);
    printf("    cmp_op = 0x%x\n", decode.cmp_op);
    printf("    branch = %d\n", decode.branch);
    printf("    aux = 0x%.8x\n", decode.aux);
    printf("    aux_res = %d\n", decode.aux_res);
    printf("    funct3 = 0x%x\n", decode.funct3);
    printf("    f_unit = %d\n", decode.f_unit);
    printf("    csr_num = 0x%.4x\n", decode.csr_num);
    printf("    csr_val = 0x%.8x\n", decode.csr_val);
    printf("    csr_wb = %d\n", decode.csr_wb);
    printf("    is_csr = %d\n", decode.is_csr);
}

void
CPU::print_exec_latches(void)
{
    exec_latches &exec = this->next_execute.stage_latches;
    printf("Execute latches:\n");
    printf("    valid = %d\n", exec.valid);
    printf("    ctrl = %.1d\n", exec.ctrl);
    printf("    ecall = %d\n", exec.ecall);
    printf("    opcode = 0x%.2x\n", exec.opcode);
    printf("    rd = %u\n", exec.rd);
    printf("    result = 0x%.8x\n", exec.result);
    printf("    addr = 0x%.8x\n", exec.addr);
    printf("    funct3 = %u\n", exec.funct3);
    printf("    csr_num = 0x%.4x\n", exec.csr_num);
    printf("    csr_val = 0x%.8x\n", exec.csr_val);
    printf("    csr_wb = %d\n", exec.csr_wb);
    printf("    is_csr = %d\n", exec.is_csr);
}

void
CPU::print_mem_latches(void)
{
    mem_latches &mem = this->next_memory.stage_latches;
    printf("Memory latches:\n");
    printf("    valid = %d\n", mem.valid);
    printf("    ecall = %d\n", mem.ecall);
    printf("    rd = %u\n", mem.rd);
    printf("    result = 0x%.8x\n", mem.result);
    printf("    csr_num = 0x%.4x\n", mem.csr_num);
    printf("    csr_val = 0x%.8x\n", mem.csr_val);
    printf("    csr_wb = %d\n", mem.csr_wb);
}

void
CPU::print_stall_signals(void)
{
    stall_signals &stalls = this->stalls;
    printf("Stall signals:\n");
    printf("    fetch_mem_stall = %d\n", stalls.fetch_mem_stall);
    printf("    decode_br_stall = %d\n", stalls.decode_br_stall);
    printf("    decode_dep_stall = %d\n", stalls.decode_dep_stall);
    printf("    decode_ei_stall = %d\n", stalls.decode_ei_stall);
    printf("    decode_ca_stall = %d\n", stalls.decode_ca_stall);
    printf("    execute_br_stall = %d\n", stalls.execute_br_stall);
    printf("    execute_ei_stall = %d\n", stalls.execute_ei_stall);
    printf("    execute_ca_stall = %d\n", stalls.execute_ca_stall);
    printf("    memory_br_stall = %d\n", stalls.memory_br_stall);
    printf("    memory_ei_stall = %d\n", stalls.memory_ei_stall);
    printf("    memory_mem_stall = %d\n", stalls.memory_mem_stall);
    printf("    memory_ca_stall = %d\n", stalls.memory_ca_stall);
}


void
CPU::pdump(void)
{
    this->print_fetch_latches();
    this->print_dec_latches();
    this->print_exec_latches();
    this->print_mem_latches();
    this->print_stall_signals();
}

void
CPU::print_perf(void)
{
    u64 cycles = ((u64)csr_file.read(CSR_MCYCLEH) << 32)
                     | csr_file.read(CSR_MCYCLE);
    u64 retired = ((u64)csr_file.read(CSR_MINSTRETH) << 32)
                      | csr_file.read(CSR_MINSTRET);

    printf("Cycle count (mcycle): %llu\n", cycles);
    printf("Retired instructions (minstret): %llu\n", retired);
}

void
CPU::mshow(u32 addr, u32 word)
{
    printf("m[");
    if (this->color) {
        Color_Set(COLOR_BLUE, COLOR_DEFAULT, STYLE_NONE);
    }
    printf("0x%.8x", addr);
    if (this->color) {
        Color_Reset();
    }
    printf("] = ");
    // Color all nonzero values
    if (this->color && word != 0) {
        Color_Set(COLOR_MAGENTA, COLOR_DEFAULT, STYLE_NONE);
    }
    // Color the last memory location that was written
    if (this->color && addr == this->next_execute.stage_latches.addr) {
        Color_Set(COLOR_YELLOW, COLOR_DEFAULT, STYLE_BOLD);
    }
    if (this->nice && word == 0) {
        printf("0");
    } else if (this->nice) {
        printf("0x%.8x (%d)", word, word);
    } else {
        printf("0x%.8x", word);
    }
    if (this->color) {
        Color_Reset();
    }
    putchar('\n');
}

void
CPU::mdump(u32 lo, u32 hi)
{
    u32 addr;
    u32 start;
    u32 end;
    u32 word;

    start = 4*(lo / 4);
    end   = 4*ROUND_UP_DIV(hi, 4);
    printf("mdump: start = 0x%.8x, end = 0x%.8x\n", start, end);
    for (addr = start; addr <= end; addr += 4) {
        word = memread(addr, 4);
        mshow(addr, word);
    }
}

/// Randomize memory stalls
static inline bool mem_ready()
{
    // On average, 1 in 4 memory accesses will stall
    return (rand() & 3) == 3;
}

/* TODO: Throw an exception for misaligned accesses
 * We may be able to detect that in decode, actually. */
bool
CPU::mem_store(u32 data, u32 addr, u8 f3)
{
    u8 width = 1 << (f3 & 0x3);

#if RANDOM_SEED != 0
    if(!mem_ready()) {
        return false;
    }
#endif

    assert(Aligned_MemoryWidth(addr, width));
    memwrite(data, addr, width);
    return true;
}

bool
CPU::mem_load(u32 &data, u32 addr, u8 f3)
{
    u8 width = 1 << (f3 & 0x3);
    bool sext = ((f3 & 0x4) == 0);

#if RANDOM_SEED != 0
    if(!mem_ready()) {
        return false;
    }
#endif

    // For loads, the "width" may also be 0b100 or 0b101 (LBU and LHU)
    data = memread(addr, width);
    // If the high bit in the funct3 bits is not set, must sign-extend
    // the result. The width is either 8 (LBU) or 16 (LHU).
    if (sext) {
        data = SEXT(data, (width * 8));
    }
    return true;
}
