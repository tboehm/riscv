/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "execute_stage.h"

#include "assertions.h"
#include "encodings.h"
#include "processor.h"
#include "util.h"

using namespace inttypes;

void
CPU::Execute_Stage(void)
{
    ////////////////////////////////////
    //          STAGE LOGIC           //
    ////////////////////////////////////

    dec_latches &decode = prev_decode.stage_latches;
    exec_latches &exec = next_execute.stage_latches;

    if (decode.valid) {
        exec_logic(decode, exec);
    }

    // All instructions need to pass on these latches
    exec.ctrl    = decode.ctrl;
    exec.ecall   = decode.ecall;
    exec.opcode  = decode.opcode;
    exec.funct3  = decode.funct3;
    exec.rd      = decode.rd;
    exec.csr_num = decode.csr_num;
    exec.csr_wb  = decode.csr_wb;
    exec.is_csr  = decode.is_csr;

    ////////////////////////////////////
    //       PIPELINE CONTROL         //
    ////////////////////////////////////

    // STALLS: Branch, exception/interrupt
    stalls.execute_br_stall = decode.valid && decode.ctrl;
    stalls.execute_ei_stall = decode.valid && decode.ecall; // TODO: expand
    stalls.execute_ca_stall = decode.valid && decode.is_csr;

    // VALID BIT: Inherit from decode
    exec.valid = decode.valid;

    // PROPAGATE: No memory stall
    bool propagate = !(stalls.memory_mem_stall);
    if (propagate) {
        prev_execute.stage_latches = next_execute.stage_latches;
    }
}

void
exec_logic(dec_latches &decode, exec_latches &exec)
{
    u32 result;
    bool branch_taken;

    result = 0;
    switch (decode.f_unit) {
        case F_UNIT_ALU:
            result = exec_alu(decode.alu_a, decode.alu_b, decode.alu_op,
                              decode.c_in, decode.invert_a);
            break;

        case F_UNIT_SHF:
            result = exec_shf(decode.alu_a, decode.alu_b, decode.alu_op,
                              decode.c_in);
            break;

        case F_UNIT_CMP:
            result = exec_cmp(decode.alu_a, decode.alu_b, decode.alu_op);
            break;

        case F_UNIT_NOP:
            result = 0;
            break;

        default:
            // TODO: Handle bad f_unit more gracefully
            assert(decode.f_unit == F_UNIT_ALU || decode.f_unit == F_UNIT_SHF
                || decode.f_unit == F_UNIT_CMP || decode.f_unit == F_UNIT_NOP);
            break;
    }

    if (decode.branch) {
        // Branches do addition for address generation and a comparison for
        // taken/not taken.
        branch_taken = exec_cmp(decode.cmp_a, decode.cmp_b, decode.funct3);
        if (branch_taken) {
            // result is the new PC
            assert(PC_ALIGNED(result));
            exec.addr = result;
        } else {
            // aux contains the old PC + 4
            exec.addr = decode.aux;
        }
    } else if (decode.is_csr) {
        exec.csr_val = result;
        exec.result = decode.csr_val;
    } else {
        if (decode.alu_addr) {
            exec.addr = result;
        }

        if (decode.aux_res) {
            exec.result = decode.aux;
        } else {
            exec.result = result;
        }
    }
}

u32 exec_alu(u32 src1, u32 src2, u8 funct3, u8 carryin, bool invert_a)
{
    u32 result;

    if (invert_a) {
        // CSRRC and CSRRCI need this
        src1 = ~src1;
    }

    result = 0;
    switch (funct3) {
        case F3_ADD:
            if (carryin == 0) {
                result = src1 + src2;
            } else if (carryin == 1) {
                result = src1 - src2;
            } else {
                assert(carryin == 0 || carryin == 1);
            }
            break;
        case F3_AND:
            result = src1 & src2;
            break;
        case F3_OR:
            result = src1 | src2;
            break;
        case F3_XOR:
            result = src1 ^ src2;
            break;
        default:
            printf("Invalid alu op: 0x%x\n", funct3);
            break;
    }

    return result;
}

u32 exec_shf(u32 data, u32 bits, u8 funct3, u8 carryin)
{
    u32 result;

    result = 0;
    switch (funct3) {
        case F3_SHFL:
            result = data << bits;
            break;
        case F3_SHFR:
            if (carryin == 0) {
                result = data >> bits;
            } else if (carryin == 1) {
                result = (i32)data >> bits;
            } else {
                assert(carryin == 0 || carryin == 1);
            }
            break;
        default:
            printf("Invalid alu op: 0x%x\n", funct3);
            break;
    }

    return result;
}

u32 exec_cmp(u32 lhs, u32 rhs, u8 funct3)
{
    u32 result;

    result = 0;
    switch (funct3) {
        case F3_BEQ:
            result = (lhs == rhs) ? 1 : 0;
            break;
        case F3_BNE:
            result = (lhs != rhs) ? 1 : 0;
            break;
        case F3_BLT:
        case F3_SLT:
            result = ((i32)lhs < (i32)rhs) ? 1 : 0;
            break;
        case F3_BLTU:
        case F3_SLTU:
            result = (lhs < rhs) ? 1 : 0;
            break;
        case F3_BGE:
            result = ((i32)lhs >= (i32)rhs) ? 1 : 0;
            break;
        case F3_BGEU:
            result = (lhs >= rhs) ? 1 : 0;
            break;
        default:
            printf("Invalid alu op: 0x%x\n", funct3);
            break;
    }

    return result;
}
