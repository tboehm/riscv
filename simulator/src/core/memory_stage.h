/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MEMORY_STAGE_H__
#define __MEMORY_STAGE_H__

/**
 * @file memory_stage.h
 *
 * The memory portion of the instruction pipeline.
 *
 * The main function in this file is "Memory_Stage", which is called by
 * the simulator. It performs load/store operations.
 *
 * @author Trey Boehm
 */

#endif // __MEMORY_STAGE_H__
