/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __FETCH_STAGE_H__
#define __FETCH_STAGE_H__

/**
 * @file fetch_stage.h
 *
 * The fetch portion of the instruction pipeline.
 *
 * The main function in this file is "Fetch_Stage", which is called by
 * the simulator. It grabs the next 4 bytes from the memory that the PC
 * points to and puts it in the IR.
 *
 * @author Trey Boehm
 */

#endif // __FETCH_STAGE_H__
