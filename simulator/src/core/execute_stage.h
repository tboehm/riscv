/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __EXECUTE_STAGE_H__
#define __EXECUTE_STAGE_H__

/**
 * @file execute_stage.h
 *
 * The execute portion of the instruction pipeline.
 *
 * The main function in this file is "Execute_Stage", which is called
 * by the simulator. It takes the decoded information and executes the
 * instruction or generates the address for be used by the memory stage.
 *
 * @author Trey Boehm
 */

#include "processor.h"
#include "util.h"

using namespace inttypes;

/**
 * The heavy lifting for execution logic.
 *
 * @return None.
 */
void exec_logic(
    dec_latches &decode, ///< Operand values
    exec_latches &exec   ///< Resulting values
);

/**
 * Perform arithmetic and logical operations.
 *
 * @return The result of the ALU computation
 */
u32 exec_alu(
    u32 src1,     ///< First input to the ALU
    u32 src2,     ///< Second input to the ALU
    u8 funct3,    ///< Type of operation
    u8 carryin,   ///< Carry-in (1 indicates subtraction)
    bool invert_a ///< Invert the A input to the ALU
);

/**
 * Perform shift operations.
 *
 * @return The result of the shift computation
 */
u32 exec_shf(
    u32 data,   ///< Data to shift
    u32 bits,   ///< Number of bits to shift
    u8 funct3,  ///< Type of shift
    u8 carryin  ///< Carry-in for right shifts
);

/**
 * Perform comparisons.
 *
 * Used by the set-less-than instructions and the conditional branch
 * instructions. This works because there are no collisions between the
 * funct3 bits for the different instruction types.
 *
 * @return The result of the comparison
 */
u32 exec_cmp(
    u32 lhs,  ///< Left-hand side of expression
    u32 rhs,  ///< Right-hand side of expression
    u8 funct3 ///< Type of comparison
);

#endif // __EXECUTE_STAGE_H__
