/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __CSR_H__
#define __CSR_H__

/**
 * @file csr.h
 *
 * Control and status register encodings, definitions, and accesses.
 *
 * @author Trey Boehm
 */

#include <map>

#include "encodings.h"
#include "util.h"

using namespace inttypes;

#define CSR_RW(addr)         (addr >> 10)
#define CSR_READ_WRITE(addr) (CSR_RW(addr) != 0)
#define CSR_READ_ONLY(addr)  (CSR_RW(addr) == 0)
#define CSR_PRIV(addr)       ((addr >> 8) & 0x3)
#define CSR_USER(addr)       (CSR_PRIV(addr) == 0)
#define CSR_SUPERVISOR(addr) (CSR_PRIV(addr) <= 1)
// Note: H-mode was removed from the spec in Version 1.10
#define CSR_HYPERVISOR(addr) (CSR_PRIV(addr) <= 2)

/**
 * A single CSR.
 *
 * This associates an address, a name, and 32-bits of data.
 */
struct csr {
    u16 addr;         ///< CSR 12-bit address
    u32 data;         ///< Data held in the CSR
    const char *name; ///< Single word CSR name
};

/// The initial values for the core's CSRs.
const struct csr CSR_INIT[] = {
    { CSR_MVENDORID, 0,           "mvendorid", },
    { CSR_MARCHID,   0,           "marchid",   },
    { CSR_MIMPID,    0,           "mimpid",    },
    { CSR_MHARTID,   0,           "mhartid",   },
    { CSR_MSTATUS,   0,           "mstatus",   },
    { CSR_MTVEC,     0,           "mtvec",     },
    { CSR_MSCRATCH,  0,           "mscratch",  },
    { CSR_MEPC,      0,           "mepc",      },
    { CSR_MCAUSE,    0,           "mcause",    },
    { CSR_MTVAL,     0,           "mtval",     },
    { CSR_MIP,       0,           "mip",       },
    { CSR_MCYCLE,    0,           "mcycle",    },
    { CSR_MCYCLEH,   0,           "mcycleh",   },
    { CSR_MINSTRET,  0,           "mcycle",    },
    { CSR_MINSTRETH, 0,           "mcycleh",   },

    // Defines the implementation. Not currently stable.
    { CSR_MISA, MXL_32 | EXTENSION_I, "misa", },
};

/// The number of elements in the CSR file.
static const size_t NUM_CSRS = sizeof(CSR_INIT) / sizeof(CSR_INIT[0]);

/// The mapping used in the CSR file from addresses to registers.
typedef std::map<u16, struct csr> csr_mapping;

/**
 * The CSR file.
 *
 * This class contains a mapping of addresses to CSR instances. It supports
 * reads, writes, and printing operations.
 */
class CSRFile {
private:
    /**
     * Mapping from CSR address to the data.
     *
     * The values have type `struct csr`, which includes the address, data, and
     * name. This facilitates printing.
     */
    csr_mapping csr_file;

    /**
     * Get a pointer to a `struct csr` by address.
     *
     * @return The struct for a given CSR, if it exists. Otherwise null.
     */
    struct csr *byaddr(
        u16 addr ///< Address of the CSR
    );

    /**
     * Get a pointer to a `struct csr` by name.
     *
     * @return The struct for a given CSR, if it exists. Otherwise null.
     */
    struct csr *byname(
        const char *name ///< Name of the CSR
    );

public:
    /**
     * Initialize the CSR file.
     *
     * Set default values in machine system information and trap setup
     * registers.
     */
    CSRFile(void);

    /**
     * Print the contents of the CSR file.
     *
     * @return None.
     */
    void show(void);

    /**
     * Print the contents of a single CSR.
     *
     * @return None.
     */
    void print(
        u16 addr ///< Address of a CSR to print
    );

    /**
     * Write to a CSR.
     *
     * @return True if the write succeeded, otherwise false.
     */
    bool write(
        u16 addr, ///< CSR address to write to
        u32 data  ///< Data to write
    );

    /**
     * Read from a CSR.
     *
     * @return The data read from the CSR.
     */
    u32 read(
        u16 addr ///< CSR address to read from
    );

    /**
     * Get the name of a CSR.
     *
     * @return A character pointer to the name of the CSR.
     */
    const char *getname(
        u16 addr ///< CSR address to get the name of
    );

    /**
     * Check whether a CSR address is implemented.
     *
     * @return True if the CSR is implemented, otherwise false.
     */
    bool implemented(
        u16 addr ///< CSR address to check
    );
};

#endif // __CSR_H__
