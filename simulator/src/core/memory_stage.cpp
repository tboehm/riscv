/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "memory_stage.h"

#include <assert.h>

#include "assertions.h"
#include "color.h"
#include "encodings.h"
#include "memory.h"
#include "processor.h"
#include "util.h"

using namespace inttypes;

void
CPU::Memory_Stage(void)
{
    ////////////////////////////////////
    //          STAGE LOGIC           //
    ////////////////////////////////////

    exec_latches &exec = prev_execute.stage_latches;
    mem_latches &mem = next_memory.stage_latches;
    bool ready; // memory load/store succeeded

    // only access memory if the stage is valid
    if (exec.valid) {
        switch (exec.opcode) {
        case OP_LOAD:
            /* TODO: Handle the exception instead of this assertion */
            assert(exec.rd != 0);
            ready = mem_load(mem.result, exec.addr, exec.funct3);
            if (this->delta && ready) {
                if (this->color) {
                    Color_Set(COLOR_BLUE, COLOR_DEFAULT, STYLE_NONE);
                }
                printf("Read %d bytes from address 0x%.8x\n",
                       (1 << (exec.funct3 & 0x3)), exec.addr);
                if (this->color) {
                    Color_Reset();
                }
            }
            break;
        case OP_STORE:
            ready = mem_store(exec.result, exec.addr, exec.funct3);
            if (this->delta && ready) {
                if (this->color) {
                    Color_Set(COLOR_BLUE, COLOR_DEFAULT, STYLE_NONE);
                }
                printf("Wrote %d bytes of 0x%.8x to address 0x%.8x\n",
                       (1 << (exec.funct3 & 0x3)), exec.result, exec.addr);
                if (this->color) {
                    Color_Reset();
                }
            }
            mem.result = 0;
            break;
        default:
            ready = true;
            mem.result = exec.result;
            break;
        }
    }

    // Update the PC if we had a valid control transfer instruction
    if (exec.valid && exec.ctrl) {
        regwrite(PC_NUM, exec.addr);
    }

    mem.ecall   = exec.ecall;
    mem.rd      = exec.rd;
    mem.csr_num = exec.csr_num;
    mem.csr_val = exec.csr_val;
    mem.csr_wb  = exec.csr_wb;

    ////////////////////////////////////
    //       PIPELINE CONTROL         //
    ////////////////////////////////////

    // VALID BIT: Memory transaction successful
    mem.valid = exec.valid && ready;

    // STALLS: Memory, branch, exception/interrupt
    stalls.memory_br_stall = exec.valid && exec.ctrl;
    stalls.memory_mem_stall = exec.valid && !(ready);
    stalls.memory_ei_stall = exec.valid && exec.ecall; // TODO: expand
    stalls.memory_ca_stall = exec.valid && exec.is_csr;

    // PROPAGATE: Always
    prev_memory.stage_latches = next_memory.stage_latches;
}
