/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __WRITEBACK_STAGE_H__
#define __WRITEBACK_STAGE_H__

/**
 * @file writeback_stage.h
 *
 * The writeback portion of the instruction pipeline.
 *
 * The main function in this file is "Writeback_Stage", which is called
 * by the simulator. It writes a value to the register file.
 *
 * @author Trey Boehm
 */

#include "csr.h"
#include "util.h"

using namespace inttypes;

/**
 * Increment a performance counter.
 *
 * Since these are 64-bit registers on a 32-bit machine, we need to do them one
 * at a time. In hardware, this would be a little bit trickier, but I'll leave
 * that for later.
 *
 * @return None.
 */
void wb_inc_perf_csr(
    CSRFile &csr_file, ///< CSR file for the core
    u16 lo_addr,       ///< Address of low 32 bits of the register
    u16 hi_addr        ///< Address of high 32 bits of the register
);


/**
 * Increment the retired instruction counter.
 *
 * @return None.
 */
void wb_inc_minstret(
    CSRFile &csr_file ///< CSR file for the core
);

/**
 * Increment the cycle counter.
 *
 * @return None
 */
void wb_inc_mcycle(
    CSRFile &csr_file ///< CSR file for the core
);

#endif // __WRITEBACK_STAGE_H__
