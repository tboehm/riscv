/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __DECODE_STAGE_H__
#define __DECODE_STAGE_H__

/**
 * @file decode_stage.h
 *
 * The decode portion of the instruction pipeline.
 *
 * The main function in this file is "Decode_Stage", which is called by
 * the simulator. It takes in a 32-bit instruction and decodes it.
 *
 * @author Trey Boehm
 */

#include "util.h"

using namespace inttypes;

/**
 * Check whether this is a control transfer instruction
 *
 * @return True, if this is a branch, jal, or jalr. False, otherwise.
 */
bool check_ctrl(
    u32 ir ///< The instruction bits
);

/**
 * Get the itype (R, I, etc.) of an instruction.
 *
 * @return The itype of instruction encoding
 */
u8 get_instr_type(
    u8 opcode ///< The instruction's opcode opcode
);

/**
 * Get the opcode from an instruction.
 *
 * @return The 7-bit opcode
 */
u8 get_opcode(
    u32 instr ///< The instruction bits
);

/**
 * Get the destination register number from an instruction.
 *
 * @return The 5-bit destination register number
 */
u8 get_rd(
    u32 instr, ///< The instruction bits
    u8 itype   ///< The type of instruction
);

/**
 * Get the first source register number from an instruction.
 *
 * @return The 5-bit source register 1 number
 */
u8 get_rs1(
    u32 instr, ///< The instruction bits
    u8 itype   ///< The type of instruction
);

/**
 * Get the second source register number from an instruction.
 *
 * @return The 5-bit source register 2 number
 */
u8 get_rs2(
    u32 instr, ///< The instruction bits
    u8 itype   ///< The type of instruction
);

/**
 * Get the A input for the ALU.
 *
 * This may be the contents of source register 1, the PC, an immediate (for CSR
 * instructions), or 0.
 *
 * Note: the CSR immediate uses the same bits as rs1 in the instruction.
 *
 * @return The A input for the ALU.
 */
u32 get_alu_a(
    u8 itype,    ///< Instruction type
    u8 opcode,   ///< Opcode
    u8 funct3,   ///< funct3 bits
    u32 rs1_val, ///< Source register 1 contents
    u32 pc,      ///< PC for the instruction
    u8  csr_imm  ///< Immediate value for CSRxxI instructions
);

/**
 * Get the B input for the ALU.
 *
 * This may be the contents of source register 2, the PC, an immediate, a CSR
 * register value, or 0.
 *
 * @return The B input for the ALU.
 */
u32 get_alu_b(
    u8 itype,    ///< Instruction type
    u8 opcode,   ///< Opcode
    u8 funct3,   ///< funct3 bits
    u32 rs2_val, ///< Source register 2 contents
    u32 imm,     ///< Immediate value
    u32 csr_val  ///< CSR contents
);

/**
 * Get the ALU opcode.
 *
 * For many instructions, this is always F3_ADD. For many R_Type and I_Type,
 * this is the actual funct3 bits.
 *
 * @return The ALU opcode for an instruction.
 */
u8 get_alu_op(
    u8 itype,  ///< Instruction type
    u8 opcode, ///< Opcode
    u8 funct3  ///< funct3 bits
);

/**
 * Does this instruction generate an address for accessing memory or the PC?
 *
 * @return true for branches, loads, stores, and jumps, otherwise false.
 */
bool is_alu_addr(
    u8 itype, ///< Instruction type
    u8 opcode ///< Opcode
);


/**
 * Invert the ALU input A?
 *
 * @return true for CSRRC and CSRRCI instructions, otherwise false.
 */
bool get_alu_inv_a(
    u8 opcode, ///< Opcode
    u8 funct3  ///< funct3 bits
);

/**
 * Get the A input for the comparator.
 *
 * For branches, return the value in rs1. For all other instructions, return 0.
 *
 * @return The A input for the comparator, if necessary.
 */
u32 get_cmp_a(
    u8 itype,   ///< Instruction type
    u32 rs1_val ///< Source register 1 contents
);

/**
 * Get the B input for the comparator.
 *
 * For branches, return the value in rs2. For all other instructions, return 0.
 *
 * @return The B input for the comparator, if necessary.
 */
u32 get_cmp_b(
    u8 itype,   ///< Instruction type
    u32 rs2_val ///< Source register 2 contents
);

/**
 * Get auxiliary data to pass through to a later stage.
 *
 * For S-type instructions, pass the value in rs2.
 * For U-type instructions, pass the immediate.
 * For J-type and B-type instructions, pass the old PC + 4.
 *
 * @return Data to pass through execute.
 */
u32 get_aux(
    u8 itype,    ///< Instruction type
    bool ctrl,   ///< Help select link for JALR
    u32 rs2_val, ///< Source register 2 contents
    u32 imm,     ///< Immediate value
    u32 link     ///< Link register
);

/**
 * Does the auxiliary register contain the result?
 *
 * @return true for stores and LUI, otherwise false.
 */
bool is_aux_res(
    u8 opcode ///< Opcode
);

/**
 * Get the funct3 bits from an instruction.
 *
 * @return The 3-bit funct3 value
 */
u8 get_funct3(
    u32 instr, ///< The instruction bits
    u8 itype   ///< The type of instruction
);

/**
 * Get the funct7 bits from an instruction.
 *
 * There are a few cases that deviate from the norm. SLLI, SRLI, and
 * SRAI are all technically I-type instructions that do not have a
 * funct7 value. However, their immediate (`shamt`) is only 5 bits and
 * a bit in the upper 7 determines whether the shift is logical or
 * arithmetic. This steering bit (used only by SRAI, really) acts like
 * a funct7.
 *
 * @return The 7-bit funct7 value
 */
u8 get_funct7(
    u32 instr, ///< The instruction bits
    u8 itype,  ///< The type of instruction
    u8 opcode, ///< The instruction's opcode
    u8 funct3  ///< The instruction's funct3 value
);

/**
 * Determine which functional unit to use.
 *
 * @return The 3-bit value indicating which unit to use
 */
u8 get_f_unit(
    u8 opcode, ///< The instruction's opcode
    u8 funct3  ///< The instruction's funct3 value
);

/**
 * Get the immediate value from an instruction.
 *
 * The logic for this function is much more complex than the other
 * instruction-parsing routines. The immediate value can be in a variety
 * of places, so we need to do a lot of shifting and masking.
 *
 * @return The encoded immediate value
 */
u32 get_imm(
    u32 instr, ///< The instruction bits
    u8 itype   ///< The type of instruction
);

/**
 * Get the immediate value from an I-type instruction.
 *
 * I-type instructions have a 12-bit immediate value in the top 12 bits
 * of the instruction. These form the lower 12 bits of the value. The
 * upper 20 bits are formed by sign extension.
 *
 * @return The sign-extended 12-bit immediate value
 */
u32 get_i_imm(
    u32 instr ///< The instruction bits
);

/**
 * Get the immediate value from an S-type instruction.
 *
 * S-type instructions have a 12-bit immediate value split up between
 * the typical "rd" section of the instruction (bits 11:7) and the top 7
 * bits. These form the lower 12 bits of the value. The top 20 are
 * formed by sign extension.
 *
 * @return The sign-extended 12-bit immediate value
 */
u32 get_s_imm(
    u32 instr ///< The instruction bits
);

/**
 * Get the immediate value from an B-type instruction.
 *
 * B-type instructions have a 12-bit immediate value split up between
 * the typical "rd" section of the instruction (bits 11:7) and the top 7
 * bits. The lowest bit is always a 0 and the top 19 bits are formed by
 * sign extension.
 *
 * @return The sign-extended 13-bit immediate value
 */
u32 get_b_imm(
    u32 instr ///< The instruction bits
);

/**
 * Get the immediate value from an I-type instruction.
 *
 * U-type instructions have a 20-bit immediate value in the upper 20
 * bits of the instruction. These form the top 20 bits of the value. The
 * lower 12 bits are zeroed out.
 *
 * @return The 32-bit immediate value
 */
u32 get_u_imm(
    u32 instr ///< The instruction bits
);

/**
 * Get the immediate value from a J-type instruction.
 *
 * J-type instructions have a 20-bit immediate value in the upper 20
 * bits of the instruction. They are ordered in a somewhat unusual way,
 * but form bits 20:1 of the value. The lowest bit is always a 0, and
 * the top 11 bits are formed by sign extension.
 *
 * @return The sign-extended 21-bit immediate value
 */
u32 get_j_imm(
    u32 instr ///< The instruction bits
);

/**
 * Get the CSR number from an instruction.
 *
 * @return The top 12 bits, zero-extended.
 */
u16 get_csr_num(
    u32 instr ///< Instruction bits
);


/**
 * CSR instruction that needs an immediate based on its funct3 bits.
 *
 * @return true iff funct3[2] is set.
 */
bool is_f3_csri(
    u8 funct3 ///< funct3 bits
);

/**
 * Check if the funct3 bits are those for a CSR instruction.
 *
 * @return true iff the funct3 bits correspond to a CSR instruction.
 */
bool is_f3_csr(
    u8 funct3 ///< funct3 bits
);

/**
 * Check if the funct3 bits are those for a CSRRW[I] instruction.
 *
 * @retrun true iff the funct3 bits correspond to CSRRW or CSRRWI.
 */
bool is_f3_csrw(
    u8 funct3 ///< funct3 bits
);

#endif // __DECODE_STAGE_H__
