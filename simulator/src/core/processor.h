/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __PROCESSOR_H__
#define __PROCESSOR_H__

/**
 * @file processor.h
 *
 * The state of the processor.
 *
 * Currently, this file only contains functions for accessing the
 * general-purpose registers, PC, and IR, as well as their definitions.
 * As the project becomes more complex, this file will contain the
 * definitions and functions for other registers.
 *
 * @author Trey Boehm
 */

#include "csr.h"
#include "util.h"

using namespace inttypes;

/**
 * Numbers to access general-purpose and specialized registers.
 *
 * Notice that GP_REGS is defined as 32, so checks to see if a register
 * number refers to a general-purpose register should use a less-than
 * operator. PC_NUM and IR_NUM, by contrast, are indices used to access
 * those registers. This is why the number 32 is used twice.
 *
 * Only the general-purpose registers can be directaly accessed by most
 * instructions. The rest may or may not be architecturally visible, but
 * it is nice to have them all in one place to print out with rdump.
 */
enum Reg_Num {
    GP_REGS = 32,     ///< Number of general-purpose registers
    PC_NUM = GP_REGS, ///< Index for the program counter
    IR_NUM,           ///< Index for the instruction register
    REG_COUNT,        ///< Total number of registers
};

/// Standard names of all general-purpose registers.
static const char *GPRegNames[] = {
    "x0",  "x1",  "x2",  "x3",  "x4",  "x5",  "x6",  "x7",
    "x8",  "x9",  "x10", "x11", "x12", "x13", "x14", "x15",
    "x16", "x17", "x18", "x19", "x20", "x21", "x22", "x23",
    "x24", "x25", "x26", "x27", "x28", "x29", "x30", "x31",
};

/// ABI names of general-purpose registers
static const char *ABIRegNames[] = {
    "zero",  "ra", "sp",  "gp",  "tp", "t0", "t1", "t2",
    "s0/fp", "s1", "a0",  "a1",  "a2", "a3", "a4", "a5",
    "a6",    "a7", "s2",  "s3",  "s4", "s5", "s6", "s7",
    "s8",    "s9", "s10", "s11", "t3", "t4", "t5", "t6"
};

/// Latches between each stage.
template<class stage>
class Latches {
public:
    /// The struct containing the actual latch values
    stage stage_latches;

    /**
     * Clear the contents of the latches.
     *
     * @return None.
     */
    void clear(void)
    {
        memset(&(this->stage_latches), 0, sizeof(stage));
    }
};

/// The results of the fetch stage.
typedef struct {
    bool valid; ///< Valid instruction or bubble?
    u32 pc;     ///< Program counter
    u32 link;   ///< Program counter + 4, for linkage
    u32 ir;     ///< Instruction bits
} fetch_latches;

/**
 * The results of the decode stage.
 *
 * After the decode stage, we will have a struct containing all of the
 * operands for this instruction. We can pass this on to the address
 * generation/execute stage.
 *
 * TODO: Fields for FENCE, CSR*, atomic, and division instructions.
 */
typedef struct {
    bool valid;    ///< Valid instruction or bubble?
    bool ctrl;     ///< Is this a control instruction?
    bool ecall;    ///< Is this an ECALL instruction?
    u8 opcode;     ///< Opcode
    u8 rd;         ///< Destination register, or x0
    u32 alu_a;     ///< A input to the ALU
    u32 alu_b;     ///< B input to the ALU
    bool c_in;     ///< Carry-in for the ALU
    u8 alu_op;     ///< ALU operation (from funct3, or instruction-specific)
    bool alu_addr; ///< ALU generates a new PC or data memory address
    bool invert_a; ///< Invert the ALU input A?
    u32 cmp_a;     ///< A input to the comparator (branches)
    u32 cmp_b;     ///< B input to the comparator (branches)
    u8 cmp_op;     ///< CMP operation (from funct3, or instruction-specific)
    bool branch;   ///< Comparator result is for conditional branches
    u32 aux;       ///< Auxilliary data to pass through execute
    bool aux_res;  ///< Auxilliary data already contains the result
    u8 funct3;     ///< 3-bit function (R, I, S, B)
    u8 f_unit;     ///< Functional unit to use in exec stage
    u16 csr_num;   ///< CSR number
    u32 csr_val;   ///< Value from the CSR
    bool csr_wb;   ///< Write-back to the CSR
    bool is_csr;   ///< Whether this is a CSR instruction
} dec_latches;

/// The results of the execute stage.
typedef struct {
    bool valid;  ///< Valid instruction or bubble?
    bool ctrl;   ///< Whether this instruction may modify PC
    bool ecall;  ///< Whether this instruction is ECALL
    u8 opcode;   ///< Six-bit opcode for the instruction
    u8 rd;       ///< Destination register's number
    u32 result;  ///< Resulting computation or address
    u32 addr;    ///< Address for the memory stage, or a new PC
    u8 funct3;   ///< Width of data to load/store
    u16 csr_num; ///< CSR number
    u32 csr_val; ///< Value from the CSR
    bool csr_wb; ///< Write-back to the CSR
    bool is_csr; ///< Whether this is a CSR instruction
} exec_latches;

/// The results of the memory access stage.
typedef struct {
    bool valid;  ///< Valid instruction or bubble?
    bool ecall;  ///< Whether this instruction is ECALL
    u8 rd;       ///< Destination register's number
    u32 result;  ///< Resulting computation or address
    u16 csr_num; ///< CSR number
    u32 csr_val; ///< Value from the CSR
    bool csr_wb; ///< Write-back to the CSR
} mem_latches;

/// Stall signals generated on this cycle.
typedef struct {
    bool fetch_mem_stall;  ///< Memory stall in the fetch stage
    bool decode_br_stall;  ///< Branch/jump in the decode stage
    bool decode_dep_stall; ///< Data dependency in the decode stage
    bool decode_ei_stall;  ///< Exception/interrupt in the decode stage
    bool decode_ca_stall;  ///< CSR or atomic instruction in the decode stage
    bool execute_br_stall; ///< Branch/jump in the execute stage
    bool execute_ei_stall; ///< Exception/interrupt in the execute stage
    bool execute_ca_stall; ///< CSR or atomic instruction in the execute stage
    bool memory_br_stall;  ///< Branch/jump in the memory stage
    bool memory_ei_stall;  ///< Exception/interrupt in the memory stage
    bool memory_mem_stall; ///< Memory stall in the memory stage
    bool memory_ca_stall;  ///< CSR or atomic instruction in the memory stage
} stall_signals;

/// The state of the CPU.
class CPU {
private:
    /// Fetch stage latches at the start of the cycle
    Latches<fetch_latches> prev_fetch;

    /// Fetch stage latches generated this cycle
    Latches<fetch_latches> next_fetch;

    /// Decode stage latches at the start of the cycle
    Latches<dec_latches> prev_decode;

    /// Decode stage latches generated this cycle
    Latches<dec_latches> next_decode;

    /// Execute stage latches at the start of the cycle
    Latches<exec_latches> prev_execute;

    /// Execute stage latches generated this cycle
    Latches<exec_latches> next_execute;

    /// Memory stage latches at the start of the cycle
    Latches<mem_latches> prev_memory;

    /// Memory stage latches generated this cycle
    Latches<mem_latches> next_memory;

    /// Pipeline stall signals
    stall_signals stalls;

    /// The CPU's registers
    u32 Registers[REG_COUNT];

    /// Whether the CPU is running or stopped
    bool running;

    /// Whether the CPU is powered on or not
    bool power_on;

    /// Whether or not to print CPU latches at each cycle
    bool verbose;

    /// Print information about changed registers and memory
    bool delta;

    /// Print color-coded text
    bool color;

    /// Display information in a more human-readable way
    bool nice;

    /// Control and status registers
    CSRFile csr_file;

public:
    /**
     * Initialize the state of the CPU.
     *
     * @return A CPU ready for simulation.
     */
    CPU(
        u32 entry,       ///< Entry point (starting PC)
        bool interactive ///< For interactive (not automated) use
    );

    /**
     * Format and show the name and contents of a register.
     *
     * Looks at the color option, most recent destination register, and
     * whether or not the value is zero.
     *
     * @return none.
     */
    void rshow(
        const char *name, ///< Register name
        u8 rnum           ///< Register number
    );

    /**
     * Dump all of the CPU's internal registers.
     *
     * Show the contents of all general purpose registers, the program
     * counter, and the instruction register. Any additional registers
     * that are added to the machine should be included here, too.
     *
     * @return None.
     */
    void rdump(
        u8 cols, ///< Number of columns for printing the GP regs
        bool abi ///< Use ABI register naming for GP regs
    );

    /**
     * Write some data to a register.
     *
     * If regnum is in the range [0,GP_REGS], it refers to a general-
     * purpose register. Writes to register 0 will not fail, but will
     * have no effect on the contents (x0 always contains 0). However,
     * the spec says:
     *
     *  *Loads with a destination of x0 must still raise any exceptions
     *  and action any other side effects even though the load value is
     *  discarded.*
     *
     * We handle this in detection in decode, so it should never be a
     * problem by the time regwrite is called.
     *
     * Use the macros enumerated in #Reg_Num to refer to non-general-
     * purpose registers when calling regwrite or regread.
     *
     * @return None.
     */
    void regwrite(
        u8 regnum, ///< The register to write into
        u32 data   ///< The data to write into the register
    );

    /**
     * Read some data from a register.
     *
     * See regwrite() documentation for info on the regnum parameter.
     *
     * @return The contents of the register
     */
    u32 regread(
        u8 regnum ///< The register to read from
    );

    /**
     * Set the verbose variable.
     *
     * @return None.
     */
    void set_verbose(
        bool verbosity ///< The level of verbosity
    );

    /**
     * Set the delta variable.
     *
     * After each cycle, print the registers/memory locations that
     * changed.
     *
     * @return None.
     */
    void set_delta(
        bool delta ///< Whether to display processor state delta
    );

    /**
     * Set the color variable.
     *
     * Color can help make register dumps easier to look at. Every non-
     * zero value gets a color. Additionally, emphasize the most
     * recently updated register with a different color. Along with the
     * delta variable, this makes it easy to understand how the state of
     * the system is evolving step-by-step.
     *
     * @return None.
     */
    void set_color(
        bool color ///< Whether to display color-coded output
    );

    /**
     * Set the readability of the output.
     *
     * Do things like print "0" instead of "0x00000000" for registers/
     * memory locations with all-zero data and print the value in
     * decimal as well as hex.
     *
     * This isn't set by default because it doesn't really add any
     * additional information. Generating key files would be a pain
     * because I would like to add quality of life improvements like
     * this without having to go back and change every single key file.
     *
     * @return None.
     */
    void set_nice(
        bool nice ///< Nice output for readability or not
    );

    /**
     * Halt the processor.
     *
     * @return None.
     */
    void halt(void);

    /**
     * Advance execution by one cycle.
     *
     * @return None.
     */
    void cycle(void);

    /**
     * Run for a particular number of cycles, or to completion.
     *
     * If the num_cycles is 0, run forever.
     *
     * @return None.
     */
    void run(
        u32 num_cycles ///< How many cycles to run
    );

    /**
     * Fetch an instruction from memory.
     *
     * This function looks at the opcode and funct bits to determine the
     * itype of instruction. It populates a struct with the operands and
     * leaves any unused operands as 0.
     *
     * @return None.
     */
    void Fetch_Stage(void);

    /**
     * Decode an instruction.
     *
     * This function looks at the opcode and funct bits to determine the
     * itype of instruction. It populates a struct with the operands and
     * leaves any unused operands as 0.
     *
     * @return None.
     */
    void Decode_Stage(void);

    /**
     * Execute an instruction.
     *
     * @return None.
     */
    void Execute_Stage(void);

    /**
     * Perform an instruction's memory access.
     *
     * @return None.
     */
    void Memory_Stage(void);

    /**
     * Write a value to the register file.
     *
     * @return None.
     */
    void Writeback_Stage(void);

    /**
     * Print the contents of the next fetch latches.
     *
     * @return None.
     */
    void print_fetch_latches(void);
    /**
     * Print the contents of the next fetch latches.
     *
     * @return None.
     */
    void print_dec_latches(void);

    /**
     * Print the contents of the next fetch latches.
     *
     * @return None.
     */
    void print_exec_latches(void);

    /**
     * Print the contents of the next fetch latches.
     *
     * @return None.
     */
    void print_mem_latches(void);

    /**
     * Print the stall signals for the pipelien.
     *
     * @return None.
     */
    void print_stall_signals(void);

    /**
     * Dump the contents of all pipeline latches.
     *
     * @return None.
     */
    void pdump(void);

    /**
     * Print performance information for the core.
     *
     * @return None.
     */
    void print_perf(void);

    /**
     * Format and show the address and contents of memory.
     *
     * Looks at the color option, most recent destination address,
     * whether or not the value is zero, and whether to display decimal
     * ('nice' attribute).
     *
     * @return none.
     */
    void mshow(
        u32 addr,
        u32 word
    );

    /**
     * Dump a subset of memory to stdout.
     *
     * Output the memory contents between the provided low and high
     * address. If the low address is not word-aligned, start from the
     * previous word address. Similarly, if the high address is not
     * word-aligned, finish at the next word.
     *
     * @return None.
     */
    void mdump(
        u32 lo, ///< The low address to start the dump
        u32 hi  ///< The high address to end the dump
    );

    /**
     * Store data to memory.
     *
     * This function is basically just a wrapper for mem_write that deals in
     * funct3 bits instead of real byte widths. It may also stall.
     *
     * @return True if the load succeeded, otherwise false.
     */
    bool mem_store(
        u32 data,  ///< Data to store in memory
        u32 addr,  ///< Address at which to store
        u8 f3      ///< The funct3 bits from the instruction
    );

    /**
     * Store data to memory.
     *
     * This function is basically just a wrapper for mem_read that deals in
     * funct3 bits instead of real byte widths. It may also stall.
     *
     * @return True if the load succeeded, otherwise false.
     */
    bool mem_load(
        u32 &data,  ///< Reference to the data we are reading
        u32 addr,   ///< Address from which to load
        u8 f3       ///< The funct3 bits from the instruction
    );
};

#endif // __PROCESSOR_H__
