// Copyright (c) 2020 Trey Boehm
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.


#ifndef __CORECONFIG_H__
#define __CORECONFIG_H__

/**
 * @file coreconfig.h
 *
 * Configuration for core memory ???
 *
 * @author Trey Boehm
 */

/**
 * Address of the trap handler.
 *
 * I plan on implementing vectored interrupts at some point, but for now this
 * points to a common handler for all traps.
 *
 * The assembled "trap.s" goes here.
 */
#define TRAP_VECTOR_BASE 0x00002000

/**
 * Main entry point.
 *
 * Load user programs (test cases) at this address. The reset handler will
 * branch to this address once setup is complete.
 */
#define MAIN_ENTRY 0x00000000

#endif /* __CORECONFIG_H__ */
