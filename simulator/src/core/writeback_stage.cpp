/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "writeback_stage.h"

#include <stdio.h>

#include "color.h"
#include "csr.h"
#include "encodings.h"
#include "processor.h"
#include "util.h"

using namespace inttypes;

void
CPU::Writeback_Stage(void)
{
    ////////////////////////////////////
    //          STAGE LOGIC           //
    ////////////////////////////////////

    mem_latches &mem = prev_memory.stage_latches;

    wb_inc_mcycle(csr_file);

    if (!mem.valid) {
        return;
    }

    regwrite(mem.rd, mem.result);

    if (mem.csr_wb) {
        csr_file.write(mem.csr_num, mem.csr_val);
    }

    if (this->delta && (mem.rd != 0 || mem.csr_wb)) {
        if (this->color) {
            Color_Set(COLOR_YELLOW, COLOR_DEFAULT, STYLE_BOLD);
        }

        if (mem.rd != 0) {
            // Use ABI register names if the "nice" flag is set
            printf("Wrote 0x%.8x to register %s\n", mem.result,
                   this->nice ? ABIRegNames[mem.rd] : GPRegNames[mem.rd]);
        }

        if (mem.csr_wb) {
                printf("Wrote 0x%.8x to CSR %s\n", mem.csr_val,
                       csr_file.getname(mem.csr_num));
        }

        if (this->color) {
            Color_Reset();
        }
    }

    wb_inc_minstret(this->csr_file);

    ////////////////////////////////////
    //       PIPELINE CONTROL         //
    ////////////////////////////////////

    // (None)
}

void
wb_inc_perf_csr(CSRFile &csr_file, u16 lo_addr, u16 hi_addr)
{
    u32 lo_value;
    u32 hi_value;

    lo_value = csr_file.read(lo_addr);
    if (lo_value == UINT32_MAX) {
        hi_value = csr_file.read(hi_addr);
        hi_value++;
        csr_file.write(hi_addr, hi_value);
    }
    lo_value++;
    csr_file.write(lo_addr, lo_value);
}

void
wb_inc_minstret(CSRFile &csr_file)
{
    wb_inc_perf_csr(csr_file, CSR_MINSTRET, CSR_MINSTRETH);
}

void
wb_inc_mcycle(CSRFile &csr_file)
{
    wb_inc_perf_csr(csr_file, CSR_MCYCLE, CSR_MCYCLEH);
}
