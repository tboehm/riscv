/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "decode_stage.h"

#include <assert.h>

#include "assertions.h"
#include "csr.h"
#include "encodings.h"
#include "processor.h"
#include "util.h"

using namespace inttypes;

void
CPU::Decode_Stage(void)
{
    ////////////////////////////////////
    //          STAGE LOGIC           //
    ////////////////////////////////////

    fetch_latches &fetch = prev_fetch.stage_latches;
    dec_latches &decode = next_decode.stage_latches;

    // COMBINATIONAL LOGIC
    u32 instr     = fetch.ir;
    bool ctrl     = check_ctrl(instr);
    bool ecall    = fetch.ir == OP_SYSTEM;
    u8 opcode     = get_opcode(instr);
    u8 itype      = get_instr_type(opcode);
    u8 rs1        = get_rs1(instr, itype);
    u8 rs2        = get_rs2(instr, itype);
    u32 imm       = get_imm(instr, itype);
    u16 csr_num   = get_csr_num(instr);
    u8 funct3     = get_funct3(instr, itype);
    u8 funct7     = get_funct7(instr, itype, opcode, funct3);
    u32 csr_val   = 0;
    bool csr_wb   = false;
    bool is_csr   = false;

    // READ REGISTERS
    // TODO figure out a better abstraction so these can go in functions
    u32 rs1_val   = regread(rs1);
    u32 rs2_val   = regread(rs2);
    if (opcode == OP_SYSTEM && is_f3_csr(funct3)) {
        is_csr = true;
        csr_val = csr_file.read(csr_num);
        if (rs1 == 0 && (funct3 == F3_CSRRS || funct3 == F3_CSRRSI
                      || funct3 == F3_CSRRC || funct3 == F3_CSRRCI)) {
            // Do not write back to the register unless we want to actually
            // change it. For RW operations, we always change it. For set/clear,
            // we only change it if the rs1 (or uimm5) is non-zero.
            csr_wb = false;
        } else {
            csr_wb = true;
        }
    }

    // LATCH VALUES
    decode.alu_a    = get_alu_a(itype, opcode, funct3, rs1_val, fetch.pc, rs1);
    decode.alu_b    = get_alu_b(itype, opcode, funct3, rs2_val, imm, csr_val);
    decode.c_in     = BIT_SEL(funct7, 5, 5);
    decode.alu_op   = get_alu_op(itype, opcode, funct3);
    decode.alu_addr = is_alu_addr(itype, opcode);
    decode.invert_a = get_alu_inv_a(opcode, funct3);
    decode.cmp_a    = get_cmp_a(itype, rs1_val);
    decode.cmp_b    = get_cmp_b(itype, rs2_val);
    decode.branch   = opcode == OP_BRANCH;
    decode.aux      = get_aux(itype, ctrl, rs2_val, imm, fetch.link);
    decode.aux_res  = is_aux_res(opcode);
    decode.ctrl     = ctrl;
    decode.ecall    = ecall;
    decode.rd       = get_rd(instr, itype);
    decode.opcode   = opcode;
    decode.funct3   = funct3;
    decode.f_unit   = get_f_unit(opcode, funct3);
    decode.csr_val  = csr_val;
    decode.csr_num  = csr_num;
    decode.csr_wb   = csr_wb;
    decode.is_csr   = is_csr;

    ////////////////////////////////////
    //       PIPELINE CONTROL         //
    ////////////////////////////////////

    // STALLS: Dependency, branch, exception/interrupt
    u8 dec_rd  = prev_decode.stage_latches.rd;
    u8 dec_v   = prev_decode.stage_latches.valid;
    u8 exec_rd = prev_execute.stage_latches.rd;
    u8 exec_v  = prev_execute.stage_latches.valid;
    u8 mem_rd  = prev_memory.stage_latches.rd;
    u8 mem_v   = prev_memory.stage_latches.valid;
    if ((dec_rd  != 0 && dec_v  && (dec_rd  == rs1 || dec_rd  == rs2))
     || (exec_rd != 0 && exec_v && (exec_rd == rs1 || exec_rd == rs2))
     || (mem_rd  != 0 && mem_v  && (mem_rd  == rs1 || mem_rd  == rs2))) {
        // A valid, in-flight instruction must have the same destination
        // register as one of this instruction's source registers.
        // TODO: Add register forwarding.
        // TODO: Optional verbose messages to show the depedence.
        stalls.decode_dep_stall = fetch.valid;
    } else {
        stalls.decode_dep_stall = false;
    }
    stalls.decode_br_stall = fetch.valid && ctrl;
    stalls.decode_ei_stall = fetch.valid && ecall; // TODO: expand
    stalls.decode_ca_stall = fetch.valid && is_csr;

    // VALID BIT:
    // 1) The latches from fetch were valid.
    // 2) There is no data dependency.
    // 3) There are no branch/jump instructions in execute or memory.
    // 4) There is no ECALL later in the pipeline.
    decode.valid = fetch.valid && !(stalls.decode_dep_stall
                || stalls.execute_br_stall || stalls.memory_br_stall
                || stalls.execute_ei_stall || stalls.memory_ei_stall
                || stalls.execute_ca_stall || stalls.memory_ca_stall);

    // PROPAGATE: No memory stall
    bool propagate = !(stalls.memory_mem_stall);
    if (propagate) {
        prev_decode.stage_latches = next_decode.stage_latches;
    }
}

bool check_ctrl(u32 ir)
{
    /* All control transfer instructions have ir[6:4] = 110:
     *  11000: BRANCH
     *  11001: JALR
     *  11010: (reserved)
     *  11011: JAL
     * I need to be careful of the (reserved) case. For now, ignore all
     * but these three bits.
     */
    return ((ir >> 4) & 0x7) == 0x6;
}

u8 get_instr_type(u8 opcode)
{
    switch (opcode) {
        case OP:
            return R_Type;
        case OP_IMM:
        case OP_LOAD:
        case OP_JALR:
        case OP_SYSTEM:
            return I_Type;
        case OP_STORE:
            return S_Type;
        case OP_BRANCH:
            return B_Type;
        case OP_LUI:
        case OP_AUIPC:
            return U_Type;
        case OP_JAL:
            return J_Type;
        default:
            //printf("opcode = 0x%x\n", opcode);
            //assert(Valid_Opcode(opcode));
            return Unknown_Type;
    }
}

u8 get_opcode(u32 instr)
{
    return BIT_SEL(instr, 6, 0);
}

u8 get_rd(u32 instr, u8 itype)
{
    u8 rd = BIT_SEL(instr, 11, 7);
    switch (itype) {
        case R_Type:
        case I_Type:
        case U_Type:
        case J_Type:
            return rd;
        case S_Type:
        case B_Type:
        default:
            return 0;
    }
}

u8 get_rs1(u32 instr, u8 itype)
{
    u8 rs1 = BIT_SEL(instr, 19, 15);
    switch (itype) {
        case R_Type:
        case I_Type:
        case S_Type:
        case B_Type:
            return rs1;
        case U_Type:
        case J_Type:
        default:
            return 0;
    }
}

u8 get_rs2(u32 instr, u8 itype)
{
    u8 rs2 = BIT_SEL(instr, 24, 20);
    switch (itype) {
        case R_Type:
        case S_Type:
        case B_Type:
            return rs2;
        case I_Type:
        case U_Type:
        case J_Type:
        default:
            return 0;
    }
}

u32
get_alu_a(u8 itype, u8 opcode, u8 funct3, u32 rs1_val, u32 pc, u8 csr_imm)
{
    switch (itype) {
        case R_Type:
        case S_Type:
            return rs1_val;

        case I_Type:
            if (opcode == OP_SYSTEM && is_f3_csri(funct3)) {
                // CSRRWI, CSRRSI, and CSRRCI pass an immediate to the ALU
                return csr_imm;
            } else {
                return rs1_val;
            }

        case B_Type:
        case U_Type:
        case J_Type:
            return pc;

        default:
            return 0;
    };
}

u32
get_alu_b(u8 itype, u8 opcode, u8 funct3, u32 rs2_val, u32 imm, u32 csr_val)
{
    switch (itype) {
        case R_Type:
            return rs2_val;

        case B_Type:
        case S_Type:
        case U_Type:
        case J_Type:
            return imm;

        case I_Type:
            if (opcode == OP_SYSTEM && is_f3_csr(funct3)) {
                if (is_f3_csrw(funct3)) {
                    // For CSRRW[I], add the contents of rs1 to 0
                    return 0;
                } else {
                    return csr_val;
                }
            } else if (opcode == OP_JALR) {
                // "The JALR instruction ignores the lowest bit of the
                // calculated address." - Volume I, p. 16
                return MASK(imm, 31, 1);
            } else {
                return imm;
            }

        default:
            return 0;
    };
}

u8
get_alu_op(u8 itype, u8 opcode, u8 funct3)
{
    switch (itype) {
        case R_Type:
            return funct3;

        case S_Type:
        case B_Type:
        case U_Type:
        case J_Type:
            return F3_ADD;

        case I_Type:
            if (opcode == OP_JALR || opcode == OP_LOAD) {
                return F3_ADD;
            } else if (opcode == OP_SYSTEM) {
                if (funct3 == F3_CSRRS || funct3 == F3_CSRRSI) {
                    return F3_OR;
                } else if (funct3 == F3_CSRRC || funct3 == F3_CSRRCI) {
                    return F3_AND;
                } else if (funct3 == F3_CSRRW || funct3 == F3_CSRRWI) {
                    // Could also pass through in aux, but that might make
                    // decode logic more complex. OR with 0 is easy.
                    return F3_OR;
                }
            } else {
                return funct3;
            }

        default:
            return 0;
    }
}

bool
is_alu_addr(u8 itype, u8 opcode)
{
    switch (itype) {
        case R_Type:
        case U_Type:
            return false;

        case S_Type:
        case B_Type:
        case J_Type:
            return true;

        case I_Type:
            if (opcode == OP_JALR || opcode == OP_LOAD) {
                return true;
            } else {
                return false;
            }

        default:
            return false;
    }
}

bool
get_alu_inv_a(u8 opcode, u8 funct3)
{
    if (opcode == OP_SYSTEM) {
        if (funct3 == F3_CSRRC || funct3 == F3_CSRRCI) {
            return true;
        }
    }
    return false;
}

u32
get_cmp_a(u8 itype, u32 rs1_val)
{
    if (itype == B_Type) {
        return rs1_val;
    } else {
        return 0;
    }
}

u32
get_cmp_b(u8 itype, u32 rs2_val)
{
    if (itype == B_Type) {
        return rs2_val;
    } else {
        return 0;
    }
}

u32
get_aux(u8 itype, bool ctrl, u32 rs2_val, u32 imm, u32 link)
{
    switch (itype) {
        case S_Type:
            return rs2_val;

        case U_Type:
            // We technically don't need to return anything for AUIPC, but it
            // makes the logic simpler to do so anyway.
            return imm;

        case J_Type:
        case I_Type:
        case B_Type:
            if (ctrl) {
                return link;
            } else {
                return 0;
            }

        case R_Type:
        default:
            return 0;
    }
}

bool
is_aux_res(u8 opcode)
{
    return opcode == OP_STORE || opcode == OP_LUI || opcode == OP_JAL
        || opcode == OP_JALR;
}

u8 get_funct3(u32 instr, u8 itype)
{
    u8 funct3 = BIT_SEL(instr, 14, 12);
    switch (itype) {
        case R_Type:
        case I_Type:
        case S_Type:
        case B_Type:
            return funct3;
        case U_Type:
        case J_Type:
        default:
            return 0;
    }
}

u8 get_funct7(u32 instr, u8 itype, u8 opcode, u8 funct3)
{
    u8 funct7 = BIT_SEL(instr, 31, 25);
    switch (itype) {
        case R_Type:
            return funct7;
        case I_Type:
            // The only immediate instructions with a funct7 are the shifts.
            // For both logical shifts, it is 0. For the SRAI, 010000000.
            if (opcode == OP_IMM && funct3 == F3_SHFR) {
                return funct7;
            } else {
                return 0;
            }
        case S_Type:
        case B_Type:
        case U_Type:
        case J_Type:
        default:
            return 0;
    }
}

u8 get_f_unit(u8 opcode, u8 funct3)
{
    switch (opcode) {
        case OP:
        case OP_IMM:
            switch (funct3) {
                case F3_ADD:
                case F3_AND:
                case F3_OR:
                case F3_XOR:
                    return F_UNIT_ALU;
                case F3_SHFL:
                case F3_SHFR:
                    return F_UNIT_SHF;
                case F3_SLT:
                case F3_SLTU:
                    return F_UNIT_CMP;
                default:
                    return F_UNIT_NOP;
            }

        case OP_AUIPC:
        case OP_BRANCH:
        case OP_JAL:
        case OP_JALR:
        case OP_LOAD:
        case OP_STORE:
            return F_UNIT_ALU;

        case OP_SYSTEM:
            if (is_f3_csr(funct3)) {
                return F_UNIT_ALU;
            } else {
                return F_UNIT_NOP;
            }

        case OP_LUI:
            return F_UNIT_NOP;

        default:
            assert(Valid_Opcode(opcode));
            return 0;
    }
}

u32 get_imm(u32 instr, u8 itype)
{
    switch (itype) {
        case R_Type:
            return 0;
        case I_Type:
            return get_i_imm(instr);
        case S_Type:
            return get_s_imm(instr);
        case B_Type:
            return get_b_imm(instr);
        case U_Type:
            return get_u_imm(instr);
        case J_Type:
            return get_j_imm(instr);
        default:
            assert((itype != Unknown_Type));
            return 0;
    }
}

u32 get_i_imm(u32 instr)
{
    u32 imm = BIT_SEL(instr, 31, 20);
    return SEXT(imm, 12);
}

u32 get_s_imm(u32 instr)
{
    u8 lower_5 = BIT_SEL(instr, 11, 7);
    u8 upper_7 = BIT_SEL(instr, 31, 25);
    u32 imm = lower_5 + (upper_7 << 5);
    return SEXT(imm, 12);
}

u32 get_b_imm(u32 instr)
{
    u8 bit_11  = BIT_SEL(instr, 7, 7);
    u8 lower_4 = BIT_SEL(instr, 11, 8);
    u8 upper_6 = BIT_SEL(instr, 30, 25);
    u8 bit_12  = BIT_SEL(instr, 31, 31);
    u32 imm = (lower_4 << 1) + (upper_6 << 5)
            + (bit_11 << 11) + (bit_12 << 12);
    return SEXT(imm, 13);
}

u32 get_u_imm(u32 instr)
{
    u32 imm = MASK(instr, 31, 12);
    return imm;
}

u32 get_j_imm(u32 instr)
{
    u16 lower_10 = BIT_SEL(instr, 30, 21);
    u8 bit_11    = BIT_SEL(instr, 20, 20);
    u8 upper_8   = BIT_SEL(instr, 19, 12);
    u8 bit_20    = BIT_SEL(instr, 31, 31);
    u32 imm = (lower_10 << 1) + (bit_11 << 11)
            + (upper_8 << 12) + (bit_20 << 20);
    return SEXT(imm, 21);
}

u16
get_csr_num(u32 instr)
{
    return BIT_SEL(instr, 31, 20);
}

bool
is_f3_csri(u8 funct3)
{
    return (funct3 & F3_CSRI_M) != 0;
}

bool
is_f3_csr(u8 funct3)
{
    return (funct3 & F3_CSR_M) != 0;
}

bool
is_f3_csrw(u8 funct3)
{
    return BIT_SEL(funct3, 1, 0) == 0x1;
}
