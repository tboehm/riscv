#include "trap.h"

#include "encodings.h"
#include "util.h"

void
trap_handler(void)
{
    u32 mcause;
    u32 trap;

    mcause = read_csr(mcause);
    trap = mcause & (~INTERRUPT_MASK);

    if (mcause & INTERRUPT_MASK) {
        (*Interrupts[trap])();
    } else {
        (*Exceptions[trap])();
    }
}

void
trap_not_implemented(void)
{
    while (1) {
        ; // Spin
    }
}
