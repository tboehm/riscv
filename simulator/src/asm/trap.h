// Copyright (c) 2020 Trey Boehm
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.


#ifndef __TRAP_H__
#define __TRAP_H__

/**
 * @file trap.h
 *
 * Trap tables and handlers.
 *
 * This file is not part of the source code for the simulator. Rather, it is
 * supporting assembly code that we need to load into the proper place in memory
 * at runtime. It contains the reset vector, a trap handler to save registers,
 * and a higher-level trap handler in C to determine the cause and take action.
 *
 * @author Trey Boehm
 */

#include "encodings.h"

// The following CSR macros are from the riscv-compliance suite.
//
// Copyright (c) 2012-2015, The Regents of the University of California
// (Regents).  All Rights Reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. Neither the name of the Regents nor the
//    names of its contributors may be used to endorse or promote products
//    derived from this software without specific prior written permission.
//
// IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT,
// SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
// ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
// REGENTS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE. THE SOFTWARE AND ACCOMPANYING DOCUMENTATION, IF ANY, PROVIDED
// HEREUNDER IS PROVIDED "AS IS". REGENTS HAS NO OBLIGATION TO PROVIDE
// MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

/// Read a value from a CSR
#define read_csr(reg) ({ unsigned long __tmp; \
  asm volatile ("csrr %0, " #reg : "=r"(__tmp)); \
  __tmp; })

/// Write a value to a CSR
#define write_csr(reg, val) ({ \
  asm volatile ("csrw " #reg ", %0" :: "rK"(val)); })

/// Atomically swap the value in a CSR
#define swap_csr(reg, val) ({ unsigned long __tmp; \
  asm volatile ("csrrw %0, " #reg ", %1" : "=r"(__tmp) : "rK"(val)); \
  __tmp; })

/// Set bits in a CSR
#define set_csr(reg, bit) ({ unsigned long __tmp; \
  asm volatile ("csrrs %0, " #reg ", %1" : "=r"(__tmp) : "rK"(bit)); \
  __tmp; })

/// Clear bits in a CSR
#define clear_csr(reg, bit) ({ unsigned long __tmp; \
  asm volatile ("csrrc %0, " #reg ", %1" : "=r"(__tmp) : "rK"(bit)); \
  __tmp; })

// (end of copyrighted riscv-compliance section)

/**
 * Entry point for all traps.
 *
 * @return None.
 */
void trap_handler(void);

/**
 * An exception or interrupt that is not implemented
 *
 * Spin forever.
 *
 * @return None.
 */
void trap_not_implemented(void);

/// Exception handlers
void (*Exceptions[])(void) = {
    // Instructions
    [EXC_INSTR_ADDR_MISALGN] = trap_not_implemented,
    [EXC_INSTR_ADDR_FAULT]   = trap_not_implemented,
    [EXC_ILLEGAL_INSTR]      = trap_not_implemented,
    [EXC_BREAKPOINT]         = trap_not_implemented,

    // Load/store/AMO accesses
    [EXC_LOAD_ADDR_MISALGN]      = trap_not_implemented,
    [EXC_LOAD_ACCESS_FAULT]      = trap_not_implemented,
    [EXC_STORE_AMO_ADDR_MISALGN] = trap_not_implemented,
    [EXC_STORE_AMO_ACCESS_FAULT] = trap_not_implemented,

    // ECALL
    [EXC_ECALL_U_MODE] = trap_not_implemented,
    [EXC_ECALL_H_MODE] = trap_not_implemented,
    [EXC_RESERVED_10]  = trap_not_implemented,
    [EXC_ECALL_M_MODE] = trap_not_implemented,

    // Page faults
    [EXC_INSTR_PAGE_FAULT]     = trap_not_implemented,
    [EXC_LOAD_PAGE_FAULT]      = trap_not_implemented,
    [EXC_RESERVED_14]          = trap_not_implemented,
    [EXC_STORE_AMO_PAGE_FAULT] = trap_not_implemented,
};

/// Interrupt handlers
void (*Interrupts[])(void) = {
    // Software interrupts
    [INT_USER_SOFTWARE]       = trap_not_implemented,
    [INT_SUPERVISOR_SOFTWARE] = trap_not_implemented,
    [INT_RESERVED_2]          = trap_not_implemented,
    [INT_MACHINE_SOFTWARE]    = trap_not_implemented,

    // Timer interrupts
    [INT_USER_TIMER]       = trap_not_implemented,
    [INT_SUPERVISOR_TIMER] = trap_not_implemented,
    [INT_RESERVED_6]       = trap_not_implemented,
    [INT_MACHINE_TIMER]    = trap_not_implemented,

    // External interrupts
    [INT_USER_EXTERNAL]       = trap_not_implemented,
    [INT_SUPERVISOR_EXTERNAL] = trap_not_implemented,
    [INT_RESERVED_10]         = trap_not_implemented,
    [INT_MACHINE_EXTERNAL]    = trap_not_implemented,
};

#endif // __TRAP_H__
