/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __ELF_H__
#define __ELF_H__

/**
 * @file elf.h
 *
 * Load ELF files into the simulator
 *
 * @author Trey Boehm
 * @date 2019-05-20
 */

#include <stdio.h>

#include "util.h"

using namespace inttypes;

/// Size of an ELF header, in bytes
#define ELF_HEADER_SIZE     52
/// Size of a program header, in bytes
#define PROGRAM_HEADER_SIZE 32
/// ELF magic bytes (Little Endian)
#define ELF_MAGIC           0x464c457f
/// EI_CLASS value for 32-bit ELF
#define ELF_32              1
/// EI_DATA value for Little Endian
#define ELF_LITTLE_ENDIAN   1
/// ELF header ISA number for RISC-V
#define ISA_RISC_V 0xf3
/// Loadable program section
#define PT_LOAD 0x1

/// Definitions for offsets into the ELF header
enum ELF_Offsets {
    EI_MAG0       = 0x00, // ELF magic byte 0, 0x7f
    EI_MAG1       = 0x01, // ELF magic byte 1, 'E'
    EI_MAG2       = 0x02, // ELF magic byte 2, 'L'
    EI_MAG3       = 0x03, // ELF magic byte 2, 'F'
    EI_CLASS      = 0x04, // Architecture. 1 for 32-bit, 2 for 64-bit
    EI_DATA       = 0x05, // Byte order. 1 for little endian, 2 for big
    EI_VERSON     = 0x06, // ELF version. 1 for current version
    EI_OSABI      = 0x07, // Target operating system ABI
    EI_ABIVERSION = 0x08, // ABI-dependent
    EI_PAD        = 0x09, // Unused
    EH_TYPE       = 0x10, // Identifies object file type
    EH_MACHINE    = 0x12, // Specifies ISA
    EH_VERSION    = 0x14, // Set to 1 for original version of ELF
    EH_ENTRY      = 0x18, // Entry point for the program
    EH_PHOFF      = 0x1c, // Start of program header table
    EH_SHOFF      = 0x20, // Start of section header table
    EH_FLAGS      = 0x24, // Architecture-dependent
    EH_EHSIZE     = 0x28, // Size of this header (52 for 32-bit)
    EH_PHENTSIZE  = 0x2a, // Size of a program header table entry
    EH_PHNUM      = 0x2c, // Number of entries in program header table
    EH_SHENTSIZE  = 0x2e, // Size of a section header table entry
    EH_SHNUM      = 0x30, // Number of entries in section header table
    EH_SHSTRNDX   = 0x32, // Index of the section header table that
                          // contains the section names
};

/**
 * Fields included in the ELF header
 *
 * Padding is included (ei_pad1, ei_pad2, ei_pad3) so we can fread from
 * the image directly into the struct.
 */
struct ELF_Header {
    uint32_t ei_magic;
    uint8_t  ei_class;
    uint8_t  ei_data;
    uint8_t  ei_version;
    uint8_t  ei_osabi;
    uint8_t  ei_abiversion;
    uint8_t  ei_pad1;
    uint16_t ei_pad2;
    uint32_t ei_pad4;
    uint16_t e_type;
    uint16_t e_machine;
    uint32_t e_version;
    uint32_t e_entry;
    uint32_t e_phoff;
    uint32_t e_shoff;
    uint32_t e_flags;
    uint16_t e_ehsize;
    uint16_t e_phentsize;
    uint16_t e_phnum;
    uint16_t e_shentsize;
    uint16_t e_shnum;
    uint16_t e_shstrndx;
};

/// Fields included in a program header
struct Program_Header {
    uint32_t p_type;   ///< Type of segment
    uint32_t p_offset; ///< Offset of the segment in the file
    uint32_t p_vaddr;  ///< Virtual address of the segment in memory
    uint32_t p_paddr;  ///< Segment's physical address, if relevant
    uint32_t p_filesz; ///< Size in bytes of the segment in the file
    uint32_t p_memsz;  ///< Size in bytes of the segment in memory
    uint32_t p_flags;  ///< Segment-dependent flags
    uint32_t p_align;  ///< Alignment (power of 2, or 0/1 for none)
};

/**
 * Read in an ELF header from a file.
 *
 * @return None.
 */
void ELF_ReadELFHeader(
    FILE *image,                  ///< File to read from
    struct ELF_Header *elf_header ///< Struct to read into
);

/**
 * Read in a program header from a file.
 *
 * @return None.
 */
void ELF_ReadProgramHeader(
    FILE *image,                     ///< File to read from
    struct ELF_Header *elf_header,   ///< ELF header, for indices
    struct Program_Header *p_header, ///< Program header to read into
    uint16_t phnum                   ///< Program number to read
);

/**
 * Load a program into memory.
 *
 * @return The entry point (starting PC)
 */
u32 ELF_LoadProgram(
    FILE *image ///< File to read from
);

#endif // __ELF_H__
