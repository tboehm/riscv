/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __COLOR_H__
#define __COLOR_H__

/**
 * @file color.h
 *
 * Colored text output
 *
 * @author Trey Boehm
 */

#include <string>

#include "util.h"

using namespace inttypes;

/// Text color code definitions
enum Color_Code : uint8_t {
    COLOR_WHITE = 0,
    COLOR_RED,
    COLOR_GREEN,
    COLOR_YELLOW,
    COLOR_BLUE,
    COLOR_MAGENTA,
    COLOR_CYAN,
    COLOR_BLACK,
    COLOR_SET,
    COLOR_DEFAULT,
    NUM_COLORS,
};

/// Text style code definitions
enum Color_Style : uint8_t {
    STYLE_NONE      = 0,
    STYLE_BOLD      = 1,
    STYLE_ITALIC    = 3,
    STYLE_UNDERLINE = 4,
    STYLE_REVERSE   = 7,
};

/**
 * Get the escape sequence for color and style formatting.
 *
 * @return The escape sequence.
 */
std::string Color(
    Color_Code fg,    ///< Foreground color
    Color_Code bg,    ///< Background color
    Color_Style style ///< Text style (bold, italic, etc.)
);

/**
 * Set the color and style for text output
 *
 * @return None.
 */
void Color_Set(
    Color_Code fg,    ///< Foreground color
    Color_Code bg,    ///< Background color
    Color_Style style ///< Text style (bold, italic, etc.)
);

/**
 * Reset the color and style for text output to default.
 *
 * @return None.
 */
void Color_Reset(void);

#endif // __COLOR_H__
