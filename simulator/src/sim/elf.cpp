/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "elf.h"

#include <assert.h>
#include <stddef.h>
#include <stdio.h>

#include "memory.h"
#include "util.h"

using namespace inttypes;

u32 ELF_LoadProgram(FILE *image)
{
    struct ELF_Header elf_header;
    struct Program_Header p_header;
    u16 phnum;
    u32 vaddr;
    u32 file_offset;
    u32 size;
    u32 offset;
    u8 *page;
    u32 remaining;
    u32 load_size;

    // Memory must be initialized in order to load in a program
    if (get_pagedir() == NULL) {
        fprintf(stderr,
                "Must initialize memory before loading a program\n");
    };


    // Make sure to start at the beginning of the file
    rewind(image);
    ELF_ReadELFHeader(image, &elf_header);

    // Load any loadable program section into memory
    for (phnum = 0; phnum < elf_header.e_phnum; phnum++) {
        ELF_ReadProgramHeader(image, &elf_header, &p_header, phnum);
        if (p_header.p_type == PT_LOAD) {
            vaddr = p_header.p_vaddr;
            file_offset = p_header.p_offset;
            size = p_header.p_filesz;
            // Seek to the start of the segment
            fseek(image, file_offset, SEEK_SET);
            // Transfer byte-by-byte from file to memory
            for (offset = 0; offset < size; offset += PAGE_SIZE) {
                // Get a new page
                page = get_page(vaddr + offset);
                remaining = size - offset;
                load_size = (remaining < PAGE_SIZE) ? remaining : PAGE_SIZE;
                fread(page, load_size, 1, image);
            }
        }
    }

    // DEBUG: Print out what was just loaded in
    /*
    u8 data;
    u32 word;
    u8 count;

    for (phnum = 0; phnum < elf_header.e_phnum; phnum++) {
        ELF_ReadProgramHeader(image, &elf_header, &p_header, phnum);
        if (p_header.p_type == PT_LOAD) {
            vaddr = p_header.p_vaddr;
            size = p_header.p_filesz;
            // Transfer byte-by-byte from file to memory
            word = 0;
            count = 0;
            for (offset = 0; offset < size; offset++) {
                data = memread(vaddr + offset, sizeof(data));
                // Little Endian junk
                word >>= 8;
                word |= (data << 24);
                count++;
                if ((count == 4) || (offset + 1 == size)) {
                    if (offset + 1 == size) {
                        word >>= ((4 - count) * 8);
                    }
                    printf("0x%.8x\n", word);
                    word = 0;
                    count = 0;
                }
            }
        }
    }
    */

    // Return the entry point (starting PC)
    return elf_header.e_entry;
}

void ELF_ReadELFHeader(FILE *image, struct ELF_Header *elf_header)
{
    assert(ELF_HEADER_SIZE == sizeof(struct ELF_Header));
    fread(elf_header, sizeof(u8), ELF_HEADER_SIZE, image);

    if (elf_header->ei_magic != ELF_MAGIC) {
        fprintf(stderr, "Not an ELF file - it has the wrong "
                  "magic bytes at the start\n");
        exit(EXIT_FAILURE);
    }


    if (elf_header->ei_data != ELF_LITTLE_ENDIAN) {
        fprintf(stderr, "Not a Little Endian ELF\n");
        exit(EXIT_FAILURE);
    }

    if (elf_header->ei_class != ELF_32) {
        fprintf(stderr, "Not a 32-bit ELF\n");
        exit(EXIT_FAILURE);
    }

    if (elf_header->e_machine != ISA_RISC_V) {
        fprintf(stderr, "Not a RISC-V ELF\n");
        exit(EXIT_FAILURE);
    }
}

void ELF_ReadProgramHeader(FILE *image, struct ELF_Header *elf_header,
        struct Program_Header *p_header, u16 phnum)
{
    u32 pos;
    assert(PROGRAM_HEADER_SIZE == sizeof(struct Program_Header));

    pos = elf_header->e_phoff + (phnum * elf_header->e_phentsize);
    fseek(image, pos, SEEK_SET);
    fread(p_header, sizeof(u8), PROGRAM_HEADER_SIZE, image);
}


