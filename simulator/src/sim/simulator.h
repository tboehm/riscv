/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __SIMULATOR_H__
#define __SIMULATOR_H__

/**
 * @file simulator.h
 *
 * Simulate a RISC-V program.
 *
 * This file is sort of a mess right now. It loads a program into
 * memory and then calls on other files to initialize the machine's
 * registers and simulate the program.
 *
 * @author Trey Boehm
 */

#include "processor.h"
#include "util.h"

using std::string;
using std::vector;
using namespace inttypes;

/**
 * Initialize the machine.
 *
 * Load in the program and set up all of the machine's registers. By
 * default, the PC starts at 0. Only one program is allowed. This must
 * be extended at some point, but right now I'm focusing on properly
 * simulating each instruction.
 *
 * @return An instance of a CPU
 */
CPU init(
    int argc,    ///< argc from main
    char *argv[] ///< argv from main (name of the file to load)
);

/**
 * Simulate the program.
 *
 * Run the program, starting with PC = 0, until either an invalid
 * instruction is reached or an entirely 0 (0x00000000) instruction
 * is reached.
 *
 * @return The status of the simulator
 */
int simulate(
    CPU &core ///< The CPU to simulate
);

/**
 * Split up a string into tokens separated by some delimeter.
 *
 * @return Tokens contained in str
 */
vector<string> split(
    string str,  ///< String to split up
    string delim ///< Delimeter string
);

/**
 * Get and run a user command.
 *
 * Get a command from the user (standard input) and run it. The command
 * may be one of:
 *
 *  * c[olor]
 *  * nc/nocolor
 *  * d[elta]
 *  * nd/nodelta
 *  * n[ext]
 *  * md[ump] (start) (end)
 *  * rd[ump] (columns)
 *  * ad[ump] (columns)
 *  * run (cycles)
 *  * go
 *  * q[uit]
 *  * t[erse]
 *  * v[erbose]
 *
 * @return None.
 */
void run_command(
    CPU &core ///< The CPU to run the command on
);

#endif // __SIMULATOR_H__
