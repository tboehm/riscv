/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "memory.h"

#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

static u8 ***Memory = NULL;

void load_hex(FILE *fp)
{
    char c;

    u32 addr;
    u32 word;

    for (addr = 0; (c = getc(fp)) != EOF; addr += 4) {
        if (c == '\n') {
            addr -= 4;
        } else {
            ungetc(c, fp);
            word = getword(fp);
            memwrite(word, addr, 4);
        }
    }
}

void mem_init(void)
{
    // Right now, PTEs are actually just pointers. This gets us the
    // illusion of infinite memory without the memory protection
    // benefits (and complexity) of real virtual memory.
    Memory = (u8 ***)calloc(1, (PAGE_SIZE / PTE_SIZE) * sizeof(u8 **));
    assert(Memory != NULL);
}


u8 ***get_pagedir(void)
{
    return Memory;
}

u8 *get_page(u32 addr)
{
    u32 vpn1;
    u32 vpn0;
    u8 ***page_dir;
    u8 **page_table;
    u8 *page;

    vpn1   = SV32_VPN1(addr);
    vpn0   = SV32_VPN0(addr);

    page_dir = Memory;
    assert(page_dir != NULL);

    if (page_dir[vpn1] == NULL) {
        page_dir[vpn1] = (u8 **)calloc(1, (PAGE_SIZE / PTE_SIZE) * sizeof(u8 *));
    }
    page_table = page_dir[vpn1];
    assert(page_table != NULL);

    if (page_table[vpn0] == NULL) {
        page_table[vpn0] = (u8 *)calloc(1, PAGE_SIZE * sizeof(u8));
    }
    page = page_table[vpn0];
    assert(page != NULL);

    return page;
}

void memwrite(u32 data, u32 addr, size_t len)
{
    u32 offset;
    u8 *page;
    u8 byte;
    size_t i;

    page = get_page(addr);
    offset = SV32_OFFSET(addr);

    for (i = 0; i < len; i++) {
        byte = (data >> (i * 8)) & 0xff;
        page[offset + i] = byte;
    }
}

u32 memread(u32 addr, size_t len)
{
    u32 offset;
    u8 *page;
    u8 byte;
    u32 data;
    size_t i;

    page = get_page(addr);
    offset = SV32_OFFSET(addr);

    data = 0;
    for (i = 0; i < len; i++) {
        byte = *(page + offset + i);
        data += byte << (i * 8);
    }

    return data;
}

u8 tohex(char c)
{
    u8 hex;
    c = tolower(c);
    if (c >= 'a')
        hex = c - 'a' + 10;
    else
        hex = c - '0';
    return hex;
}

u8 tobyte(char c1, char c2)
{
    return (tohex(c1) << 4) + tohex(c2);
}

u32 getword(FILE *fp)
{
    char c1, c2;
    size_t i;
    u8 byte;
    u32 word = 0;
    for (i = 0; i < 4; i++) {
        c1 = getc(fp);
        c2 = getc(fp);
        byte = tobyte(c1, c2);
        word += (byte << 8*(3-i));
    }
    return word;
}
