/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "simulator.h"

#include <iostream>
#include <string>
#include <string.h>
#include <unistd.h>
#include <vector>

#include "elf.h"
#include "memory.h"
#include "processor.h"
#include "util.h"

// Avoid using the entire std namespace
using std::cin;
using std::cout;
using std::endl;
using std::getline;
using std::string;
using std::vector;
using namespace inttypes;

/// Indicate when to break out of the simulation loop
bool Simulating;

int
main(int argc, char *argv[])
{
    CPU core1 = init(argc, argv);
    simulate(core1);

    return 0;
}

void
usage(void)
{
    fprintf(stderr,
            "Usage: simulate --traps <trapfile> [--interactive] <program>\n");
    exit(EXIT_FAILURE);
}

CPU
init(int argc, char *argv[])
{
    char *token;
    FILE *infile;
    bool interactive;
    bool trap_loaded;
    u32 entry;

    // Initialize memory
    mem_init();

    interactive = false;
    trap_loaded = false;

    for (int i = 1; i < argc; i++) {
        token = argv[i];
        if (strcmp(token, "--interactive") == 0) {
            interactive = true;
        } else if (strcmp(token, "--traps") == 0) {
            // Trap routine
            i++;
            if (access(argv[i], R_OK) != 0) {
                usage();
            }
            infile = fopen(argv[i], "r");
            entry = ELF_LoadProgram(infile);
            fclose(infile);
            trap_loaded = true;
        } else if (access(argv[i], R_OK) != -1) {
            infile = fopen(argv[i], "r");
            ELF_LoadProgram(infile);
            fclose(infile);
        } else {
            usage();
        }
    }

    if (!trap_loaded) {
        usage();
    }

    /* Initialize the internal state of the processor */
    return CPU(entry, interactive);
}

int
simulate(CPU &core)
{
    Simulating = true;

    /* Notice: The order that I call the different stages determines
     * whether this is functioning as a true pipeline (multiple
     * instructions in-flight at once) or just an instruction-level
     * simulator. I don't want to worry about hazards right now so I
     * am just calling each stage in the order that they occur in the
     * pipeline.
     */
    while (Simulating) {
        run_command(core);
    }

    return EXIT_SUCCESS;
}

void
run_command(CPU &core)
{
    string cmd_string;
    static vector<string> last_tokens;
    vector<string> tokens;
    unsigned num_args;

    getline(cin, cmd_string);
    if (cin.eof())
        cmd_string = "quit";
    tokens = split(cmd_string, " ");

    /* If the user just hits enter, repeat the last command sequence */
    if (tokens[0] == "") {
        tokens = last_tokens;
    } else {
        last_tokens = tokens;
    }

    /* TODO: Handle bad input numbers */
    while (!tokens.empty()) {
        string cmd = tokens[0];
        if (cmd == "c" || cmd == "color") {
            core.set_color(true);
            num_args = 1;
        } else if (cmd == "nc" || cmd == "nocolor") {
            core.set_color(false);
            num_args = 1;
        } else if (cmd == "d" || cmd == "delta") {
            core.set_delta(true);
            num_args = 1;
        } else if (cmd == "nd" || cmd == "nodelta") {
            core.set_delta(false);
            num_args = 1;
        } else if (cmd == "md" || cmd == "mdump") {
            if (tokens.size() >= 3) {
                u32 start, end;
                u8 start_base, end_base;
                start_base = tokens[1][1] == 'x' ? 16 : 10;
                end_base   = tokens[2][1] == 'x' ? 16 : 10;
                start = stoul(tokens[1], nullptr, start_base);
                end   = stoul(tokens[2], nullptr, end_base);
                core.mdump(start, end);
                num_args = 3;
            } else {
                cout << "mdump: wrong number of operands" << endl;
                return;
            }
        } else if (cmd == "n" || cmd == "next") {
            core.run(1);
            num_args = 1;
        } else if (cmd == "nice") {
            core.set_nice(true);
            num_args = 1;
        } else if (cmd == "nonice") {
            core.set_nice(false);
            num_args = 1;
        } else if (cmd == "pd" || cmd == "pdump") {
            core.pdump();
            num_args = 1;
        } else if (cmd == "perf") {
            core.print_perf();
            num_args = 1;
        } else if (cmd == "rd" || cmd == "rdump"
                || cmd == "ad" || cmd == "adump") {
            /* "ad[ump]" indicates we want ABI register names */
            bool abi = (cmd[0] == 'a') ? true : false;

            if (tokens.size() == 1 || !isdigit(tokens[1][0])) {
                /* Default to 4 columns of registers */
                core.rdump(4, abi);
                num_args = 1;
            } else {
                u32 columns = stoi(tokens[1]);
                core.rdump(columns, abi);
                num_args = 2;
            }
        } else if (cmd == "run") {
            if (tokens.size() == 1 || !isdigit(tokens[1][0])) {
                core.run(0);
                num_args = 1;
            } else {
                u32 num_cycles = stoi(tokens[1]);
                core.run(num_cycles);
                num_args = 2;
            }
        } else if (cmd == "q" || cmd == "quit") {
            Simulating = false;
            num_args = 1;
        } else if (cmd == "t" || cmd == "terse") {
            core.set_verbose(false);
            num_args = 1;
        } else if (cmd == "v" || cmd == "verbose") {
            core.set_verbose(true);
            num_args = 1;
        } else {
            cout << "unknown command: " << tokens[0] << endl;
            return;
        }

        /* After running the command, remove tokens that were used */
        tokens.erase(tokens.begin(), tokens.begin() + num_args);
    }
}

vector<string>
split(string str, string delim)
{
    vector<string> tokens;
    size_t start = 0;
    size_t end = str.find(delim, start);

    while (end != string::npos) {
        /* Only push non-empty tokens */
        if (start != end) {
            tokens.push_back(str.substr(start, end-start));
        }
        start = end + delim.length();
        end = str.find(delim, start);
    }
    /* Push the last token */
    if (start != end) {
        tokens.push_back(str.substr(start, end));
    }

    return tokens;
}
