/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "color.h"

#include <iostream>
#include <string>

#include "util.h"

using namespace inttypes;

std::string Color(Color_Code fg, Color_Code bg, Color_Style style)
{
    if (fg > NUM_COLORS || bg > NUM_COLORS) {
        return "";
    }
    return "\033[" + std::to_string(style)
            + ";3" + std::to_string(fg)
            + ";4" + std::to_string(bg) + "m";
}

void Color_Set(Color_Code fg, Color_Code bg, Color_Style style)
{
    std::cout << Color(fg, bg, style);
}

void Color_Reset(void)
{
    std::cout << "\033[0m";
}
