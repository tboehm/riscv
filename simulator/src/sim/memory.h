/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __MEMORY_H__
#define __MEMORY_H__

/**
 * @file memory.h
 *
 * Initialization and control of memory accesses.
 *
 * This file defines the size of our memory. The current implementation
 * uses a static approach to memory, creating a single large array at
 * the start. There are also a few functions to access memory and assist
 * the "load" function.
 *
 * Do not confuse this file with "memory_stage.c", which deals with the
 * actual instructions that access memory (and uses some of these
 * functions).
 *
 * @author Trey Boehm
 */

#include <stdio.h>

#include "util.h"

using namespace inttypes;

/// Get vpn1 (page where the page table is located) of a virtual address
#define SV32_VPN1(addr) ((addr >> 22) & 0x3ff)

/// Get vpn0 (page where the data is located) of a virtual address
#define SV32_VPN0(addr) ((addr >> 12) & 0x3ff)

/// Get the byte offset of a virtual address
#define SV32_OFFSET(addr) (addr & 0xfff)

/// Size of a page, in bytes
#define PAGE_SIZE 4096

/// Size of a PTE, in bytes
#define PTE_SIZE 4

/**
 * Loads a hex file into memory.
 *
 * The file should be formatted as 8 hex characters per line (no 0x at
 * the start). The letters may be upper or lowercase. Currently, the
 * function loads in files starting at address 0.
 *
 * @return None.
 */
void load_hex(
    FILE *file ///< A pointer to the file to load into memory
);

/**
 * Initialize memory.
 *
 * Sets the "Memory" global variable to the address of the page
 * directory. Every entry in the page directory (i.e., pointer to page
 * table) is initialized to the null pointer.
 */
void mem_init(void);

/**
 * Get the page directory.
 *
 * Used to verify that memory has been initialized.
 *
 * @return Pointer to the page directory.
 */
u8 ***get_pagedir(void);

/**
 * Get the page associated with a virtual address
 *
 * Sv32 virtual address:
 *
 *     +-------------------+-------------------+----------------------+
 *     |       VPN 1       |       VPN 0       |     page offset      |
 *     +-------------------+-------------------+----------------------+
 *      31               22 21               12 11                   0
 *
 *  - Page directory: 1024 PTEs for page tables, each 4 bytes (4 KiB)
 *  - Page table: 1024 PTEs for pages, each 4 bytes (4 KiB)
 *  - Page: Data (4 KiB)
 *
 * Instead of actually keeping track of PTEs, I have the following:
 *      - Pages are uint8_t arrays of size 4096
 *      - Page tables are uint8_t* arrays of size 1024
 *      - Page directory is a uint8_t** array of size 1024
 *
 * Index into the page directory with vpn1 = addr[31:22] to get a page
 * table, then index into that page table with vpn0 = addr[21:12] to get
 * the page itself.
 */
uint8_t *get_page(uint32_t addr);

/**
 * Write some data to a particular address.
 *
 * Take between 1 and 4 bytes of data and write it into the "mem" array
 * at the address specified. This function does not check for unaligned
 * accesses, nor does in check that the range of addresses are valid. It
 * is the caller's responsibility to decide what is valid input and what
 * is not.
 *
 * @return None.
 */
void memwrite(
    u32 data,  ///< Data to put in memory
    u32 addr,  ///< Address to start writing
    size_t len ///< Number of bytes to write
);

/**
 * Read some data from a particular address.
 *
 * @return The data from memory
 */
u32 memread(
    u32 addr,  ///< Address to start reading
    size_t len ///< Number of bytes to read
);

/**
 * Convert an ASCII character to a hex value.
 *
 * Given some character (such as 'a', 'F', or '0'), return the numerical
 * value (10, 15, or 0 respectively).
 *
 * @return The numerical value of the character
 */
u8 tohex(
    char c ///< The character to convert
);

/**
 * Convent two hex characters to a byte.
 *
 * Characters c1 and c2 must be hex characters ('a', 'F', '0', etc.).
 * Put c1 in the high nibble of the of the byte and c2 in the low
 * nibble.
 *
 * @return A byte made from the numerical values of c1 and c2
 */
u8 tobyte(
    char c1, ///< A hex ASCII character for the high nibble
    char c2  ///< A hex ASCII character for the low nibble
);

/**
 * Get the next 4 bytes from the input file.
 *
 * Interpret the next 8 characters in the file as hex characters. Read
 * them 2 at a time, convert to a byte, and stick them in the word that
 * will be returned.
 *
 * @return The numerical value of the next 8 hex chars in the file
 */
u32 getword(
    FILE *fp ///< A pointer to the file to read from
);

#endif // __MEMORY_H__
