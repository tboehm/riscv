# glowbo

RISC-V assembler

## Basic procedure
 * Generate a symbol table with a flex parser (`Lexer.l`)
 * Tokenize the input file with a the same parser
 * Fit the assembly to a Bison grammar (`Grammar.y`)
 * If the line corresponds to a known form, try to assembly it

## What works
 * Recognizing and (sort of) assembling most base integer instructions (RV32I)
 * Recognizing assembler options and directives

## Next steps
 * Perform more rigorous test on the current assembly functions before moving forward with more extensions/instructions
 * The rest of RV32I (ECALL/EBREAK, CSRR\*), RV32M, and RV32A
 * _Some_ pseudo-instructions
 * Symbol table


## Dependencies

### General
 * gcc (or clang)
 * Make
 * [flex](https://www.gnu.org/software/flex)
 * [bison](https://www.gnu.org/software/bison)

### Test script
 * Python
 * [rv8](https://github.com/rv8-io/rv8)
 * [RISC-V GNU toolchain](https://github.com/riscv/riscv-gnu-toolchain)

