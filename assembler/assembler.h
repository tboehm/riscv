/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef __ASSEMBLER_H__
#define __ASSEMBLER_H__
#include <assert.h>
#include <ctype.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "encodings.h"
#include "util.h"

/************************ MACROS ************************/
#define LINE_LENGTH 1024
#define WORD_LENGTH 128
#define MAX_WORDS 128

#define UNDEFINED_INSTRUCTION 0xffffffff

/************************ STRUCTS ************************/
struct instr_base {
    char instr[10]; /* The name of the instruction */
    u32 base;       /* The base bits for the instruction */
};

/* The symbol table also will be set up as a linked list so that new
 * symbols can be easily added.
 */
typedef struct symtab_entry_struct {
    char* name;
    u32 addr;
    struct symtab_entry_struct *next;
} symtab_entry;

/* Assembled machine code ("mcode") will also be set up as a linked
 * list. Note that each time a label is encountered, the list must be
 * traversed so we can its address.
 */
typedef struct mcode_struct {
    u32 inst;
    u32 addr;
    struct mcode_struct *next;
} mcode;

/************************ CONSTANTS ************************/
/* XLEN refers to the width of 'x' registers in bits */
static const u32 XLEN = 32;

/* Shift amounts for the different pieces of an instruction */
//static const u32 RD_SHF = 7;         /* R, I, U-type */
//static const u32 IMM_4_0_SHF = 7;    /* S-type */
//static const u32 F3_SHF = 12;        /* R, I, S-type */
//static const u32 IMM_31_12_SHF = 12; /* U-type */
//static const u32 RS1_SHF = 15;       /* R, I, S-type */
//static const u32 RS2_SHF = 20;       /* R, S-type */
//static const u32 IMM_11_0_SHF = 20;  /* I-type */
//static const u32 F7_SHF = 25;        /* R-type */
//static const u32 IMM_11_5_SHF = 25;  /* S-type */

/* The FENCE instruction on its own implies FENCE IORW, IORW */
static const u32 IORW = 0xf;

/* Base instruction bits (opcode, funct3/7) */
/* ... for RV32I R-type instructions */
static const u32 add_base  = OP + (F3_ADD  << F3_SHF);
static const u32 sub_base  = OP + (F3_ADD  << F3_SHF) + (F7_NEG << F7_SHF);
static const u32 sll_base  = OP + (F3_SHFL << F3_SHF);
static const u32 slt_base  = OP + (F3_SLT  << F3_SHF);
static const u32 sltu_base = OP + (F3_SLTU << F3_SHF);
static const u32 xor_base  = OP + (F3_XOR  << F3_SHF);
static const u32 srl_base  = OP + (F3_SHFR << F3_SHF);
static const u32 sra_base  = OP + (F3_SHFR << F3_SHF) + (F7_NEG << F7_SHF);
static const u32 or_base   = OP + (F3_OR   << F3_SHF);
static const u32 and_base  = OP + (F3_AND  << F3_SHF);
/* ... for RV32I I-type instructions */
static const u32 jalr_base  = OP_JALR;
static const u32 addi_base  = OP_IMM  + (F3_ADD   << F3_SHF);
static const u32 slti_base  = OP_IMM  + (F3_SLT   << F3_SHF);
static const u32 sltiu_base = OP_IMM  + (F3_SLTU  << F3_SHF);
static const u32 xori_base  = OP_IMM  + (F3_XOR   << F3_SHF);
static const u32 ori_base   = OP_IMM  + (F3_OR    << F3_SHF);
static const u32 andi_base  = OP_IMM  + (F3_AND   << F3_SHF);
static const u32 slli_base  = OP_IMM  + (F3_SHFL  << F3_SHF);
static const u32 srli_base  = OP_IMM  + (F3_SHFR  << F3_SHF);
static const u32 srai_base  = OP_IMM  + (F3_SHFR  << F3_SHF) + (F7_NEG << F7_SHF);
static const u32 lb_base    = OP_LOAD + (F3_LB    << F3_SHF);
static const u32 lh_base    = OP_LOAD + (F3_LH    << F3_SHF);
static const u32 lw_base    = OP_LOAD + (F3_LW    << F3_SHF);
static const u32 lbu_base   = OP_LOAD + (F3_LBU   << F3_SHF);
static const u32 lhu_base   = OP_LOAD + (F3_LHU   << F3_SHF);
/* ... for RV32I S-type instructions */
static const u32 sb_base = OP_STORE + (F3_SB << F3_SHF);
static const u32 sh_base = OP_STORE + (F3_SH << F3_SHF);
static const u32 sw_base = OP_STORE + (F3_SW << F3_SHF);
/* ... for RV32I B-type instructions */
static const u32 beq_base  = OP_BRANCH + (F3_BEQ  << F3_SHF);
static const u32 bne_base  = OP_BRANCH + (F3_BNE  << F3_SHF);
static const u32 blt_base  = OP_BRANCH + (F3_BLT  << F3_SHF);
static const u32 bge_base  = OP_BRANCH + (F3_BGE  << F3_SHF);
static const u32 bltu_base = OP_BRANCH + (F3_BLTU << F3_SHF);
static const u32 bgeu_base = OP_BRANCH + (F3_BGEU << F3_SHF);
/* ... for RV32I U-type instructions */
static const u32 lui_base   = OP_LUI;
static const u32 auipc_base = OP_AUIPC;
/* ... for RV32I J-type instructions */
static const u32 jal_base = OP_JAL;
/* ... for fence instructions */
static const u32 fence_base = OP_MISC_MEM;
static const u32 fence_i_base = OP_MISC_MEM + (0x1 << F3_SHF);

/* Organize instructions into the six different types, associated with
 * their base bits (eg opcode, funct3, funct7,)
 */
static const struct instr_base R_Types[] = {
    {.instr = "add", .base = add_base},
    {.instr = "sub", .base = sub_base},
    {.instr = "sll", .base = sll_base},
    {.instr = "slt", .base = slt_base},
    {.instr = "sltu", .base = sltu_base},
    {.instr = "xor", .base = xor_base},
    {.instr = "srl", .base = srl_base},
    {.instr = "sra", .base = sra_base},
    {.instr = "or", .base = or_base},
    {.instr = "and", .base = and_base},
    {.instr = "\0", .base = 0}
};

static const struct instr_base I_Types[] = {
    {.instr = "jalr", .base = jalr_base},
    {.instr = "lb", .base = lb_base},
    {.instr = "lh", .base = lh_base},
    {.instr = "lw", .base = lw_base},
    {.instr = "lbu", .base = lbu_base},
    {.instr = "lhu", .base = lhu_base},
    {.instr = "addi", .base = addi_base},
    {.instr = "slti", .base = slti_base},
    {.instr = "sltiu", .base = sltiu_base},
    {.instr = "xori", .base = xori_base},
    {.instr = "ori", .base = ori_base},
    {.instr = "andi", .base = andi_base},
    {.instr = "slli", .base = slli_base},
    {.instr = "srli", .base = srli_base},
    {.instr = "srai", .base = srai_base},
    {.instr = "\0", .base = 0}
};

static const struct instr_base S_Types[] = {
    {.instr = "sb", .base = sb_base},
    {.instr = "sh", .base = sh_base},
    {.instr = "sw", .base = sw_base},
    {.instr = "\0", .base = 0}
};

static const struct instr_base B_Types[] = {
    {.instr = "beq", .base = beq_base},
    {.instr = "bne", .base = bne_base},
    {.instr = "blt", .base = blt_base},
    {.instr = "bge", .base = bge_base},
    {.instr = "bltu", .base = bltu_base},
    {.instr = "bgeu", .base = bgeu_base},
    {.instr = "\0", .base = 0}
};

static const struct instr_base U_Types[] = {
    {.instr = "lui", .base = lui_base},
    {.instr = "auipc", .base = auipc_base},
    {.instr = "\0", .base = 0}
};

static const struct instr_base J_Types[] = {
    {.instr = "jal", .base = jal_base},
    {.instr = "\0", .base = 0}
};

static const struct instr_base Fence_Types[] = {
    {.instr = "fence", .base = fence_base},
    {.instr = "fence.i", .base = fence_i_base},
    {.instr = "\0", .base = 0}
};

static const char Fence_Ops[][17] = {
    "",   "w",   "r",   "rw",
    "o",  "ow",  "or",  "orw",
    "i",  "iw",  "ir",  "irw",
    "io", "iow", "ior", "iorw",
    "\0"
};

/************************ FUNCTIONS ************************/
u32 get_base(const char *instr, const struct instr_base *base_array);
u32 get_fence_op(const char* op);
u32 asm_single(char *instr);
u32 asm_text_text(char *instr, char *text1, char *text2);
u32 asm_reg_imm(char *instr, u32 rd, u32 imm);
u32 asm_reg_reg_imm(char *instr, u32 r1, u32 r2, u32 imm);
u32 asm_ld_st(char *instr, u32 r1, u32 offset, u32 r2);
u32 asm_r_type(char *instr, u32 rd, u32 rs1, u32 rs2);
u32 asm_i_type(char *instr, u32 rd, u32 rs1, u32 imm);
u32 asm_s_type(char *instr, u32 rs2, u32 imm, u32 rs1);
u32 asm_b_type(char *instr, u32 rs1, u32 rs2, u32 imm);
u32 asm_u_type(char *instr, u32 rd, u32 imm);
u32 asm_j_type(char *instr, u32 rd, u32 imm);
u32 asm_fence_type(char *instr, u32 pred, u32 succ);
u32 calc_offset(u32 addr, char* labelname);
symtab_entry *new_sym(char* name, u32 addr);
int32_t sym_addr(char* labelname);
void print_symtab();
void free_symtab();
mcode *new_mcode_line(u32 inst, u32 addr);
void print_mcode();
void write_mcode(FILE* outfile);
void free_mcode();

#endif /* __ASSEMBLER_H__ */
