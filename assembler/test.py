#!/usr/bin/env python3

# Copyright (c) 2019 Trey Boehm
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import subprocess as sp
import sys
import os

Test_Dir = "tests"
RV_Asm = "riscv64-unknown-elf-as"

red = '\033[1;31m'
green = '\033[32m'
off = '\033[0m'
match = green + 'match' + off
failed = red + 'failed' + off
differ = red + 'differ' + off

def gen_key(infile, keyfile):
    # Assemble the test file with the GNU RISC-V assembler
    asm_cmd = "%s -o %s.orig %s" % (RV_Asm, keyfile, infile)
    os.system(asm_cmd);
    # Use RV8 to disassemble
    rv8_cmd = "rv-bin dump -d %s.orig | grep -Eo ' [0-9a-f]{8} ' > %s" % (keyfile, keyfile)
    os.system(rv8_cmd)
    # Remove the .orig file
    rm_cmd = "rm %s.orig" % keyfile
    os.system(rm_cmd)
    return keyfile

def run_test(file_base):
    infile  = "%s.s" % file_base
    outfile = "%s.out" % file_base
    keyfile = "%s.asm_key" % file_base
    keyfile = gen_key(infile, keyfile)

    assemble_cmd = "./assemble %s %s > %s" % (infile, outfile, outfile)
    os.system(assemble_cmd)

    diff = "diff -w %s %s" % (outfile, keyfile)
    child = sp.Popen(diff.split(), stdout=sp.PIPE)
    streamdata = child.communicate()
    rc = child.returncode
    sys.stdout.write("Files %s and %s " % (outfile, keyfile))
    if rc == 1:
        #print("\tFiles %s and %s %s" % (outfile, keyfile, differ))
        print(differ)
        os.system(diff)
    else:
        #print("\tThe output file and the key match - %s" % good)
        print(match)

if __name__ == "__main__":
    test_files = [os.path.join(Test_Dir, f) \
            for f in os.listdir(Test_Dir) if f.endswith(".s")]

    for f in test_files:
        base_name = os.path.splitext(f)[0]
        print("File %s:" % f)
        run_test(base_name)
