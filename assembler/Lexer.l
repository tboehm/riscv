/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

%{
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <ctype.h>
    #include "Grammar.tab.h"

    int streq(const char* str1, const char* str2);
    void strlower(char* src);
    int toInt(char* src);

    void copySval(const char* yytext, char** sval);

    int parseReg(char* yytext);
    int parseDirective(char* yytext);
    int parseArgument(char* yytext);
    int parseRelocation(char* yytext);
    int parseSection(char* yytext);
    int parseOption(char* yytext);
    int parseRV32I(char* yytext);
    int parseRV64I(char* yytext);
    int parsePseudo(char* yytext);
    int parseString(char* yytext);
    int parseNum(char* yytext);
    int parseNumLabel(char* yytext);
    int parseText(char* yytext);

    const char* regNums[] = {"zero", "ra", "sp", "gp", "tp", "t0", "t1",
        "t2", "fp", "s0", "s1", "a0", "a1", "a2", "a3", "a4", "a5",
        "a6", "a7", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9",
        "s10", "s11", "t3", "t4", "t5", "t6"
    };
%}

%option noyywrap
%option outfile="Lexer.yy.c" header-file="Lexer.yy.h"

%%

    /* Assembler directives */

\.(?i:align)        { return parseDirective(yytext); }
\.(?i:file)         { return parseDirective(yytext); }
\.(?i:globl)        { return parseDirective(yytext); }
\.(?i:local)        { return parseDirective(yytext); }
\.(?i:comm)         { return parseDirective(yytext); }
\.(?i:common)       { return parseDirective(yytext); }
\.(?i:ident)        { return parseDirective(yytext); }
\.(?i:section)      { return parseDirective(yytext); }
\.(?i:size)         { return parseDirective(yytext); }
\.(?i:text)         { return parseDirective(yytext); }
\.(?i:data)         { return parseDirective(yytext); }
\.(?i:rodata)       { return parseDirective(yytext); }
\.(?i:bss)          { return parseDirective(yytext); }
\.(?i:string)       { return parseDirective(yytext); }
\.(?i:asciz)        { return parseDirective(yytext); }
\.(?i:equ)          { return parseDirective(yytext); }
\.(?i:macro)        { return parseDirective(yytext); }
\.(?i:endm)         { return parseDirective(yytext); }
\.(?i:type)         { return parseDirective(yytext); }
\.(?i:option)       { return parseDirective(yytext); }
\.(?i:byte)         { return parseDirective(yytext); }
\.(?i:2byte)        { return parseDirective(yytext); }
\.(?i:4byte)        { return parseDirective(yytext); }
\.(?i:8byte)        { return parseDirective(yytext); }
\.(?i:half)         { return parseDirective(yytext); }
\.(?i:word)         { return parseDirective(yytext); }
\.(?i:dword)        { return parseDirective(yytext); }
\.(?i:dtprelword)   { return parseDirective(yytext); }
\.(?i:dtpreldword)  { return parseDirective(yytext); }
\.(?i:sleb128)      { return parseDirective(yytext); }
\.(?i:uleb128)      { return parseDirective(yytext); }
\.(?i:p2align)      { return parseDirective(yytext); }
\.(?i:balign)       { return parseDirective(yytext); }
\.(?i:zero)         { return parseDirective(yytext); }

    /* Directive arguments */
(?i:@function)      { return parseArgument(yytext); }

    /* Relocation functions */
%(?i:hi)            { return parseRelocation(yytext); }
%(?i:lo)            { return parseRelocation(yytext); }
%(?i:pcrel_hi)      { return parseRelocation(yytext); }
%(?i:pcrel_lo)      { return parseRelocation(yytext); }
%(?i:tprel_hi)      { return parseRelocation(yytext); }
%(?i:tprel_lo)      { return parseRelocation(yytext); }
%(?i:tprel_add)     { return parseRelocation(yytext); }

    /* Opcodes */

    /* ... RV32I: */
(?i:AUIPC)          { return parseRV32I(yytext); }
(?i:LUI)            { return parseRV32I(yytext); }
(?i:JAL)            { return parseRV32I(yytext); }
(?i:JALR)           { return parseRV32I(yytext); }
(?i:BEQ)            { return parseRV32I(yytext); }
(?i:BNE)            { return parseRV32I(yytext); }
(?i:BLT)            { return parseRV32I(yytext); }
(?i:BGE)            { return parseRV32I(yytext); }
(?i:BLTU)           { return parseRV32I(yytext); }
(?i:BGEU)           { return parseRV32I(yytext); }
(?i:LB)             { return parseRV32I(yytext); }
(?i:LH)             { return parseRV32I(yytext); }
(?i:LW)             { return parseRV32I(yytext); }
(?i:LBU)            { return parseRV32I(yytext); }
(?i:LHU)            { return parseRV32I(yytext); }
(?i:SB)             { return parseRV32I(yytext); }
(?i:SH)             { return parseRV32I(yytext); }
(?i:SW)             { return parseRV32I(yytext); }
(?i:ADDI)           { return parseRV32I(yytext); }
(?i:SLTI)           { return parseRV32I(yytext); }
(?i:SLTIU)          { return parseRV32I(yytext); }
(?i:XORI)           { return parseRV32I(yytext); }
(?i:ORI)            { return parseRV32I(yytext); }
(?i:ANDI)           { return parseRV32I(yytext); }
(?i:SLLI)           { return parseRV32I(yytext); }
(?i:SRLI)           { return parseRV32I(yytext); }
(?i:SRAI)           { return parseRV32I(yytext); }
(?i:ADD)            { return parseRV32I(yytext); }
(?i:SUB)            { return parseRV32I(yytext); }
(?i:SLL)            { return parseRV32I(yytext); }
(?i:SLT)            { return parseRV32I(yytext); }
(?i:SLTU)           { return parseRV32I(yytext); }
(?i:XOR)            { return parseRV32I(yytext); }
(?i:SRL)            { return parseRV32I(yytext); }
(?i:SRA)            { return parseRV32I(yytext); }
(?i:OR)             { return parseRV32I(yytext); }
(?i:AND)            { return parseRV32I(yytext); }
(?i:FENCE)          { return parseRV32I(yytext); }
(?i:FENCE\.I)       { return parseRV32I(yytext); }
(?i:ECALL)          { return parseRV32I(yytext); }
(?i:EBREAK)         { return parseRV32I(yytext); }
(?i:CSRRW)          { return parseRV32I(yytext); }
(?i:CSRRS)          { return parseRV32I(yytext); }
(?i:CSRRC)          { return parseRV32I(yytext); }
(?i:CSRRWI)         { return parseRV32I(yytext); }
(?i:CSRRSI)         { return parseRV32I(yytext); }
(?i:CSRRCI)         { return parseRV32I(yytext); }

    /* ... RV64I */
(?i:LWU)            { return parseRV64I(yytext); }
(?i:LD)             { return parseRV64I(yytext); }
(?i:SD)             { return parseRV64I(yytext); }
(?i:ADDIW)          { return parseRV64I(yytext); }
(?i:SLLIW)          { return parseRV64I(yytext); }
(?i:SRLIW)          { return parseRV64I(yytext); }
(?i:SRAIW)          { return parseRV64I(yytext); }
(?i:ADDW)           { return parseRV64I(yytext); }
(?i:SUBW)           { return parseRV64I(yytext); }
(?i:SLLW)           { return parseRV64I(yytext); }
(?i:SRLW)           { return parseRV64I(yytext); }
(?i:SRAW)           { return parseRV64I(yytext); }

    /* Pseudo-instructions. They basically act like opcodes. */
(?i:LA)             { return parsePseudo(yytext); }
(?i:FLW)            { return parsePseudo(yytext); }
(?i:FLD)            { return parsePseudo(yytext); }
(?i:FSW)            { return parsePseudo(yytext); }
(?i:FSD)            { return parsePseudo(yytext); }
(?i:NOP)            { return parsePseudo(yytext); }
(?i:LI)             { return parsePseudo(yytext); }
(?i:MV)             { return parsePseudo(yytext); }
(?i:NOT)            { return parsePseudo(yytext); }
(?i:NEG)            { return parsePseudo(yytext); }
(?i:NEGW)           { return parsePseudo(yytext); }
(?i:SEXT\.W)        { return parsePseudo(yytext); }
(?i:SEQZ)           { return parsePseudo(yytext); }
(?i:SNEZ)           { return parsePseudo(yytext); }
(?i:SLTZ)           { return parsePseudo(yytext); }
(?i:SGTZ)           { return parsePseudo(yytext); }
(?i:FMV\.S)         { return parsePseudo(yytext); }
(?i:FABS\.S)        { return parsePseudo(yytext); }
(?i:FNEG\.S)        { return parsePseudo(yytext); }
(?i:FMV\.D)         { return parsePseudo(yytext); }
(?i:FABS\.D)        { return parsePseudo(yytext); }
(?i:FNEG\.D)        { return parsePseudo(yytext); }
(?i:BEQZ)           { return parsePseudo(yytext); }
(?i:BNEZ)           { return parsePseudo(yytext); }
(?i:BLEZ)           { return parsePseudo(yytext); }
(?i:BGEZ)           { return parsePseudo(yytext); }
(?i:BLTZ)           { return parsePseudo(yytext); }
(?i:BGTZ)           { return parsePseudo(yytext); }
(?i:BGTBLE)         { return parsePseudo(yytext); }
(?i:BGTU)           { return parsePseudo(yytext); }
(?i:BLEU)           { return parsePseudo(yytext); }
(?i:J)              { return parsePseudo(yytext); }
(?i:JR)             { return parsePseudo(yytext); }
(?i:RET)            { return parsePseudo(yytext); }
(?i:CALL)           { return parsePseudo(yytext); }
(?i:TAIL)           { return parsePseudo(yytext); }
(?i:RDINSTRET)      { return parsePseudo(yytext); }
(?i:RDINSTRETH)     { return parsePseudo(yytext); }
(?i:RDCYCLE)        { return parsePseudo(yytext); }
(?i:RDCYCLEH)       { return parsePseudo(yytext); }
(?i:RDTIME)         { return parsePseudo(yytext); }
(?i:RDTIMEH)        { return parsePseudo(yytext); }
(?i:CSRR)           { return parsePseudo(yytext); }
(?i:CSRW)           { return parsePseudo(yytext); }
(?i:CSRS)           { return parsePseudo(yytext); }
(?i:CSRC)           { return parsePseudo(yytext); }
(?i:CSRWI)          { return parsePseudo(yytext); }
(?i:CSRSI)          { return parsePseudo(yytext); }
(?i:CSRCI)          { return parsePseudo(yytext); }
(?i:FRCSR)          { return parsePseudo(yytext); }
(?i:FSCSR)          { return parsePseudo(yytext); }
(?i:FRRM)           { return parsePseudo(yytext); }
(?i:FSRM)           { return parsePseudo(yytext); }
(?i:FSRMI)          { return parsePseudo(yytext); }
(?i:FRFLAGS)        { return parsePseudo(yytext); }
(?i:FSFLAGS)        { return parsePseudo(yytext); }
(?i:FSFLAGSI)       { return parsePseudo(yytext); }
    /* ... .option options */
(?i:RVC)            { return parseOption(yytext); }
(?i:NORVC)          { return parseOption(yytext); }
(?i:PIC)            { return parseOption(yytext); }
(?i:NOPIC)          { return parseOption(yytext); }
(?i:PUSH)           { return parseOption(yytext); }
(?i:POP)            { return parseOption(yytext); }

    /* ... .section types */
(?i:.TEXT)          { return parseSection(yytext); }
(?i:.DATA)          { return parseSection(yytext); }
(?i:.RODATA)        { return parseSection(yytext); }
(?i:.BSS)           { return parseSection(yytext); }

    /* Registers */
(?i:zero)           { return parseReg(yytext); }
(?i:ra)             { return parseReg(yytext); }
(?i:[fpst]p)        { return parseReg(yytext); }
(?i:s[0-9])         { return parseReg(yytext); }
(?i:s1[0-1])        { return parseReg(yytext); }
(?i:a[0-7])         { return parseReg(yytext); }
(?i:t[0-6])         { return parseReg(yytext); }
(?i:x[0-9])         { return parseReg(yytext); }
(?i:x[1-2][0-9])    { return parseReg(yytext); }
(?i:x3[0-1])        { return parseReg(yytext); }

    /* Numeric labels and references */
[0-9]+/:            { return parseNumLabel(yytext); }
(:)                 { return COLON; }
[0-9]+[bf]          { printf("Forward/backward local label reference: %s\n", yytext); }

    /* Text labels */
[a-zA-Z_][a-zA-Z0-9_]?*/:? { return parseText(yytext);}


    /* Numbers */
(?i:0x-?[a-f0-9]+)  {return parseNum(yytext);}
(?i:-?[0-9]+)       {return parseNum(yytext);}


    /* Everything else */
(?i:[iorw])/[,\n]   { return parseText(yytext); }
(?i:\".*\")         { return parseString(yytext); }
(\n)                { return NL; }
(,)                 { return COMMA; }
(\()                { return LPAREN; }
(\))                { return RPAREN; }
(\")                { return DQUOTE; }
(')                 { return SQUOTE; }
(\#.*)/\n            { ; } /* Ignore comments */
([\ \t\r])          { ; } /* Ignore whitespace */
.                   { ; }


%%

void yyerror(const char* s)
{
    //fprintf(stderr, "Error at line %d: %s\n", yylineno, s);
    //exit(-1);
    return;
}

int streq(const char* str1, const char* str2)
{
    return (strcmp(str1, str2) == 0);
}

void strlower(char* src)
{
    for (char* cp = src; *cp; cp++) {
        *cp = (char)tolower(*cp);
    }
}

int toInt(char* src)
{
    int num = 0;
    int base;
    char* endptr;
    base = 10;
    if (src[0] == '0' && (src[1] == 'x' || src[1] == 'X')) {
        src = src + 2;
        base = 16;
    }
    num = strtol(src, &endptr, base);
    return (int)num;
}


void copySval(const char* yytext, char** sval)
{
    *sval = (char*)malloc(sizeof(char)*(strlen(yytext)+1));
    strcpy(*sval, yytext);
    strlower(*sval);
    #ifdef DEBUG
    printf("%s ", yytext);
    #endif
}

int parseReg(char* yytext)
{
    int reg;
    strlower(yytext);

    for (reg = 0; reg < 32; reg++) {
        /* Walk the list of assembly programmer-friendly registers */
        if (streq(regNums[reg], yytext)) {
            yylval.ival = reg;
            #ifdef DEBUG
            printf("%s -> x%d", yytext, yylval.ival);
            #endif
            return REG;
        }
    }

    /* If nothing comes up, we got a register like x0, x1, x2 ... */
    if (strlen(yytext) == 2) {
        reg = yytext[1]-'0';
    } else if (strlen(yytext) == 3) {
        reg = (yytext[1]-'0')*10+(yytext[2]-'0');
    }
    yylval.ival = reg;

    #ifdef DEBUG
    printf("x%d", yylval.ival);
    #endif
    return REG;
}

int parseDirective(char* yytext)
{
    copySval(yytext, &yylval.sval);
    return DIRECTIVE;
}

int parseArgument(char* yytext)
{
    copySval(yytext, &yylval.sval);
    return ARGUMENT;
}

int parseSection(char* yytext)
{
    copySval(yytext, &yylval.sval);
    return SECTION;
}

int parseRelocation(char* yytext)
{
    copySval(yytext, &yylval.sval);
    return RELOCATION;
}

int parseOption(char* yytext)
{
    copySval(yytext, &yylval.sval);
    return OPTION;
}

int parseRV32I(char* yytext)
{
    copySval(yytext, &yylval.sval);
    return RV32I;
}

int parseRV64I(char* yytext)
{
    copySval(yytext, &yylval.sval);
    return RV64I;
}

int parsePseudo(char* yytext)
{
    copySval(yytext, &yylval.sval);
    return PSEUDO;
}

int parseString(char* yytext)
{
    copySval(yytext, &yylval.sval);
    printf("string: %s\n", yytext);
    return STRING;
}

int parseNum(char* yytext)
{
    yylval.ival = toInt(yytext);
    #ifdef DEBUG
    printf("%d", yylval.ival);
    #endif
    return NUM;
}

int parseNumLabel(char* yytext)
{
    #ifdef DEBUG
    printf("Entire num label: \"%s\"\n", yytext);
    #endif
    yylval.ival = toInt(yytext);
    return NUMLABEL;
}

int parseText(char* yytext)
{
    copySval(yytext, &yylval.sval);
    //printf("text: %s\n", yytext);
    return TEXT;
}

