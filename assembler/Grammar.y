/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

%{
    #include <stdio.h>
    #include <stdint.h>
    #include <stdlib.h>
    #include <string.h>

    #include "assembler.h"

    extern int yylex();
    extern int yylex_destroy();
    extern int yyparse();
    extern FILE* yyin;

    int PASS_NUMBER;
    u32 PC = 0;

    void yyerror(const char* s);
%}

%union value {
    int ival;
    char* sval;
}

%token COMMA COLON NL END LPAREN RPAREN DQUOTE SQUOTE
%token <ival> NUM REG NUMLABEL
%token <sval> TEXT STRING DIRECTIVE ARGUMENT RELOCATION SECTION OPTION
%token <sval> RV32I RV64I PSEUDO

/* For yylineno */
%locations

%%

grammar:
      line
    | line grammar
    ;

line:
      RV32I REG COMMA REG COMMA REG NL { new_mcode_line(asm_r_type($1, $2, $4, $6), PC); }
    | RV32I REG COMMA REG COMMA NUM NL { new_mcode_line(asm_reg_reg_imm($1, $2, $4, $6), PC); }
    | RV32I REG COMMA NUM NL { new_mcode_line(asm_reg_imm($1, $2, $4), PC); }
    | RV32I REG COMMA NUM LPAREN REG RPAREN NL { new_mcode_line(asm_ld_st($1, $2, $4, $6), PC); }
    | RV32I TEXT COMMA TEXT NL { new_mcode_line(asm_text_text($1, $2, $4), PC); }
    | RV32I NL { new_mcode_line(asm_single($1), PC); }
    | REG
    | NUM
    | TEXT
    | STRING
    | DIRECTIVE
    | ARGUMENT
    | RELOCATION
    | SECTION
    | OPTION
    | COMMA
    | COLON
    | LPAREN
    | RPAREN
    | DQUOTE
    | SQUOTE
    | NL
    | RV32I
    | RV64I
    | PSEUDO

%%

//RV32I REG COMMA REG COMMA REG NL { printf("0x%.8x", asm_xor($2, $4, $6)); }
//%.8x\n", asm_xor($2, $4, $6)); }
int main(int argc, char* argv[])
{
    FILE* outfile;

    if (argc != 3) {
        fprintf(stderr, "Error: Must specify input and output files\n");
        exit(-1);
    }

    /* First pass: Generate the symbol table */
    PASS_NUMBER = 1;

    yyin = fopen(argv[1], "r");
    yyparse();
    fclose(yyin);
    yylex_destroy();

    /* Second pass: Generate the machine code */
    PASS_NUMBER = 2;

    yyin = fopen(argv[1], "r");
    yyparse();
    fclose(yyin);
    yylex_destroy();

    outfile = fopen(argv[2], "w");
    write_mcode(outfile);
    fclose(outfile);

    return 0;
}

