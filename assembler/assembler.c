/* Copyright (c) 2019 Trey Boehm
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "assembler.h"

/* The symbol table linked list */
symtab_entry *SymTabHead = NULL;
symtab_entry *SymTabTail = NULL;

/* The assembled machine code linked list */
mcode *MCodeHead;
mcode *MCodeTail;

/* Globals located in Grammar.y */
extern int PASS_NUMBER;
extern u32 PC;

mcode* new_mcode_line(u32 inst, u32 addr)
{
    mcode* new_inst;

    #ifdef DEBUG
    //printf("0x%.8x: 0x%.8x\n", addr, inst);
    #endif

    if (PASS_NUMBER == 1)
        return NULL;

    if (MCodeHead == NULL) {
        /* First entry, so create the head */
        MCodeHead = (mcode*)malloc(sizeof(mcode));
        /* Give it the generic name so the code after this block is the
         * same whether we're initializing the structure or not.
         */
        new_inst = MCodeHead;
        /* Only one entry, so head == tail */
        MCodeTail = MCodeHead;
    } else {
        new_inst = (mcode*)malloc(sizeof(mcode));
        /* The old tail must point to the new entry */
        MCodeTail->next = new_inst;
    }
    new_inst->inst = inst;
    new_inst->addr = addr;
    /* Add it to the end of the linked list */
    new_inst->next = NULL;
    MCodeTail = new_inst;

    return new_inst;
}

void print_mcode()
{
    mcode* current = MCodeHead;
    while (current != NULL) {
        printf("0x%.8x\n", current->inst);
        current = current->next;
    }
}

void write_mcode(FILE* outfile)
{
    mcode* current = MCodeHead;
    while (current != NULL) {
        fprintf(outfile, "%.8x\n", current->inst);
        current = current->next;
    }
}

void free_mcode()
{
    mcode* current = MCodeHead;
    mcode* next;
    while (current != NULL) {
        next = current->next;
        free(current);
        current = next;
    }
}

/* Add another entry to the symbol table. Take the name of the label and
 * its address as arguments. If the table is empty (ie, head == NULL),
 * create a new head. Otherwise, add a new entry at the tail.
 */
symtab_entry* new_sym(char* name, u32 addr)
{
    symtab_entry* new_entry;

    if (PASS_NUMBER != 1) {
        /* We only want to generate the symbol table on pass 1 */
        free(name);
        return NULL;
    }

    if (sym_addr(name) != -1) {
        /* If the symbol is already defined, throw an error */
        fprintf(stderr, "Error: Symbol %s defined at 0x%.8x and 0x%.8x\n",
                name, sym_addr(name), addr);
        exit(EXIT_FAILURE);
    }

    #ifdef DEBUG
    printf("LABEL NL: %s, 0x%.8x\n", name, PC);
    #endif

    if (SymTabHead == NULL) {
        SymTabHead = (symtab_entry*)malloc(sizeof(symtab_entry));
        new_entry = SymTabHead;
        /* Only one entry, so head == tail */
        SymTabTail = SymTabHead;
    } else {
        new_entry = (symtab_entry*)malloc(sizeof(symtab_entry));
        /* The old tail must point to the new_entry */
        SymTabTail->next = new_entry;
    }
    new_entry->name = (char*)malloc(sizeof(char)*(strlen(name)+1));
    strcpy(new_entry->name, name);
    new_entry->addr = addr;
    new_entry->next = NULL;
    SymTabTail = new_entry;

    #ifdef DEBUG
    print_symtab();
    #endif

    free(name);
    return new_entry;
}

i32 sym_addr(char* labelname)
{
    symtab_entry* label = SymTabHead;
    while (label != NULL) {
        if (strcmp(label->name, labelname) == 0) {
            return label->addr;
        }
        label = label->next;
    }
    return -1;
}

void print_symtab()
{
    printf("\tSymbol table:\n");
    symtab_entry* current = SymTabHead;
    while (current != NULL) {
        printf("\t0x%.8x has label \"%s\"\n", current->addr, current->name);
        current = current->next;
    }
}

void free_symtab()
{
    symtab_entry* current = SymTabHead;
    symtab_entry* next;
    while (current != NULL) {
        free(current->name);
        next = current->next;
        free(current);
        current = next;
    }
}

u32 calc_offset(u32 addr, char* labelname)
{
    i32 label_addr = sym_addr(labelname);
    if (label_addr == -1) {
        fprintf(stderr, "Error: Undefined symbol (%s)\n", labelname);
        exit(EXIT_FAILURE);
    } else {
        return (u32)((i32)(label_addr) - (i32)addr - 4);
    }
}

u32 get_base(const char *instr, const struct instr_base *base_array)
{
    int i;
    for (i = 0; base_array[i].instr[0] != '\0'; i++) {
        if (strcmp(base_array[i].instr, instr) == 0) {
            return base_array[i].base;
        }
    }
    return UNDEFINED_INSTRUCTION;
}

u32 get_fence_op(const char* op)
{
    int i;
    for (i = 1; Fence_Ops[i][0] != '\0'; i++) {
        if (strcmp(op, Fence_Ops[i]) == 0)
            return i;
    }
    return UNDEFINED_INSTRUCTION;
}

u32 asm_single(char *instr)
{
    /* "instr" could be a supervisor instruction, FENCE.I, or a pseudo-
     * instruction like NOP, RET, or FENCE (with no arguments)
     *
     * For now, we just check for one of the fence instructions.
     * TODO: Fix this mess.
     */
    if (get_base(instr, Fence_Types) != UNDEFINED_INSTRUCTION) {
        return asm_fence_type(instr, IORW, IORW);
    } else {
        fprintf(stderr, "Error: Invalid instruction %s\n", instr);
        exit(EXIT_FAILURE);
    }
}

u32 asm_text_text(char *instr, char *text1, char *text2)
{
    /* "instr num1, num2" could only be FENCE */
    u32 pred;
    u32 succ;

    pred = get_fence_op(text1);
    succ = get_fence_op(text2);
    if (strcmp(instr, "fence") == 0 && pred != UNDEFINED_INSTRUCTION
            && succ != UNDEFINED_INSTRUCTION) {
        return asm_fence_type(instr, pred, succ);
    } else {
        fprintf(stderr, "Error: Invalid instruction %s\n", instr);
        exit(EXIT_FAILURE);
    }
}

u32 asm_reg_imm(char *instr, u32 rd, u32 imm)
{
    /* "instr rd, imm" could be a J-type or a U-type */
    if (get_base(instr, J_Types) != UNDEFINED_INSTRUCTION) {
        return asm_j_type(instr, rd, imm);
    } else if (get_base(instr, U_Types) != UNDEFINED_INSTRUCTION) {
        return asm_u_type(instr, rd, imm);
    } else {
        fprintf(stderr, "Error: Invalid instruction %s\n", instr);
        exit(EXIT_FAILURE);
    }
}

u32 asm_reg_reg_imm(char *instr, u32 r1, u32 r2, u32 imm)
{
    /* "instr r1, r2, imm" could be an I-type or a B-type */
    if (get_base(instr, B_Types) != UNDEFINED_INSTRUCTION) {
        return asm_b_type(instr, r1, r2, imm);
    } else if (get_base(instr, I_Types) != UNDEFINED_INSTRUCTION) {
        return asm_i_type(instr, r1, r2, imm);
    } else {
        fprintf(stderr, "Error: Invalid instruction %s\n", instr);
        exit(EXIT_FAILURE);
    }
}

u32 asm_ld_st(char *instr, u32 r1, u32 offset, u32 r2)
{
    /* "instr r1, offset(r2)" could be a load or a store */
    if (instr[0] == 'l') {
        return asm_i_type(instr, r1, r2, offset);
    } else if (instr[0] == 's') {
        return asm_s_type(instr, r1, offset, r2);
    } else {
        fprintf(stderr, "Error: Invalid instruction %s\n", instr);
        exit(EXIT_FAILURE);
    }
}

u32 asm_r_type(char *instr, u32 rd, u32 rs1, u32 rs2)
{
    u32 base;

    base = get_base(instr, R_Types);
    assert(base != UNDEFINED_INSTRUCTION);

    base += rd << RD_SHF;
    base += rs1 << RS1_SHF;
    base += rs2 << RS2_SHF;
    return base;
}

u32 asm_i_type(char *instr, u32 rd, u32 rs1, u32 imm)
{
    u32 base;

    base = get_base(instr, I_Types);
    assert(base != UNDEFINED_INSTRUCTION);

    base += rd << RD_SHF;
    base += rs1 << RS1_SHF;
    /* SLLI, SRLI, and SRAI are handled differently */
    /* TODO: Do this checking in a cleaner way */
    if (strcmp(instr, "slli") == 0 || strcmp(instr, "srli") == 0
            || strcmp(instr, "srai") == 0) {
        base += (MASK(imm, 4, 0) << 20);
    } else {
        base += (MASK(imm, 11, 0) << 20);
    }
    return base;
}

u32 asm_s_type(char *instr, u32 rs2, u32 imm, u32 rs1)
{
    u32 base;

    base = get_base(instr, S_Types);
    assert(base != UNDEFINED_INSTRUCTION);

    base += (BIT_SEL(imm, 4, 0) << 7);
    base += rs1 << RS1_SHF;
    base += rs2 << RS2_SHF;
    base += (BIT_SEL(imm, 11, 5) << 25);
    return base;
}

u32 asm_b_type(char *instr, u32 rs1, u32 rs2, u32 imm)
{
    u32 base;

    base = get_base(instr, B_Types);
    assert(base != UNDEFINED_INSTRUCTION);

    printf("instr = %s, base = 0x%x, imm = %d, rs1 = %d, rs2 = %d\n",
            instr, base, imm, rs1, rs2);

    base += (BIT_SEL(imm, 11, 11) << 7);
    base += (BIT_SEL(imm, 4, 1) << 8);
    base += rs1 << RS1_SHF;
    base += rs2 << RS2_SHF;
    base += (BIT_SEL(imm, 10, 5) << 25);
    base += (BIT_SEL(imm, 12, 12) << 31);
    printf("base = %.8x\n", base);
    return base;
}

u32 asm_u_type(char *instr, u32 rd, u32 imm)
{
    u32 base;

    base = get_base(instr, U_Types);
    assert(base != UNDEFINED_INSTRUCTION);

    base += rd << RD_SHF;
    base += imm << 12;
    return base;
}

u32 asm_j_type(char *instr, u32 rd, u32 imm)
{
    u32 base;

    base = get_base(instr, J_Types);
    assert(base != UNDEFINED_INSTRUCTION);

    base += rd << RD_SHF;
    base += (BIT_SEL(imm, 19, 12) << 12);
    base += (BIT_SEL(imm, 11, 11) << 20);
    base += (BIT_SEL(imm, 10, 1) << 21);
    base += (BIT_SEL(imm, 20, 20) << 31);
    return base;
}

u32 asm_fence_type(char *instr, u32 pred, u32 succ)
{
    u32 base;

    base = get_base(instr, Fence_Types);
    /* The FENCE.I instruction takes no operands */
    if (strcmp(instr, "fence.i") == 0)
        return base;

    /* The FENCE, however, takes a pred and succ */
    base += succ << 20;
    base += pred << 24;
    return base;
}
